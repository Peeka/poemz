<?php

	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
	include("includes/config.php");
	include("includes/class_database.php");
	include("includes/class_parser.php");
	
	$database = new database();
	$parser = new parser();
	
	switch($_GET['action']) {
		case 'get_sub_categories':
			getSubCategories();
		break;
	}

function getSubCategories() {
global $parser;

	$category_id = mysql_real_escape_string($_GET['category_id']);
	$sub_categories = mysql_query("SELECT * FROM `p_events_categories_sub` WHERE `parent` = '{$category_id}' ORDER BY `name` ASC");
	while($sub_cat = mysql_fetch_array($sub_categories)) {
		$sub_cats[] = $sub_cat;
	}
	
	if($category_id == 0) {
		$empty['id'] = "0";
		$empty['name'] = "<-----";
		$sub_cats[] = $empty;
	}
	
	if($_GET['return_type'] == "json") {
		echo json_encode($sub_cats);
	}
}
?>