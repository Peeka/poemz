<?php

class permissions {

	private $groups = array();
	private $permissions = array();
	
	public function __construct() {
		if(empty($this->groups) || empty($this->permissions)) {
			@$loginHash = database::escape($_COOKIE['logincookie']);
		
			$user = database::fetch("SELECT groups,permissions FROM accounts WHERE login_hash = '{$loginHash}'");
			
			if(!empty($user['groups'])) {
				// The user is in a group
				$grps = "";
				$groups = explode(",",$user['groups']);
				foreach($groups as $group) {
					$grp = database::fetch("SELECT permissions FROM groups WHERE id = '{$group}'");
					$grps[$group]['permissions'] = explode(",",$grp['permissions']);
				}
				$this->groups = $grps;
			}
			$this->permissions = explode(",",$user['permissions']);
		}
	}
	
	public function check($value) {
		$has_permission = false;
	
		// Lets check if we're in a group, if so check the groups permissions
		if(!empty($this->groups)) {
			foreach($this->groups as $group) {
				if(in_array($value,$group['permissions']) || in_array("*",$group['permissions'])) {
					$has_permission = true;
				}
			}
		}
		
		// Then check the personal permissions
		if(in_array($value,$this->permissions) || in_array("*",$this->permissions)) {
			$has_permission = true;
		}

	return $has_permission;
	}

}

?>