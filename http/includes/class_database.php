<?php

class database {

	private $db_connection;
	private $db_host = DB_HOST;
	private $db_user = DB_USER;
	private $db_password = DB_PASSWORD;
	private $db_database = DB_DATABASE;
	
	public function __construct() {
		$this->db_connection  = mysql_connect($this->db_host, $this->db_user, $this->db_password);
		mysql_select_db($this->db_database, $this->db_connection);
		mysql_set_charset('utf8'); 
	}
	
	public function escape($string) {
		return mysql_real_escape_string($string);
	}
	
	public function fetch($query) {
		return @mysql_fetch_array(mysql_query($query));
	}
	
	public function query($query) {
		mysql_query($query);
	}

	public function close() {
		mysql_close($this->db_connection);
	}

	public function escape_all() {
		// Prevent SQL injections
		if(!empty($_POST)) {
			foreach($_POST as $key => $value) {
				if(is_array($_POST[$key])) {
					foreach($_POST[$key] as $key2 => $value2) {
						$_POST[$key][$key2] = mysql_real_escape_string($value2);
					}
				}
				else {
					if(isset($_POST['no_escape'])) {
						$escape = true;
						foreach($_POST['no_escape'] as $no_escape) {
							if($escape == $key) {
								$escape = false;
							}
						}
						if($escape) {
							$_POST[$key] = mysql_real_escape_string($value);
						}
					}
				}
			}
		}
		if(!empty($_GET)) {
			foreach($_GET as $key => $value) {
				if(is_array($_GET[$key])) {
					foreach($_GET[$key] as $key2 => $value2) {
						$_GET[$key][$key2] = mysql_real_escape_string($value2);
					}
				}
				else {
					$_GET[$key] = mysql_real_escape_string($value);
				}
			}
		}
		if(!empty($_REQUEST)) {
			foreach($_REQUEST as $key => $value) {
				if(is_array($_REQUEST[$key])) {
					foreach($_REQUEST[$key] as $key2 => $value2) {
						$_REQUEST[$key][$key2] = mysql_real_escape_string($value2);
					}
				}
				else {
					$_REQUEST[$key] = mysql_real_escape_string($value);
				}
			}
		}
	}

}

?>