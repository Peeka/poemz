<?php

class text {
	
	public function __construct() {
		
	}
	
	public function cut($string, $max_length=200) {
		if (strlen($string) > $max_length){
			$string = substr($string, 0, $max_length);
			$pos = strrpos($string, " ");
			if($pos === false) {
					return substr($string, 0, $max_length);
			}
				return substr($string, 0, $pos);
		}else{
			return $string;
		}
	}

}

?>