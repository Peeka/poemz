<?php

class mail {
	
	public function __construct() {

	}
	
	public function send($to="", $subject="", $message="", $from="no-reply@horecatorget.se") {
		$headers = "MIME-Version: 1.0" . "\n";
		$headers .= "Content-type:text/html;charset=utf-8" . "\n";
		$headers .= "From: $from" . "\n";
		$subject = "=?UTF-8?B?".base64_encode($subject)."?=";
		mail($to,$subject,$message,$headers);
	}
	
	public function tags($tags, $mail) {
		foreach($tags as $find => $replace) {
			$mail['mail_title'] = str_replace("[{$find}]",$replace,$mail['mail_title']);
			$mail['mail_text'] = str_replace("[{$find}]",$replace,$mail['mail_text']);
		}
	return $mail;
	}

}

?>