<?php

class debugger {

	private $warnings = array();
	
	public function __construct() {
		
	}
	
	public function add($text,$level) {
		$warning['text'] = $text;
		$warning['level'] = $level;
		$this->warnings[] = $warning;
	}
	
	public function get() {
		return $this->warnings;
	}

}

?>