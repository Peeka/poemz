<?php

class page {

	public $page = array();
	
	public function __construct() {
		
	}
	
	public function rurl() {
		return mysql_real_escape_string(implode("/",$this->page['rurl']));
	}
	
	public function index($index) {
		return mysql_real_escape_string($this->page['rurl'][$index]);
	}
	
	public function contain($string) {
		if(in_array($string,$this->page['rurl'])) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function set($name, $value) {
		$this->page[$name] = $value;
	}
	
	public function get($name) {
		if(is_array($this->page[$name])) {
			foreach($this->page[$name] as $key => $value) {
				$this->page[$key] = mysql_real_escape_string($value);
			}
		}
		return $this->page[$name];
	}

}

?>