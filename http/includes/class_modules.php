<?php

class modules {

	private $html 		= "test";
	private $path 		= "";
	private $htmlOwn 	= false;
	private $tabs 		= array();
	private $header 	= array('title'=>'Module');
	
	public function __construct() {
		
	}
	
	public function path($action, $value = "") {
		Switch($action) {
			case 'set':
				// Set the module path
				$this->path = $value;
			break;
			case 'get':
				// Get the module path
				return $this->path;
			break;
		}
	}
	
	public function html($action, $value = "") {
		Switch($action) {
			case 'set':
				// Clear old html and add new html
				$this->html = $value;
			break;
			case 'add':
				// Add new html
				$this->html .= $value;
			break;
			case 'get':
				// Print stored html
				return $GLOBALS['parser']->parse($this->header('get') . $this->tabs('print') . $this->footer('get'));
			break;
			case 'own':
				if(!empty($value)) {
					$this->htmlOwn = $value;
				}
				else {
					return $this->htmlOwn;
				}
			break;
		}
	}
	
	public function module_url() {
		return "modules.php?module_id=".mysql_real_escape_string($_GET['module_id']);
	}
	
	public function access_token() {
		@$module_id = database::escape($_GET['module_id']);
		$get = database::fetch("SELECT access_token FROM nova_modules_access WHERE module_id = '{$module_id}' AND user_id = '". account::user('id') ."'");
		$access_token = $get['access_token'];
		
		if(!empty($access_token)) {
			return $access_token;
		}
		else {
			return 0;
		}
	}
	
	public function tabs($action, $value = "") {
		if($action == "add") {
			$this->tabs[] = $value;
		}
		else if($action == "get") {
			return $this->tabs;
		}
		else if($action == "print") {
			if(!empty($this->tabs)) {
				$temp1 = '<div id="tabs">
				<ul>';

				$i = 1;
				foreach($this->tabs as $tab) {
					$temp_li[] = '<li><a href="#tabs-'.$i.'">'. $tab['title'] .'</a></li>';
					$temp_div[] = '<div id="tabs-'.$i.'">
					<h2>'. $tab['title'] .'</h2>
					'. $tab['content'] .'
					</div>';
				$i++;
				}
				
				$temp2 = implode("",$temp_li).'</ul>';
				$temp3 = implode("",$temp_div);
				$temp4 = '</div>';
				
				return $temp1.$temp2.$temp3.$temp4;
			}
		}
	}
	
	public function header($action, $value = "") {
		if($action == "title") {
			$this->header['title'] = $value;
		}
		else if($action == "get") {
			return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
		<head>
			<title>'. $this->header['title'] .'</title>
			<style type="text/css">
				body, th {
					font-family:geneva,tahoma,verdana,helvetica,sans-serif;
					font-size:11px;
					background-color:#fff;
					text-align:left;
					margin:0px;
				}
				.wrapper {
					width:100%;
					height:100%;
					padding:0px;
				}
				tr {
					border-bottom: 1px #CCCCCC dotted;
				}
				th	{background-color:#eee;}
				label {
					font-weight: bold;
				}
				.tablerow td{
					border: 0px;
					border-bottom: 1px #cccccc dashed;
				}
				.tablerow:hover {
					background-color: #F2F2F2;
				}
			<!--
			## TABS CODE ##
			-->
				#dialog label, #dialog input { display:block; }
				#dialog label { margin-top: 0.5em; }
				#dialog input, #dialog textarea { width: 95%; }
				#tabs { margin-top: 0px;}
				#tabs li .ui-icon-close { float: left; margin: -3px -2px -1em -12px; cursor: pointer; }
				#add_tab { cursor: pointer; font-size:0.8em; }
				.ui-widget { font-size: 0.8em; }
			</style>
			<link href="js/jquery/themes/cupertino/jquery.ui.all.css" rel="stylesheet">
			<script type="text/javascript" src="js/jquery/jquery-1.7.2.js"></script>
			<script type="text/javascript" src="js/jquery/ui/jquery.ui.core.js"></script>
			<script type="text/javascript" src="js/jquery/ui/jquery.ui.widget.js"></script>
			<script type="text/javascript" src="js/jquery/ui/jquery.ui.mouse.js"></script>
			<script type="text/javascript" src="js/jquery/ui/jquery.ui.resizable.js"></script>
			<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.custom.min.js"></script>
			<script>
			$(function() {
				$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			});
			$(window).resize(function() {
				$("#tabs").width($(window).width()-8);
				$("#tabs").height($(window).height()-7);
			});
			$(document).ready(function() {
				$("#tabs").width($(window).width()-8);
				$("#tabs").height($(window).height()-7);
			});
			</script>
			<style>
				#tabs { margin-top: 0px; }
				#tabs li .ui-icon-close { float: left; margin: -3px -2px -1em -12px; cursor: pointer; }
			</style>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		</head>
		<body>
			<div class="wrapper">
			';
		}
	}
	
	public function footer($action, $value = "") {
		if($action == "get") {
			return '
				</div>
			</body>
		</html>';
		}
	}

}

?>