<?php

class pager {

	private $settings = array();
	
	public function __construct() {
		$this->settings['items_per_page'] = 10;
		$this->settings['page_margin'] = 2;
		$this->settings['page_index'] = 1;
	}
	
	public function page() {
		$page_index = $GLOBALS['parser']->parse('{page:rurl index="'. $this->settings['page_index'] .'"/}');
		return (isset($page_index) && $page_index  > 0) ? $page_index : 1 ;
	}
	
	public function limit() {
		return ($this->page() == 1) ? "0,".$this->get("items_per_page") : (($this->page() * $this->get("items_per_page")) - $this->get("items_per_page")) .",". $this->get("items_per_page");
	}
	
	public function get($attr) {
		return $this->settings[$attr];
	}
	
	public function set($attr, $value) {
		$this->settings[$attr] = $value;
	}

}

?>