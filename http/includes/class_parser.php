﻿<?php

	$debug = false;

	class parser {
	
		public $runDepth = 0;
		public $maxDepth;
		public $parsers;
		public 	$pass;
		public 	$fullcache;
		public 	$numtags;
		public 	$numparsed;
		public	$taglog;
		public	$parsed;
		
		public function __construct($maxDepth = 25) {
			$this->runDepth		= 0;
			$this->maxDepth		= $maxDepth;
			$this->pass			= 1;
			$this->fullcache	= true;
			$this->numtags		= 0;
			$this->numparsed	= 0;
			$this->taglog		= array();
			$this->parsed		= array();
			$this->lineoffset	= 0;
		}
		
		public function parse($data, $log = false, $pass = 1) {
			$debug = $GLOBALS['debug'];
			if ($pass == 2) $this->pass = 2;
	
		//	$GLOBALS['debugger']->add('Beginning parsing (pass ' . $this->pass . ').');
	
			$this->runDepth++;
			if ($this->runDepth > $this->maxDepth) {
	#			if ($debug) $GLOBALS['debugger']->add('Tags nested too deep. Max depth is ' . $this->maxDepth . '.', 2);
				$this->runDepth--;
				return $data;
			}
			
			# Create the main array. Split incoming data so we get an array that looks like ('html', '{tag}', 'html', '{anothertag}', 'html', ...)
			$pattern	= '@((?:<!--)*{/?\w+(?::\w+)*(?:\s+\w+\s*=\s*(?:"(?:[^"]|"")*"|\'(?:[^\']|\'\')*\'|[^\s\'"/}]+))*\s*/?}(?:-->)*)@si';
			$parts		= preg_split($pattern, $data, -1, PREG_SPLIT_DELIM_CAPTURE);

			$tree		= array();
			$parsetag	= false;
			$error		= false;
			$tag		= false;
			$parsed		= '';
			$this->parsed[$this->runDepth] = '';
			$line		= 0 + $this->lineoffset;
			$depth		= 0;
			$index		= 0;
			$innerhtml	= '';
			$this->topopentag = false;
			
			foreach ($parts as $current) {
			
				$line += substr_count($current, "\n");
				
				if ($tag) {
				
					//$part	= preg_split("/{(\/)*(\w+)(?::?)(\w*)\s*(.*?)(\/)*}/s", $current, -1, PREG_SPLIT_DELIM_CAPTURE);
					$part	= preg_split("/{(\/)*(\w+)(?::?)(\w*)\s*(.*?)(\/){0,1}}$/s", $current, -1, PREG_SPLIT_DELIM_CAPTURE);
	
					$thistag = array(
									'class' => ($part[3] == '') ? '' : strtolower($part[2]),
									'tag' => ($part[3] == '') ? strtolower($part[2]) : strtolower($part[3]),
									'attributes' => $part[4], 
									'line' => $line, 
									'selfclosing' => ($part[5] == '/') ? true : false, 
									'closing' => ($part[1] == '/') ? true : false, 
									'opening' => ($part[1] != '/' && $part[5] != '/') ? true : false,
									'fulltag' => $current,
									'fullname' => $part[3] != '' ? strtolower($part[2]) . ':' . strtolower($part[3]) : strtolower($part[2])
								);
	
					if ($thistag['selfclosing']) {
					
						if ($depth === 0) {
							$thistag['innerhtml'] = '';
							$parsetag = true;
						} else {
							$innerhtml .= $current;
						}
	
					} else if ($thistag['closing']) {
					
						if ($depth > 1) $innerhtml .= $current;
	
						if ($thistag['tag'] !== $tree[--$index]['tag']) {
	
						//trigger_error("Parse error: Unexpected closing tag {$thistag['fulltag']} found on line {$line}. Expected {{$tree[$index]['fullname']}}, which was opened on line {$tree[$index]['line']}",E_USER_WARNING);
					//	$GLOBALS['debugger']->add('Parse error: Unexpected closing tag ' . $thistag['fulltag'] . ' found on line ' . $line . '. Expected {/' . $tree[$index]['fullname'] . '}, which was opened on line ' . $tree[$index]['line'] . ".",1);

						} else if (--$depth === 0) {
	
							$tree[0]['innerhtml'] = $innerhtml;
							$thistag = $tree[0];
							$innerhtml = '';
							$parsetag = true;
							
						}
					
					} else if ($thistag['opening']) {
						if (!$this->topopentag) $this->topopentag = $thistag;					
						if ($depth++ > 0) $innerhtml .= $current;
						$tree[$index++] = $thistag;

					}
				
					if ($parsetag) {
						$this->numtags++;
						if ($debug && function_exists('memory_get_usage')) {
							$memnow = memory_get_usage();
							$memmax = memory_get_peak_usage();
							$mt1 = microtime();
						}
						$this->lineoffset += $thistag['opening'] ? $tree[0]['line'] : 0;//$thistag['line'];
						$parsed = $this->parsetag($thistag);
						$this->lineoffset -= $thistag['opening'] ? $tree[0]['line'] : 0;//$thistag['line'];
						$this->parsed[$this->runDepth] .= $parsed;
						if ($debug) {
							$mt2 = microtime();
							$mt = round(round(substr($mt2, 0, strpos($mt2, ' ')) * 1000, 2) - round(substr($mt1, 0, strpos($mt1, ' ')) * 1000, 2), 2);
							if (function_exists('memory_get_usage')) {
								$memtmp = memory_get_peak_usage() > $memmax ? memory_get_peak_usage() : memory_get_usage();
								$mem = $memtmp - $memnow;
							} else {
								$mem = '-';
							}
	#						$this->taglog[count($this->taglog)] = $thistag;
	#						$this->taglog[count($this->taglog)-1]['parsed'] = mb_substr($parsed, 0, strlen($thistag['fulltag']), $GLOBALS['charset']) == $thistag['fulltag'] ? 'No' : 'Yes';
	#						$this->taglog[count($this->taglog)-1]['time'] = $mt;
	#						$this->taglog[count($this->taglog)-1]['mem'] = $mem;
						}
						$parsetag = false;
					}
	
				} else {
				
					if ($depth === 0) {
						$this->parsed[$this->runDepth] .= $current;
					} else {
						$innerhtml .= $current;
					}
	
				}
			
				$tag = !$tag;
				if ($error) break;
	
			}
			
			$this->runDepth--;

			if ($depth !== 0 && !$error) {
				//if ($debug) {
					$index--;
				$topopentag = $this->topopentag;
				$this->topopentag = false;
				$tag = $topopentag['class'] != '' ? $topopentag['class'] . ':' . $topopentag['tag'] : $topopentag['tag'];
				//trigger_error("Parse error: Missing closing tag for {{$tag}} which was opened on line {$topopentag['line']}. Trying to parse as self closing tag.",E_USER_WARNING);
			//	$GLOBALS['debugger']->add('Parse error: Missing closing tag for {' . $tag . '} which was opened on line ' . $topopentag['line'] . ". Trying to parse as self closing tag.",1);
					$this->runDepth++;
					$tmptag = '{' . $tag . ($topopentag['attributes'] != '' ? ' ' : '') . $topopentag['attributes'] . '/}';
				//$this->lineoffset += $topopentag['line'];
				$this->lineoffset += $tree[0]['line'];
					$tmpres = $this->parse($tmptag);
					$this->parsed[$this->runDepth] .= $tmpres == $tmptag ? '{' . $tag . ($topopentag['attributes'] != '' ? ' ' : '') . $topopentag['attributes'] . '}' : $tmpres;
				//$this->lineoffset -= $tree[0]['line'];
				//$this->lineoffset += $tree[1]['line'];
					$this->parsed[$this->runDepth] .= $this->parse($innerhtml);
				$this->lineoffset -= $tree[0]['line'];
				//$this->lineoffset=0;// -= $topopentag['line'];
					$this->runDepth--;
				//}
			}
			
		//	$GLOBALS['debugger']->add('Parsing completed (pass ' . $this->pass . ').');

			if (!$error) {
				return $this->parsed[$this->runDepth + 1];
			} else {
				//return $this->parse($data, true, $pass);
				return $data;
			}
	
		}
		
		#
		# Here we try to parse tags that we have identified.
		#
		private function parsetag($thistag) {

			$attributes	= $this->parseattributes($thistag['attributes']);	# Create an array with all attributes of the tag.
			$result		= false;
			# Calculate what the name of the parser file should be and see if it exists.
			$part		= ($thistag['class'] != '') ? $thistag['class'] : $thistag['tag'];
			$parserfile	= 'parser_' . $part . '.php';
            //die($parserfile);

			if (file_exists('parsers/' . $part . '/' . $parserfile)) {

				include_once ('parsers/' . $part . '/' . $parserfile);

				if (!isset($attributes['cache']) || $this->pass == 2 || strtolower($attributes['cache']) != 'false') {

					if ($thistag['class'] == '') {
					
						
                        echo "parser_{$thistag['tag']}";
						if (function_exists("parser_{$thistag['tag']}")) {
							$result = $thistag['tag']($thistag, $attributes);
						} else if (class_exists("parser_{$thistag['tag']}") && method_exists("parser_{$thistag['tag']}", "__parse")) {
							$ref	= "PARSER_{$thistag['tag']}";
							$class	= "parser_{$thistag['tag']}";
							if (!isset($this->parsers[$ref])) {
								//trigger_error("Creating new object for tag {$thistag['tag']}.",E_USER_NOTICE);
							//	$GLOBALS['debugger']->add("Creating new object for tag {$thistag['tag']}.",0);
								$this->parsers[$ref] = new $class;
							} else {
								//if ($debug) $GLOBALS['debugger']->add("Reusing existing object for tag {$thistag['tag']}.", 0);
							}
							if (in_array('__parse', get_class_methods($this->parsers[$ref]))) {
								$result = $this->parsers[$ref]->__parse($thistag, $attributes);
							} else {
								//trigger_error("Method '__parse' in class parser_{$thistag['tag']} is not callable.",E_USER_WARNING);
							//	$GLOBALS['debugger']->add("Method '__parse' in class parser_{$thistag['tag']} is not callable.",1);
							}
						} else if (class_exists("parser_{$thistag['tag']}") && method_exists("parser_{$thistag['tag']}", "__fallback")) {
							$ref = "PARSER_{$thistag['tag']}";
							$class = "parser_{$thistag['tag']}";
							if (!isset($this->parsers[$ref])) {
								//trigger_error("Creating new object for tag {$thistag['tag']}.",E_USER_NOTICE);
								$this->parsers[$ref] = new $class;
							} else {
								//if ($debug) $GLOBALS['debugger']->add("Reusing existing object for tag {$thistag['tag']}.");
							}
							if (in_array('__fallback', get_class_methods($this->parsers[$ref]))) {
								$result = $this->parsers[$ref]->__fallback($thistag, $attributes);
							} else {
								//trigger_error("Method '__fallback' in class parser_{$thistag['tag']} is not callable.",E_USER_WARNING);
							//	$GLOBALS['debugger']->add("Method '__fallback' in class parser_{$thistag['tag']} is not callable.",1);
							}
						} else {
							//trigger_error("Parser function [parser_{$thistag[0]}] missing in [/addons/{$part}/parser.php].",E_USER_WARNING);
						//	$GLOBALS['debugger']->add("Parser function [parser_{$thistag[0]}] missing in [/addons/{$part}/parser.php].",1);
						}

					} else {

						if (class_exists('parser_' . $thistag['class'])) {
							if (method_exists('parser_' . $thistag['class'], "parser_{$thistag['tag']}")) {
								$ref = 'PARSER_' . $thistag['class'];
								$class = 'parser_' . $thistag['class'];
								$method = "parser_{$thistag['tag']}";
								if (!isset($this->parsers[$ref])) {
									//trigger_error("Creating new object for tag {$thistag['class']}",E_USER_NOTICE);
								//	$GLOBALS['debugger']->add('Creating new object for tag ' . $thistag['class'] . '.',0);								
									$this->parsers[$ref] = new $class;

								} else {
									//if ($debug) $GLOBALS['debugger']->add('Reusing existing object for tag ' . $thistag['class'] . '.', 0);
								}
								if (in_array($method, get_class_methods($this->parsers[$ref]))) {
									$result = $this->parsers[$ref]->$method($thistag, $attributes);
								} else {
									//trigger_error("Method '{$method}' in class parser_{$thistag['class']} is not callable.",E_USER_WARNING);
								//	$GLOBALS['debugger']->add("Method '{$method}' in class parser_{$thistag['class']} is not callable.",1);
								}
							} else if (method_exists('parser_' . $thistag['class'], '__fallback')) {
								$ref = 'PARSER_' . $thistag['class'];
								$class = 'parser_' . $thistag['class'];
								if (!isset($this->parsers[$ref])) {
									//trigger_error("Creating new object for tag {$thistag['class']}",E_USER_NOTICE);
								//	$GLOBALS['debugger']->add('Creating new object for tag ' . $thistag['class'] . '.',0);
									$this->parsers[$ref] = new $class;
								} else {
									//if ($debug) $GLOBALS['debugger']->add('Reusing existing object for tag ' . $thistag['class'] . '.', 0);
								}
								if (in_array('__fallback', get_class_methods($this->parsers[$ref]))) {
									$result = $this->parsers[$ref]->__fallback($thistag, $attributes);
								} else {
									//trigger_error("Method '__fallback' in class parser_{$thistag['class']} is not callable.",E_USER_WARNING);
								//	$GLOBALS['debugger']->add("Method '__fallback' in class parser_{$thistag['class']} is not callable.",1);
								}
							} else {
								//trigger_error("Parser method [parser_{$thistag['tag']}] missing in class [parser_" . $thistag['class'] . "] in [{$parserfile}].",E_USER_WARNING);
							//	$GLOBALS['debugger']->add("Parser method [parser_{$thistag['tag']}] missing in class [parser_" . $thistag['class'] . "] in [{$parserfile}].",1);
							}
						} else if (function_exists("parser_{$thistag['tag']}")) {
							$function = "parser_{$thistag['tag']}";
							$result = $function($thistag, $attributes);
						} else {
							//trigger_error("Parser function [parser_{$thistag['tag']}] missing in [/addons/{$part}/parser.php].",E_USER_WARNING);
						//	$GLOBALS['debugger']->add("Parser function [parser_{$thistag['tag']}] missing in [/addons/{$part}/parser.php].",1);
						}

					}

				} else {

					$this->fullcache = false;
	#				env::site('cache_gzip', 0);

				}

			# If we couldn't find a parser for the tag:
			} else {
				//trigger_error("Could not find parser for tag {$thistag['fulltag']}. Leaving tag untouched.",E_USER_WARNING);
			//	$GLOBALS['debugger']->add('Could not find parser for tag ' . $thistag['fulltag'] . '. Leaving tag untouched.',1);
			}

			if ($result !== false) {

				$this->numparsed++;

				if ($thistag['opening']) {
					if (!isset($attributes['parse']) || strtolower($attributes['parse']) != 'false') {
						return $this->parse($result);
					} else {
						return $result;
					}
				} else if ($thistag['selfclosing']) {
					if (isset($attributes['parse']) && strtolower($attributes['parse']) == 'true') {
						return $this->parse($result);
					} else {
						return $result;
					}
				}

			# If the tag could not be parsed, we just return it untouched.
			} else {

				$tag = ($thistag['class'] != '') ? $thistag['class'] . ':' . $thistag['tag'] : $thistag['tag'];
				if ($thistag['opening']) {
					return $thistag['fulltag'] . $thistag['innerhtml'] . '{/' . $tag . '}';
				} else {
					return $thistag['fulltag'];
				}

			}

		}
	
		private function parseattributes($string) {
			$attributes  = array();
			if (preg_match_all('@(\w+)\s*=\s*(?:"((?:[^"]|"")*)"|\'((?:[^\']|\'\')*)\'|([^\s\'"/}]+))@s', $string, $attribute, PREG_SET_ORDER)) {
				foreach ($attribute as $current) {
					switch (count($current)) {
						case 3:
							$val = str_replace('""', '"', $current[2]);
							break;
						case 4:
							$val = str_replace("''", "'", $current[3]);
							break;
						case 5:
							$val = $current[4];
							break;
					}
					$attributes[strtolower($current[1])] = $this->parse($val);
				}
			}
			return $attributes;
		}
		
		public function getparsed() { return isset($this->parsed[1]) ? $this->parsed[1] : ''; }
		public function setparsed($str = false) { if ($str !== false) $this->parsed[1] = $str; }
		
	}
	
?>