<?php

class template {

	private $html 			= "";
	private $areas 			= array();
	private $filePath 		= "";
	private $masterPage 	= "";
	private $templatePage 	= "";
	public  $restUrl	 	= array();
	
	public function __construct() {
		
	}
	
	public function check_redirect_from_old_website() {
		$redirect = "";
		$rurl = mysql_real_escape_string($_SERVER['REQUEST_URI']);
		
		$rurlArray = explode("/",$rurl);
		if(!empty($rurlArray)) {
			foreach($rurlArray as $rurlA) {
				if(!empty($rurlA) && $rurlA != "nya") {
					$rurls[] = $rurlA;
				}
			}
		}
		
	//	echo $rurl .'<br /><br />';
	
		// BRANSCHGUIDEN
		if($rurls[0] == "register") {
			if(empty($rurls[1])) {
				$redirect = "/branschguiden";
			}
			else {
				// Category or company?
				$category = explode("_",str_replace(".htm","",$rurls[1]));
				$id = $category[1];
				
				if($category[2] == "k") {
					// LOAD CATEGORY
					// Now found the category from our database with this ID from the old website
					$cat = mysql_fetch_array(mysql_query("SELECT * FROM branschguiden_categories WHERE id = '{$id}'"));
					
					// Did we find anything, and does it have a parent?
					if(!empty($cat['name'])) {
						if($cat['parent'] > 0) {
							// It has a parent, retrieve the friendly URL for the parent and write the new URL
							$parent = mysql_fetch_array(mysql_query("SELECT * FROM branschguiden_categories WHERE id = '{$cat['parent']}'"));
							$redirect = "/branschguiden/category/{$parent['current_url']}/{$cat['current_url']}";
						}
						else {
							// No parent found, lets write the new URL
							$redirect = "/branschguiden/category/{$cat['current_url']}";
						}
					}
				}
				else if($category[2] == "f") {
					// LOAD COMPANY
					$comp = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$id}'"));
					if(!empty($comp['name'])) {
						// Write the new URL
						$redirect = "/branschguiden/leverantor/{$comp['url_friendly']}";
					}
				}
			}
		}	// NYHETER
		else if($rurls[0] == "nyheter") {
			if($rurls[1] == "pageNyheter.php") {
				$redirect = "/nyheter";
			}
			else if(!is_numeric($rurls[1])) {
				$category = explode("=",$rurls[1]);
				$id = $category[1];
				// LOAD COMPANY
				if($id > 0) {
					$nyhet = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '{$id}'"));
					if(!empty($nyhet['title'])) {
						// Write the new URL
						$redirect = "/nyheter/{$nyhet['id']}/". $this->url_friendly($nyhet['title']);
					}
				}
			}
		} // PLATSBANKEN
		else if($rurls[0] == "platsbanken") {
			if($rurls[1] == "pagePlatsbanken.php") {
				$redirect = "/platsbanken/arbetssokande";
			}
			else if($rurls[1] != "jobb-annons") {
				// JOB ADVERT OR JOB CATEGORY?
				if(isset($_GET['WorkID'])) {
					// LOAD JOB ADVERTISEMENT
					@$id = $_GET['WorkID'];
					$job = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE id = '{$id}'"));
					if(!empty($job['title'])) {
						// Write the new URL
						$redirect = "/platsbanken/jobb-annons/{$job['id']}/". $this->url_friendly($job['title']);
					}
				}
				else {
					// LOAD JOB CATEGORY
					@$id = $_GET['regionID'];
					if(isset($_GET['regionID']) && $id == 0) {
						// Write the new URL
						$redirect = "/platsbanken/arbetssokande";
					}
					else {
						$region = mysql_fetch_array(mysql_query("SELECT * FROM regions WHERE id = '{$id}'"));
						if(!empty($region['name'])) {
							// Write the new URL
							$redirect = "/platsbanken/arbetssokande/1/{$region['url_friendly']}";
						}
					}
				}
			}
		} // KÖP & SÄLJ
		else if($rurls[0] == "buyandsell") {
			if($rurls[1] == "pageBuyandsell.php") {
				$redirect = "/till-salu/category/_all/_all/1";
			}
			else if($rurls[1] != "category") {
				// SALE ARTICLE OR SALE ARTICLE CATEGORY?
				if(isset($_GET['kategoriID'])) {
					// SALEARTICLE
					$id = $_GET['kategoriID'];
					$category = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj_categories WHERE id = '{$id}'"));
					if(!empty($category['title'])) {
						// Write the new URL
						$redirect = "/till-salu/category/{$category['url_friendly']}/_all/1";
					}
				}
				else if(isset($_GET['imported'])) {
					$article = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj WHERE old_website_imported = '{$_GET['imported']}'"));
					if(!empty($article['title'])) {
						// Write the new URL
						$redirect = "/till-salu/{$article['id']}/". $this->url_friendly($article['title']);
					}
				}
				else if(isset($_GET['MarketID'])) {
					$article = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj WHERE id = '{$_GET['MarketID']}'"));
					if(!empty($article['title'])) {
						// Write the new URL
						$redirect = "/till-salu/{$article['id']}/". $this->url_friendly($article['title']);
					}
				}
				else {
					// LOAD BUY AND SELL
					$redirect = "/till-salu/category/_all/_all/1";
				}
			}
		} // OM OSS
		else if($rurl == "/omtorget/pageOmTorget.php") {
			$redirect = "/om-oss";
		} // OM OSS - KONTAKTA OSS
		else if($rurl == "/omtorget/pageOmTorget.php?page=kontakta_oss") {
			$redirect = "/om-oss/kontakta-oss";
		} // OM OSS - VÅRA VILLKOR
		else if($rurl == "/omtorget/pageOmTorget.php?page=vara_villkor") {
			$redirect = "/om-oss/vara-villkor";
		} // ANNONSERA
		else if($rurl == "/omtorget/pageOmTorget.php?page=annonsera") {
			$redirect = "/annonsera";
		}
		
		// 410 GONE = server knows the page does not exist anymore
		if(!empty($redirect)) {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: http://".SERVER_NAME.$redirect);
		//	exit("Location: http://www.".SERVER_NAME.$redirect);
		}
	}
	
	public function load() {
	
		$rurl = mysql_real_escape_string($_SERVER['REQUEST_URI']);
		$rurl = str_replace(strstr($rurl,"?"),"",$rurl);
		
		// Lets update the page::$page->rurl with the REQUEST_URI
		$rurlArray = explode("/",$rurl);
		if(!empty($rurlArray)) {
			foreach($rurlArray as $rurlA) {
				if(!empty($rurlA) && $rurlA != "nya") {
					$rurlAs[] = $rurlA;
				}
			}
			$GLOBALS['page']->set('rurl',$rurlAs);
		}
		
		$getIndex = false;
		
		// First we load the master page
		if(!empty($rurl)) {
			$page = mysql_fetch_array(mysql_query("SELECT b.master_template_path, a.template_path FROM 
													pages AS a
													INNER JOIN templates_master AS b 
													ON b.id = a.master_template 
													WHERE url = '{$rurl}'"));
			
			if(!empty($page['master_template_path'])) {
				// Load the page
				$this->masterPage = file_get_contents(ROOT_PATH . $page['master_template_path']);
				$this->templatePage = file_get_contents(ROOT_PATH  . $page['template_path']);
			}
			else {
				// There was no match for this url, lets chop it down and see if we can find a parent
				$rurlArray = explode("/",$rurl);
				// Now lets build the different urls backwards
				// For example: /events/event/1337
				// Or: 			/events/event/1337/edit
				// We want to load the /events/event page, so we try removing the last words "1337" and "edit"
				// to find a matching page, and then pass them on as a rest URL to be used by the /events/event page, as variables.
				$match = false;
				
				$i = 0;
				while($i <= count($rurlArray)) {
					if(!empty($rurlArray[$i])) {
						$temp = "";
						for($x = 0; $x <= $i; $x++) {
							if(!empty($rurlArray[$x])) {
								$temp[] = $rurlArray[$x];
							}
						}
						$urls[] = "/". implode("/",$temp);
					}
				$i++;
				}
				@$urls = array_reverse($urls);
				
				// Now lets see if we can find a parent page
				if(!empty($urls)) {
					foreach($urls as $url) {
						$page = mysql_fetch_array(mysql_query("SELECT b.master_template_path, a.template_path FROM 
																pages AS a
																INNER JOIN templates_master AS b 
																ON b.id = a.master_template 
																WHERE url = '{$url}'"));
				
						if(!empty($page['master_template_path'])) {
							$match = true;
							// Load the page
							$this->masterPage = file_get_contents(ROOT_PATH  . $page['master_template_path']);
							$this->templatePage = file_get_contents(ROOT_PATH  . $page['template_path']);
							
							// Lets create the rest url
							$restUrl = explode("/",str_replace($url,"",$rurl));
							foreach($restUrl as $ru) {
								if(!empty($ru)) {
									$this->restUrl[] = $ru;
								}
							}
							
							break;
						}
					}
				}
				
				if(!$match) {
					$getIndex = true;
				}
			}
		}
		else {
			$getIndex = true;
		}
		
		// Load the index page
		if(empty($this->masterPage) && $getIndex) {
			$page = mysql_fetch_array(mysql_query("SELECT b.master_template_path, a.template_path FROM 
													pages AS a
													INNER JOIN templates_master AS b 
													ON b.id = a.master_template 
													WHERE is_index = 'true'"));
			$this->masterPage = file_get_contents(ROOT_PATH . $page['master_template_path']);
			$this->templatePage = file_get_contents(ROOT_PATH . $page['template_path']);
		}
		
		// Then we process the subpage
		$GLOBALS['parser']->parse($this->templatePage);
		
		// Finally we process the masterpage and implement the sub template
		$this->html = $GLOBALS['parser']->parse($this->masterPage);
	}
	
	public function clearHtml() {
		$this->html = preg_replace('/^[ \t]*[\r\n]+/m', '', $this->html);
	}
	
	public function html() {
		return $this->html;
	}
	
	public function getFilePath() {
		return $this->filePath;
	}
	
	public function filePath($path) {
		$this->filePath = $path;
	}
	
	public function url_friendly($string) {
		$url = str_replace("ö","o",strtolower($string));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("ä","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}

}

?>