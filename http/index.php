<?php

	/* Set locale to Dutch */
	setlocale(LC_ALL, 'sv_SE.utf8');
	header('Content-type: text/html; charset=UTF-8');

	include("includes/config.php");
	include("includes/class_database.php");
	include("includes/class_parser.php");
	include("includes/class_account.php");
	include("includes/class_permissions.php");
	include("includes/class_template.php");
	include("includes/class_debugger.php");
	include("includes/class_page.php");
	include("includes/class_image.php");
	include("includes/class_mail.php");
	include("includes/class_pager.php");
	include("includes/class_text.php");
	
	$database = new database();
	$account = new account();
	$permissions = new permissions();
	$page = new page();
	$debugger = new debugger();
	$parser = new parser();
	$template = new template();
	$image = new image();
	$mail = new mail();
	$pager = new pager();
	$text = new text();
	
	// First we escape all GLOBALS
	$database->escape_all();

	// Make sure we always visit the www domain
	/*
	if(substr($_SERVER['SERVER_NAME'], 0, 3) != "www") {
		header("Location: http://www.".SERVER_NAME.$_SERVER['REQUEST_URI']);
	}
	*/

	// get IP of visitor
	$ipLock = false;
	$ip = $_SERVER['REMOTE_ADDR'];
	$allowed['daniel'] = "2.64.38.159";
	$allowed['andreas'] = "46.11.91.192";
	
	if(!in_array($ip,$allowed) && $ipLock) {
		exit("The website has been IP locked. (Your IP: {$ip})");
	}

	$showErrors = (isset($_GET['debug'])) ? true : false;

	if(!$showErrors) {
		ini_set('display_errors', 'Off');
	}
	else {
		error_reporting(E_ALL);
		ini_set('display_errors', 'On');
	}
	
	// Set paths
	$page->set("internal_path",str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']));
	$page->set("external_path","http://www.".SERVER_NAME);
	
	if(!$showErrors) {
		// Set error handler
		set_error_handler("customError", E_ALL);
	}
	
	// Error handler function
	function customError($errNo="", $errStr="", $errFile="File", $errLine="0") {
		$GLOBALS['debugger']->add("[{$errFile}:{$errLine}] $errStr",$errNo);
	}
	
	// Lets get the page
	$template->filePath(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']));
	
	// First we check if the URL is an old URL and should be redirected permanently
	$template->check_redirect_from_old_website();
	// Otherwise we load the new website
	$template->load();
	
	
	$template->clearHtml(); // clears the html code from empty rows
	
	if(isset($_GET['debug'])) {
		@$warnings = $GLOBALS['debugger']->get();

		if(!empty($warnings)) {
			echo '<div style="position:absolute;top:0px;left:0px;z-index:999999999;width:100%;background-color:#ffffff;padding:10px;min-height:100px;">';
			
			foreach($warnings as $warning) {
				echo $warning['text'] ."<br /><br />";
			}
			echo "</div>";
		}
	}
	
	echo $parser->parse($template->html());

	// Close the database
	$database->close();
?>