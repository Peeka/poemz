<?php

class parser_debugger {

	public $websiteName = "";
	public $log = "";

	function __construct() {
	
	}
	
	public function parser_logs($tag, $attr) {
		$html = "";
		$logs = $GLOBALS['debugger']->get();
		foreach($logs as $log) {
			$this->log = $log;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}

	return $html;
	}
	
	public function parser_run($tag, $attr) {
		$html = "";
		if(account::user('developer') == "true") {
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}

	return $html;
	}
	
	public function parser_log($tag, $attr) {
		return $this->log[$attr['get']];
	}

}

?>