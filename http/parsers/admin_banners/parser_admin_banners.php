<?php

class parser_admin_banners {

	private $item = array();
	private $subitem = array();
	private $rurl = array();
	private $category = "";
	private $message = "";
	private $has_images=false;
	private $has_movies=false;
    private $loaded_img=0;
    private $loaded_movie=0;
	private $real_banner_edited = false;
	private $current_company_id = 0;
	private $company_name = "";
	

	function __construct() {
		// Check if we want to create a new banner
		if(isset($_POST['banner_name']) && !isset($_POST['edit_real_banner'])) {
			// Create the banner
			$start_date = strtotime($_POST['banner_start_date']);
			$start_date = "$start_date";
			$end_date = strtotime($_POST['banner_end_date']);
			$end_date = "$end_date";
			$end_date_ongoing = (isset($_POST['banner_end_date_ongoing'])) ? 'true' : 'false';
			$url = $this->fixUrl($_POST['banner_url']);
			
			mysql_query("INSERT INTO banners SET 
							name = '{$_POST['banner_name']}', 
							url = '{$url}', 
							company_id = '{$_POST['banner_company']}', 
							size_profile = '{$_POST['banner_size_profile']}', 
							size_override_width = '". intval($_POST['banner_override_width']) ."', 
							size_override_height = '". intval($_POST['banner_override_height']) ."' 
						");
						
			$id = mysql_insert_id();
		
			// Upload the flash banner
			@$allowedExts = array("swf");
			@$extension = end(explode(".", $_FILES["banner_flash"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_branschguiden_flash_".$id;
				
				if(file_exists(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				// Create the flash
				if(move_uploaded_file($_FILES["banner_flash"]["tmp_name"], INTERNAL_PATH ."public/images/advertisement/". $newFileBig)) {
					mysql_query("UPDATE banners SET flash = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			// upload the image
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["banner_fallback"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_branschguiden_fallback_".$id;
				
				if(file_exists(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the image
				if(move_uploaded_file($_FILES["banner_fallback"]["tmp_name"], INTERNAL_PATH ."public/images/advertisement/". $newFileBig)) {
					mysql_query("UPDATE banners SET fallback = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
		}
		else if(isset($_GET['delete_active_banner'])) {
			mysql_query("DELETE FROM banners_active WHERE id = '{$_GET['delete_active_banner']}'");
		}
		else if(isset($_POST['edit_real_banner'])) {
			// Create the banner
			$start_date = strtotime($_POST['banner_start_date']);
			$start_date = "$start_date";
			$end_date = strtotime($_POST['banner_end_date']);
			$end_date = "$end_date";
			$end_date_ongoing = (isset($_POST['banner_end_date_ongoing'])) ? 'true' : 'false';
			$id = $_POST['banner_id'];
			$url = $this->fixUrl($_POST['banner_url']);
			
			mysql_query("UPDATE banners SET 
							name = '{$_POST['banner_name']}', 
							url = '{$url}', 
							company_id = '{$_POST['banner_company']}', 
							size_profile = '{$_POST['banner_size_profile']}', 
							size_override_width = '". intval($_POST['banner_override_width']) ."', 
							size_override_height = '". intval($_POST['banner_override_height']) ."' 
						WHERE id = '{$id}'
						");
		
			// Upload the flash banner
			@$allowedExts = array("swf");
			@$extension = end(explode(".", $_FILES["banner_flash"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_branschguiden_flash_".$id;
				
				if(file_exists(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				// Create the flash
				if(move_uploaded_file($_FILES["banner_flash"]["tmp_name"], INTERNAL_PATH ."public/images/advertisement/". $newFileBig)) {
					mysql_query("UPDATE banners SET flash = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			// upload the image
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["banner_fallback"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_branschguiden_fallback_".$id;
				
				if(file_exists(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the image
				if(move_uploaded_file($_FILES["banner_fallback"]["tmp_name"], INTERNAL_PATH ."public/images/advertisement/". $newFileBig)) {
					mysql_query("UPDATE banners SET fallback = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			$this->real_banner_edited = true;
		}
		else if(isset($_GET['delete_page'])) {
			mysql_query("DELETE FROM banners_pages WHERE id = '{$_GET['delete_page']}'");
		}
		else if(isset($_GET['delete_banner'])) {
			@$banner = mysql_fetch_array(mysql_query("SELECT * FROM banners WHERE id = '{$_GET['delete_banner']}'"));
			unlink(INTERNAL_PATH."public/images/advertisement/".$banner['flash']);
			unlink(INTERNAL_PATH."public/images/advertisement/".$banner['fallback']);
			mysql_query("DELETE FROM banners_active WHERE banner_id = '{$_GET['delete_banner']}'");
			mysql_query("DELETE FROM banners WHERE id = '{$_GET['delete_banner']}'");
		}
	}
	
	public function fixUrl($url) {
		if(strstr($url,'.se')) {
			$url = strstr($url,'.se');
			$url = str_replace('.se','',$url);
		}
		else if(strstr($url,'.no')) {
			$url = strstr($url,'.no');
			$url = str_replace('.no','',$url);
		}
		return $url;
	}
	
	public function parser_check_page_add($tag, $attr) {
		if(isset($_POST['page_name'])) {
			$url = $this->fixUrl($_POST['page_url']);
			mysql_query("INSERT INTO banners_pages SET 
							name = '{$_POST['page_name']}', 
							url = '{$url}', 
							type = '{$_POST['page_type']}'
						") or die(mysql_error());
		}
	}
	
	public function parser_real_banner_edited($tag, $attr) {
		if($this->real_banner_edited) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return "";
		}
	}
	
	public function parser_size_profiles($tag, $attr) {
		// Load the size_profiles
		$html = "";
		@$size_profiles = mysql_query("SELECT * FROM banners_size_profiles ORDER BY name ASC");
		
		while($profile = mysql_fetch_array($size_profiles)) {
			$this->item = $profile;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		return $html;
	}
	
	public function parser_edit_banner($tag, $attr) {
		// Load the banner
		@$banner_id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
		if(isset($banner_id) && !empty($banner_id)) {
			@$banner = mysql_fetch_array(mysql_query("SELECT * FROM banners WHERE id = '{$banner_id}'"));
			$this->subitem = $banner;
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return "";
		}
	}
	
	public function parser_banner_companies($tag, $attr) {
		if(isset($_GET['parent']) && !empty($_GET['parent']) || isset($attr['show']) && $attr['show'] == "all") {
			// Load the banners who have no company
			if(isset($attr['size_profile']) && !empty($attr['size_profile'])) {
				@$banners = mysql_query("SELECT * FROM banners 
										WHERE company_id = 0 
										AND size_profile = '{$attr['size_profile']}' 
										ORDER BY name ASC");
			}
			else {
				@$banners = mysql_query("SELECT * FROM banners 
										WHERE company_id = 0 
										ORDER BY name ASC");
			}
										
			while($banner = mysql_fetch_array($banners)) {
				$bannerComps[99999999][] = $banner;
			}
			
			// Then we load each banner for the companies
			foreach($bannerComps as $company_id => $banners) {
				foreach($banners as $banner) {
					$this->company_id = $company_id;
					$this->company_name = "Inget företag";
					$this->item = $banner;
					$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
			
			$bannerComps = array();
			
			// Load the companies who have a banner
			if(isset($attr['size_profile']) && !empty($attr['size_profile'])) {
				@$banners = mysql_query("SELECT a.*, b.id AS company_id, b.name AS company_name FROM banners AS a 
											INNER JOIN 
												arbetsgivare_companies AS b 
											ON a.company_id = b.id 
											WHERE a.size_profile = '{$attr['size_profile']}' 
											ORDER BY b.name ASC");
			}
			else {
				@$banners = mysql_query("SELECT a.*, b.id AS company_id, b.name AS company_name FROM banners AS a 
										INNER JOIN 
											arbetsgivare_companies AS b 
										ON a.company_id = b.id 
										ORDER BY b.name ASC");
			}
										
			while($banner = mysql_fetch_array($banners)) {
				$bannerComps[$banner['company_id']][] = $banner;
			}
			
			// Then we load each banner for the companies
			foreach($bannerComps as $company_id => $banners) {
				foreach($banners as $banner) {
					$this->company_id = $company_id;
					$this->company_name = $banner['company_name'];
					$this->item = $banner;
					$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
		}
		else {
			$this->company_id = 99999999;
			$this->company_name = "Ingen kategori vald.";
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_size_profile($tag, $attr) {
		// Load the banner size profile
		@$banner = mysql_fetch_array(mysql_query("SELECT * FROM banners WHERE id = '{$attr['id']}'"));
		@$profile = mysql_fetch_array(mysql_query("SELECT * FROM banners_size_profiles WHERE id = '{$banner['size_profile']}'"));
		return $profile[$attr['get']];
	}
	
	public function parser_get_company($tag, $attr) {
		// Load the company
		@$banner = mysql_fetch_array(mysql_query("SELECT * FROM banners WHERE id = '{$attr['id']}'"));
		@$company = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$banner['company_id']}'"));
		return $company[$attr['get']];
	}
	
	public function parser_companies($tag, $attr) {
		// Load the companies
		@$companies = mysql_query("SELECT * FROM arbetsgivare_companies ORDER BY name ASC");
		while($company = mysql_fetch_array($companies)) {
			$this->item = $company;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_banner_company($tag, $attr) {
		if($this->current_company_id != $this->company_id) {
			$this->current_company_id = $this->company_id;
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_company_id($tag, $attr) {
		return $this->company_id;
	}
	
	public function parser_company_name($tag, $attr) {
		return $this->company_name;
	}
	
	public function parser_parent($tag, $attr) {
		return $_GET['parent'];
	}

	public function parser_sub_category_selected($tag, $attr) {
		if($this->subCategorySelected) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_parent_category($tag, $attr) {
		$this->item = '';
		if(isset($_GET['type']) && $_GET['type'] == "branschguiden") {
			if(isset($_GET['parent']) && !empty($_GET['parent'])) {
				@$parent = mysql_fetch_array(mysql_query("SELECT name FROM branschguiden_categories WHERE id = '{$_GET['parent']}'"));
				$this->item = $parent;
			}
			else {
				$this->item['name'] = "Ingen kategori vald";
			}
		}
		else if(isset($_GET['type']) && $_GET['type'] == "top-big") {
			if(isset($_GET['parent']) && !empty($_GET['parent'])) {
				@$parent = mysql_fetch_array(mysql_query("SELECT name FROM banners_pages WHERE id = '{$_GET['parent']}'"));
				$this->item = $parent;
			}
			else {
				$this->item['name'] = "Ingen sida vald";
			}
		}
		else if(isset($_GET['type']) && $_GET['type'] == "right-small") {
			if(isset($_GET['parent']) && !empty($_GET['parent'])) {
				@$parent = mysql_fetch_array(mysql_query("SELECT name FROM banners_pages WHERE id = '{$_GET['parent']}'"));
				$this->item = $parent;
			}
			else {
				$this->item['name'] = "Ingen sida vald";
			}
		}
		else if(isset($_GET['type']) && $_GET['type'] == "right-big") {
			if(isset($_GET['parent']) && !empty($_GET['parent'])) {
				@$parent = mysql_fetch_array(mysql_query("SELECT name FROM banners_pages WHERE id = '{$_GET['parent']}'"));
				$this->item = $parent;
			}
			else {
				$this->item['name'] = "Ingen sida vald";
			}
		}
		else if(isset($_GET['type']) && $_GET['type'] == "startpage-small") {
			if(isset($_GET['parent']) && !empty($_GET['parent'])) {
				@$parent = mysql_fetch_array(mysql_query("SELECT name FROM banners_pages WHERE id = '{$_GET['parent']}'"));
				$this->item = $parent;
			}
			else {
				$this->item['name'] = "Ingen sida vald";
			}
		}
		return $GLOBALS['parser']->parse($tag['innerhtml']);
	}
	
	public function parser_column_update($tag, $attr) {
		if(isset($_GET['column'])) {
		
			if($_GET['column'] == 1) {
				mysql_query("UPDATE branschguiden_categories SET on_startpage = 'true' WHERE id = '{$_GET['add']}'");
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else if($_GET['column'] == 2) {
				mysql_query("UPDATE branschguiden_categories SET on_startpage = 'false' WHERE id = '{$_GET['remove']}'");
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else if($_GET['column'] == 3) {
				@$company_id = $GLOBALS['parser']->parse("{page:rurl index='6'/}");
				if(!empty($company_id)) {
					@$company = mysql_fetch_array(mysql_query("SELECT categories FROM arbetsgivare_companies WHERE id = '{$company_id}'"));
					@$company_categories = $company['categories'];
					if(strstr($company_categories,"[{$_GET['toggle']}]")) {
						$company_categories = str_replace("[{$_GET['toggle']}]","",$company_categories);
					}
					else {
						$company_categories .= "[{$_GET['toggle']}]";
					}
					mysql_query("UPDATE arbetsgivare_companies SET categories = '{$company_categories}' WHERE id = '{$company_id}'");
				}
				else {
					@$get = mysql_fetch_array(mysql_query("SELECT on_startpage FROM branschguiden_categories WHERE id = '{$_GET['toggle']}'"));
					if($get['on_startpage'] == "true") {
						mysql_query("UPDATE branschguiden_categories SET on_startpage = 'false' WHERE id = '{$_GET['toggle']}'");
					}
					else {
						mysql_query("UPDATE branschguiden_categories SET on_startpage = 'true' WHERE id = '{$_GET['toggle']}'");
					}
				}
			}
		
		}
	}
	
	public function parser_column($tag, $attr) {
		$html = "";
		if($attr['index'] == 5) {
			// Load all banner pages
			@$pages = mysql_query("SELECT * FROM banners_pages ORDER BY name ASC");
			while($page = mysql_fetch_array($pages)) {
				$this->item = $page;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if($attr['index'] == 4) {
			// Load all main categories
			@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '0' ORDER BY name ASC");
			while($category = mysql_fetch_array($categories)) {
				$this->item = $category;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if($attr['index'] == 1) {
			// Load the main categories who are not on the firstpage of branschguiden
			@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '0' ORDER BY name ASC");
			while($category = mysql_fetch_array($categories)) {
				$this->item = $category;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if($attr['index'] == 2) {
			// Load the banners
			@$banners = mysql_query("SELECT * FROM banners WHERE type = 'branschguiden' ORDER BY name ASC");
			while($banner = mysql_fetch_array($banners)) {
				$this->item = $banner;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if($attr['index'] == 3) {
			$parent = (isset($attr['parent'])) ? $attr['parent'] : $_GET['parent'];
			$type = (isset($attr['parent'])) ?  $attr['type'] : $_GET['type'];
			
			if(isset($parent) && !empty($parent)) {
				@$banners = mysql_query("SELECT * FROM banners_active WHERE category_id = '{$parent}'");
				if(mysql_num_rows($banners) > 0) {
					while($banner = mysql_fetch_array($banners)) {
						$this->subCategorySelected = ($banner['on_startpage'] == "true") ? true : false;
						$this->item = $banner;
						$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
					}
				}
				else {
					$html .= $GLOBALS['parser']->parse($attr['empty']);
				}
			}
			else {
				$html .= $GLOBALS['parser']->parse($attr['start']);
			}
		}
		
		return $html;
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			$get = $this->subitem[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}
	
	
}

?>