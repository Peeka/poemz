<?php

class parser_sok {

	private $search_in_check = array();
	private $results = array();
	private $item = array();
	private $total = 0;
	private $q = '';
	private $search_in = array();
	private $sok_text_length = 120;

	function __construct() {
	
		if(isset($_POST['search']) && !empty($_POST['search']) || isset($_GET['q']) && !empty($_GET['q'])) {
			// Prepare the search
			$this->init();
			// Retrieve the search query
			$this->q = (isset($_POST['search']) && !empty($_POST['search'])) ? mysql_real_escape_string($_POST['search']) : mysql_real_escape_string($_GET['q']);
			// Check if we want to expand the search, if so reset the "search_in" variable
			if(isset($_GET['expand_search'])) {
				unset($_GET['search_in']);
			}
			// Search
			$this->search();
			// Save the search text
			$this->save_search();
		}
	}
	
	public function init() {

		if(isset($_GET['search_in_h']) && !empty($_GET['search_in_h'])) {
			$this->search_in[] = mysql_real_escape_string($_GET['search_in_h']);
		}
		if(isset($_GET['search_in_b']) 
				|| isset($_GET['search_in_k']) 
				|| isset($_GET['search_in_j']) 
				|| isset($_GET['search_in_n']) 
				|| isset($_GET['search_in_f']) 
				|| isset($_GET['search_in_ka']) 
				|| isset($_GET['search_in'])
				) {
			if(isset($_GET['search_in_b']) && !empty($_GET['search_in_b'])) {
				$this->search_in[] = mysql_real_escape_string($_GET['search_in_b']);
			}
			if(isset($_GET['search_in_k']) && !empty($_GET['search_in_k'])) {
				$this->search_in[] = mysql_real_escape_string($_GET['search_in_k']);
			}
			if(isset($_GET['search_in_j']) && !empty($_GET['search_in_j'])) {
				$this->search_in[] = mysql_real_escape_string($_GET['search_in_j']);
			}
			if(isset($_GET['search_in_n']) && !empty($_GET['search_in_n'])) {
				$this->search_in[] = mysql_real_escape_string($_GET['search_in_n']);
			}
			if(isset($_GET['search_in_f']) && !empty($_GET['search_in_f'])) {
				$this->search_in[] = mysql_real_escape_string($_GET['search_in_f']);
			}
			if(isset($_GET['search_in_ka']) && !empty($_GET['search_in_ka'])) {
				$this->search_in[] = mysql_real_escape_string($_GET['search_in_ka']);
			}
			
			$searchIn = explode(":",$_GET['search_in']);
			foreach($searchIn as $si) {
				if(!in_array($si, $this->search_in)) {
					$this->search_in[] = mysql_real_escape_string($si);
				}
			}
		}

	}
	
	public function parser_get_search_in($tag, $attr) {
		if(in_array($attr['check'], $this->search_in)) {
			return $attr['return'];
		}
		else if($attr['check'] == "hela_siten" && empty($this->search_in)) {
			return $attr['return'];
		}
	}
	
	public function save_search() {
		$ip = mysql_real_escape_string($_SERVER["REMOTE_ADDR"]);
		$search_in = implode(":",$this->search_in);
		$search_at = time();
		$search_at_new = (60*60*24);
		$check = mysql_fetch_array(mysql_query("SELECT id,text,number_of_results,search_at FROM search_texts WHERE text = '{$this->q}' AND ip = '{$ip}' AND search_in = '{$search_in}' ORDER BY search_at DESC"));
		
		if(!empty($check['text']) && time() < ($check['search_at'])+$search_at_new) {
			if($check['number_of_results'] == $this->total && time() < ($check['search_at'])+$search_at_new) {
				mysql_query("UPDATE search_texts SET amount = amount + 1 WHERE id = '{$check['id']}'");
			}
			else {
				mysql_query("INSERT INTO search_texts SET text = '{$this->q}', ip = '{$ip}', number_of_results = '{$this->total}',  search_in = '{$search_in}',  search_at = '{$search_at}'");
			}
		}
		else {
			mysql_query("INSERT INTO search_texts SET text = '{$this->q}', ip = '{$ip}', number_of_results = '{$this->total}',  search_in = '{$search_in}',  search_at = '{$search_at}'");
		}
	}
	
	public function search_in_branschguiden() {
		$sqlSuffix = '';
	
		// Get all affected categories
		$categories = mysql_query("SELECT id FROM branschguiden_categories WHERE name LIKE '%{$this->q}%'");
		while($c = mysql_fetch_array($categories)) {
			$sqlSuffix .= " OR categories LIKE '%[{$c['id']}]%'";
		}
		
		// Get all affected companay products
		$products = mysql_query("SELECT company_id FROM arbetsgivare_products WHERE title LIKE '%{$this->q}%' OR text LIKE '%{$this->q}%'");
		while($p = mysql_fetch_array($products)) {
			$sqlSuffix .= " OR id = '{$p['company_id']}'";
		}
		
		// Retrieve the affected companies
		$sql = "SELECT id,name,description,url_friendly,image_thumb FROM arbetsgivare_companies WHERE name LIKE '%{$this->q}%' AND type != 'hidden' OR description LIKE '%{$this->q}%' AND type != 'hidden' OR tags LIKE '%[{$this->q}]%' AND type != 'hidden'{$sqlSuffix}";
		$companies = mysql_query($sql);
		while($comps = mysql_fetch_array($companies)) {
			$result['type'] = "Företag";
			$result['title'] = $comps['name'];

			// Break all words
			$sentance = "";
			$words = explode(" ",strip_tags($comps['description']));
			$add_word = true;
			$i = 0;
			while($add_word) {
				$sentance .= ($i == 0) ? $words[$i] : " ". $words[$i];
				$add_word = (strlen($sentance) < $this->sok_text_length) ? true : false;
			$i++;
			}
			$result['text'] = $sentance.$attr['add']."...";
			
			$result['url'] = "branschguiden/leverantor/{$comps['url_friendly']}";
			$result['image'] = "/public/images/companies/".$comps['image_thumb'];
			$result['id'] = $comps['id'];
			
			$this->results[] = $result;
			$this->total=$this->total+1;
		}
	}
	
	public function search_in_platsbanken() {
		$jobb = mysql_query("SELECT id,title,description FROM arbetsgivare_jobb_annonser WHERE 
							title LIKE '%{$this->q}%' AND status = 'accepted' 
							OR intro LIKE '%{$this->q}%' AND status = 'accepted' 
							OR description LIKE '%{$this->q}%' AND status = 'accepted'
							");
		
		while($j = mysql_fetch_array($jobb)) {
			$result['type'] = "Platsbanken";
			$result['title'] = $j['title'];
			// Break all words
			$sentance = "";
			$words = explode(" ",strip_tags($j['description']));
			$add_word = true;
			$i = 0;
			while($add_word) {
				$sentance .= ($i == 0) ? $words[$i] : " ". $words[$i];
				$add_word = (strlen($sentance) < $this->sok_text_length) ? true : false;
			$i++;
			}
			$result['text'] = $sentance.$attr['add']."...";
			
			$urlFriendly = $GLOBALS['parser']->parse("{functions:url_friendly string='{$j['title']}'/}");
			$result['url'] = "/platsbanken/jobb-annons/{$j['id']}/{$urlFriendly}";
			
			$this->results[] = $result;
			$this->total=$this->total+1;
		}
	}
	
	public function search_in_kop_salj() {
		$kop = mysql_query("SELECT id,title FROM kop_salj WHERE 
							title LIKE '%{$this->q}%' AND status = 'accepted' 
							OR description LIKE '%{$this->q}%' AND status = 'accepted'
							");
		
		while($k = mysql_fetch_array($kop)) {
			$result['type'] = "Köp & Sälj";
			$result['title'] = $k['title'];
			
			// Break all words
			$sentance = "";
			$words = explode(" ",strip_tags($k['description']));
			$add_word = true;
			$i = 0;
			while($add_word) {
				$sentance .= ($i == 0) ? $words[$i] : " ". $words[$i];
				$add_word = (strlen($sentance) < $this->sok_text_length) ? true : false;
			$i++;
			}
			$result['text'] = $sentance.$attr['add']."...";
			
			$urlFriendly = $GLOBALS['parser']->parse("{functions:url_friendly string='{$k['title']}'/}");
			$result['url'] = "/till-salu/{$k['id']}/{$urlFriendly}";
			
			$this->results[] = $result;
			$this->total=$this->total+1;
		}
	}
	
	public function search_in_nyheter() {
		$nyheter = mysql_query("SELECT id,title,breadcrumb,image_mini_thumb FROM arbetsgivare_branschnyheter WHERE 
							title LIKE '%{$this->q}%' AND status = 'accepted' 
							OR breadcrumb LIKE '%{$this->q}%' AND status = 'accepted' 
							OR text_1 LIKE '%{$this->q}%' AND status = 'accepted' 
							OR text_2 LIKE '%{$this->q}%' AND status = 'accepted' 
							");
		
		while($n = mysql_fetch_array($nyheter)) {
			$result['type'] = "Nyheter";
			$result['title'] = $n['title'];
			
			// Break all words
			$sentance = "";
			$words = explode(" ",strip_tags($n['breadcrumb']));
			$add_word = true;
			$i = 0;
			while($add_word) {
				$sentance .= ($i == 0) ? $words[$i] : " ". $words[$i];
				$add_word = (strlen($sentance) < $this->sok_text_length) ? true : false;
			$i++;
			}
			$result['text'] = $sentance.$attr['add']."...";
			
			$urlFriendly = $GLOBALS['parser']->parse("{functions:url_friendly string='{$n['title']}'/}");
			$result['url'] = "/nyheter/{$n['id']}/{$urlFriendly}";
			$result['image'] = "/public/images/branschnyheter/{$n['image_mini_thumb']}";
			
			$this->results[] = $result;
			$this->total=$this->total+1;
		}
	}
	
	public function search_in_forum() {
		$forum = mysql_query("SELECT id,title,text FROM forum WHERE 
							title LIKE '%{$this->q}%' AND accepted = '1' AND active = '1' 
							OR text LIKE '%{$this->q}%' AND accepted = '1' AND active = '1' 
							");
		
		while($f = mysql_fetch_array($forum)) {
			$result['type'] = "Forum";
			$result['title'] = $f['title'];
			
			// Break all words
			$sentance = "";
			$words = explode(" ",strip_tags($f['text']));
			$add_word = true;
			$i = 0;
			while($add_word) {
				$sentance .= ($i == 0) ? $words[$i] : " ". $words[$i];
				$add_word = (strlen($sentance) < $this->sok_text_length) ? true : false;
			$i++;
			}
			$result['text'] = $sentance.$attr['add']."...";
				
			$urlFriendly = $GLOBALS['parser']->parse("{functions:url_friendly string='{$f['title']}'/}");
			$result['url'] = "/forum/list/{$f['id']}";
			
			$this->results[] = $result;
			$this->total=$this->total+1;
		}
	}
	
	public function search_in_kalender() {
		$kalender = mysql_query("SELECT id,title,text FROM kalender WHERE 
							title LIKE '%{$this->q}%' 
							OR text LIKE '%{$this->q}%' 
							");
		
		while($ka = mysql_fetch_array($kalender)) {
			$result['type'] = "Forum";
			$result['title'] = $ka['title'];
			
			// Break all words
			$sentance = "";
			$words = explode(" ",strip_tags($ka['text']));
			$add_word = true;
			$i = 0;
			while($add_word) {
				$sentance .= ($i == 0) ? $words[$i] : " ". $words[$i];
				$add_word = (strlen($sentance) < $this->sok_text_length) ? true : false;
			$i++;
			}
			$result['text'] = $sentance.$attr['add']."...";
				
			$urlFriendly = $GLOBALS['parser']->parse("{functions:url_friendly string='{$ka['title']}'/}");
			$result['url'] = "/forum/list/{$ka['id']}";
			
			$this->results[] = $result;
			$this->total=$this->total+1;
		}
	}
	
	public function search($tag, $attr) {
		$html = '';
		
		// Perform the different searches depending on type of search
		if(!empty($this->search_in) && !in_array("hela_siten", $this->search_in)) {
			foreach($this->search_in as $si) {
				$func = "search_in_{$si}";
				if(method_exists("parser_sok",$func)) {
					$this->$func();
				}
			}
		}
		else {
			// No specific type of search, so lets search everywhere
			$this->search_in_branschguiden();
			$this->search_in_kop_salj();
			$this->search_in_platsbanken();
			$this->search_in_nyheter();
			$this->search_in_forum();
			$this->search_in_kalender();
		}
	}
	
	public function parser_results($tag, $attr) {
		$html = "";
		foreach($this->results as $result) {
			$this->item = $result;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_has_image($tag, $attr) {
		if(!empty($this->item['image']) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($this->item['image']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_not_last_item($tag, $attr) {
		if(end($this->results) != $this->item) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_total($tag, $attr) {
		return $this->total;
	}
	
	public function parser_search_string($tag, $attr) {
		if(isset($attr['not_empty']) && !empty($this->q)) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $this->q;
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut'])) {
				// Break all words
				$sentance = "";
				$words = explode(" ",$this->item[$attr['get']]);
				$add_word = true;
				$i = 0;
				while($add_word) {
					$sentance .= $words[$i];
					$add_word = (strlen($sentance) < $attr['cut']) ? true : false;
				}
				return $sentance.$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

}

?>