<?php

class parser_platsbanken_add_advert {

  	public $item = array();
	public $search_parameter = "";
	
    function __construct() {
        
	}

    public function parser_list($tag, $attr) {
		$html = '';

        if (isset($attr['type'])) {
          
			$sql = "SELECT aja.id,aja.title,ac.name as company_name,from_unixtime(aja.added) as date_added,aja.status 
					FROM arbetsgivare_jobb_annonser AS aja
					INNER JOIN arbetsgivare_companies ac ON (ac.id=aja.company_id)
					WHERE f.status = '" . $attr['type'] . "'";
			
			//search filter
			if(isset($_POST['search_forum']) && !empty($_POST['search_forum'])) {
				$sql .= " AND ( ( concat(a.first_name ,' ', a.last_name) LIKE '%{$_POST['search_forum']}%') OR ( f.title LIKE '%{$_POST['search_forum']}%')) ";
				
				$this->search_parameter = $_POST['search_forum'];
			}
			
			$sql .= " ORDER BY aja.id DESC";
		
            $jobs_query = mysql_query($sql);

			if ($jobs_query) {
				while($job = mysql_fetch_array($jobs_query)) {
					
					$this->item = $job;
					$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
        }
    	return $html;
	}

    public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
}
?>