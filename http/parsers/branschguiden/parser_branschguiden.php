<?php

class parser_branschguiden {

	private $categories = array();
	private $category = array();
	private $item = array();
	private $subitem = array();
	private $parent = array();
	private $parentId = 0;
	private $companyId = 0;
	private $html = array();
	private $subCategorySelected = false;
	private $startpageCategories = array();
	private $companiesInCategory = array();
	private $totalCompaniesInCategory = array();
	private $div_class = "";
	private $this_tag = "";
	private $processed = false;

	function __construct() {
		// Check if we want to add a category
		if(isset($_POST['add']) && !empty($_POST['name'])) {
			// &nbsp; the url
			$url = $this->url_friendly($_POST['name']);
			mysql_query("INSERT INTO branschguiden_categories SET name = '{$_POST['name']}', parent = '{$_POST['parent']}', current_url = '{$url}'");
		}
		else if(isset($_POST['edit']) && !empty($_POST['name'])) {
			$old = mysql_fetch_array(mysql_query("SELECT current_url, previous_urls FROM branschguiden_categories WHERE id = '{$_POST['id']}'"));
			$oldUrl = (!empty($old['current_url'])) ? "[{$old['current_url']}]" : "";
			if(!strstr($old['previous_urls'],$oldUrl)) {
				$oldUrl = $old['previous_urls'] . $oldUrl;
			}
			// create the new url
			$url = $this->url_friendly($_POST['name']);
			mysql_query("UPDATE branschguiden_categories SET name = '{$_POST['name']}', current_url = '{$url}', previous_urls = '{$oldUrl}' WHERE id = '{$_POST['id']}'");
		}
		else if(isset($_GET['delete'])) {
			// Get all the children
			@$children = $this->get_children($_GET['delete']);
			// Delete the children
			if(!empty($children['children'])) {
				foreach($children['children'] as $child) {
					mysql_query("DELETE FROM branschguiden_categories WHERE id = '{$child['id']}'");
				}
			}
			// Then delete the parent
			mysql_query("DELETE FROM branschguiden_categories WHERE id = '{$_GET['delete']}'");
		}
		else if(isset($_GET['add_child'])) {
			// Add a child
			mysql_query("INSERT INTO branschguiden_categories SET name = '', parent = '{$_GET['add_child']}'");
		}
		
		
		// admin requests
		@$rurl = $GLOBALS['page']->get('rurl');
		if($rurl[3] == "accept") {
			mysql_query("UPDATE branschguiden_requests SET status = 'accepted' WHERE id = '{$rurl[4]}'");
		}
		else if($rurl[3] == "decline") {
			mysql_query("UPDATE branschguiden_requests SET status = 'declined' WHERE id = '{$rurl[4]}'");
		}
		
		
		// admin branschguiden category image
		if(isset($_POST['edit_category_image'])) {
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["category_image"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$id = $_POST['category_id'];
				$newName = "branschguiden_".$id;
				
				if(file_exists(INTERNAL_PATH."/public/images/branschguiden/". $newName . $extension)) {
					unlink(INTERNAL_PATH."/public/images/branschguiden/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the big image
				if(move_uploaded_file($_FILES["category_image"]["tmp_name"], INTERNAL_PATH."/public/images/branschguiden/". $newFileBig)) {
					image::resize(INTERNAL_PATH."/public/images/branschguiden/". $newFileBig,
										INTERNAL_PATH."/public/images/branschguiden/". $newFileBig,
										50,
										100);
				}
				mysql_query("UPDATE branschguiden_categories SET image = '{$newFileBig}' WHERE id = '{$id}'");
			}
		}
		
	}
	
	public function parser_has_image($tag, $attr) {
		if(isset($this->item['image']) && !empty($this->item['image']) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!isset($this->item['image']) && empty($this->item['image']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_parent_cat($tag, $attr) {
		@$cat = mysql_fetch_array(mysql_query("SELECT * FROM branschguiden_categories WHERE id = '{$attr['parent']}'"));
		return $cat[$attr['get']];
	}
	
	public function parser_is_child($tag, $attr) {
		if(isset($attr['get_parent'])) {
			return $this->subitem['parent'];
		}
		else if($this->subitem['parent'] > 0) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_sorted_company_categories($tag, $attr) {
		$html = "";
		$catz = array();
		
		@$categories = explode("]",$this->item['categories']);
		
		$where = "WHERE id = 0";
		
		foreach($categories as $category) {
			$id = str_replace("[","",$category);
			$where .= " OR id = '{$id}'";
		}
		
		@$cats = mysql_query("SELECT * FROM branschguiden_categories {$where} ORDER BY name ASC");
		
		while($catz = mysql_fetch_array($cats)) {
			$this->subitem = $catz;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_company_categories($tag, $attr) {
		$html = "";
		
		@$categories = explode("]",$this->item['categories']);
		foreach($categories as $category) {
			$id = str_replace("[","",$category);
			@$cat = mysql_fetch_array(mysql_query("SELECT * FROM branschguiden_categories WHERE id = '{$id}'"));
			$this->subitem = $cat;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_company_id($tag, $attr) {
		return $this->companyId;
	}
	
	public function parser_load_company($tag, $attr) {
		@$template = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE url_friendly = '{$attr['url_friendly']}'"));
		$this->item = $template;
		$this->companyId = $template['id'];
	}
	
	public function parser_email_template_update($tag, $attr) {
		if(isset($_POST['template_update'])) {
			mysql_query("UPDATE branschguiden_requests_emails_templates SET `title` = '{$_POST['template_title_1']}', `text` = '{$_POST['template_text_1']}' WHERE id = 1");
			mysql_query("UPDATE branschguiden_requests_emails_templates SET `title` = '{$_POST['template_title_2']}', `text` = '{$_POST['template_text_2']}' WHERE id = 2");
		}
	}
	
	public function parser_email_template($tag, $attr) {
		@$template = mysql_fetch_array(mysql_query("SELECT * FROM branschguiden_requests_emails_templates WHERE id = '{$attr['id']}'"));
		return $template[$attr['get']];
	}
	
	public function parser_check_request($tag, $attr) {
		if(isset($_POST['request_submit'])) {
			$companies = implode(",",$_POST['request_companies']);
			// First we save the request
			mysql_query("INSERT INTO branschguiden_requests SET
						`sent` = '". time() ."',
						`ip` = '". $_SERVER['REMOTE_ADDR'] ."',
						`title` = '". $_POST['request_title'] ."',
						`text` = '". $_POST['request_text'] ."',
						`name` = '". $_POST['request_name'] ."',
						`email` = '". $_POST['request_email'] ."',
						`company_name` = '". $_POST['request_company_name'] ."',
						`telephone` = '". $_POST['request_telephone'] ."',
						`company_type` = '". $_POST['request_company_type'] ."',
						`companies` = '". $companies ."'") or die(mysql_error());
						
		
			// We have a request to process
			@$templates = mysql_query("SELECT * FROM branschguiden_requests_emails_templates WHERE id = 1 OR id = 2");
			while($temp = mysql_fetch_array($templates)) {
				$template[$temp['id']]['title'] = $temp['title'];
				$template[$temp['id']]['text'] = $temp['text'];
			}
			
			// Now lets create the emails, and send them
			// First we make the companies mails, and get all companies emails
			$template[2]['title'] = str_replace("[text]",$_POST['request_text'],$template[2]['title']);
			$template[2]['title'] = str_replace("[name]",$_POST['request_name'],$template[2]['title']);
			$template[2]['title'] = str_replace("[email]",$_POST['request_email'],$template[2]['title']);
			$template[2]['title'] = str_replace("[company_name]",$_POST['request_company_name'],$template[2]['title']);
			$template[2]['title'] = str_replace("[telephone]",$_POST['request_telephone'],$template[2]['title']);
			$template[2]['title'] = str_replace("[company_type]",$_POST['request_company_type'],$template[2]['title']);
			$template[2]['title'] = str_replace("[title]",$_POST['request_title'],$template[2]['title']);
			$template[2]['text'] = str_replace("[text]",$_POST['request_text'],$template[2]['text']);
			$template[2]['text'] = str_replace("[name]",$_POST['request_name'],$template[2]['text']);
			$template[2]['text'] = str_replace("[email]",$_POST['request_email'],$template[2]['text']);
			$template[2]['text'] = str_replace("[company_name]",$_POST['request_company_name'],$template[2]['text']);
			$template[2]['text'] = str_replace("[telephone]",$_POST['request_telephone'],$template[2]['text']);
			$template[2]['text'] = str_replace("[company_type]",$_POST['request_company_type'],$template[2]['text']);
			$template[2]['text'] = str_replace("[title]",$_POST['request_title'],$template[2]['text']);
			
				// First we make the companies mails, and get all companies emails
			$template[1]['title'] = str_replace("[text]",$_POST['request_text'],$template[1]['title']);
			$template[1]['title'] = str_replace("[name]",$_POST['request_name'],$template[1]['title']);
			$template[1]['title'] = str_replace("[email]",$_POST['request_email'],$template[1]['title']);
			$template[1]['title'] = str_replace("[company_name]",$_POST['request_company_name'],$template[1]['title']);
			$template[1]['title'] = str_replace("[telephone]",$_POST['request_telephone'],$template[1]['title']);
			$template[1]['title'] = str_replace("[company_type]",$_POST['request_company_type'],$template[1]['title']);
			$template[1]['title'] = str_replace("[title]",$_POST['request_title'],$template[1]['title']);
			$template[1]['text'] = str_replace("[text]",$_POST['request_text'],$template[1]['text']);
			$template[1]['text'] = str_replace("[name]",$_POST['request_name'],$template[1]['text']);
			$template[1]['text'] = str_replace("[email]",$_POST['request_email'],$template[1]['text']);
			$template[1]['text'] = str_replace("[company_name]",$_POST['request_company_name'],$template[1]['text']);
			$template[1]['text'] = str_replace("[telephone]",$_POST['request_telephone'],$template[1]['text']);
			$template[1]['text'] = str_replace("[company_type]",$_POST['request_company_type'],$template[1]['text']);
			$template[1]['text'] = str_replace("[title]",$_POST['request_title'],$template[1]['text']);
			
			foreach($_POST['request_companies'] AS $company_id) {
				@$company = mysql_fetch_array(mysql_query("SELECT contact_mail FROM arbetsgivare_companies WHERE id = '{$company_id}'"));
				// Send the mail to the company
				$GLOBALS['mail']->send($company['contact_mail'],$template[1]['title'],$template[1]['text']);
			}
			
			// Now lets send the confirmation email to the visitor
			$GLOBALS['mail']->send($_POST['request_email'],$template[2]['title'],$template[2]['text']);
			
			$this->processed = true;
		}
	}
	
	public function parser_request_companies($tag, $attr) {
		$html = "";
		$companies = explode(",",$this->item['companies']);
		
		if(!isset($attr['all'])) {
			foreach($companies as $company) {
				@$comp1 = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$company}'"));
				if(isset($comp1['id']) && !empty($comp1['id'])) {
					$this->subitem = $comp1;
					$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
		}
		else {
			@$comps = mysql_query("SELECT * FROM arbetsgivare_companies ORDER BY name ASC");
			while($comp = mysql_fetch_array($comps)) {
				if(isset($comp['id']) && !empty($comp['id']) && !in_array($comp['id'],$companies)) {
					$this->subitem = $comp;
					$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
		}
		
	return $html;
	}
	
	public function parser_processed($tag, $attr) {
		if($this->processed) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_requests_total_items($tag, $attr) {
		$total = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total_items FROM branschguiden_requests"));
		return $total['total_items'];
	}
	
	public function parser_requests($tag, $attr) {
		$html = "";
		@$status = (isset($attr['status']) && !empty($attr['status'])) ? " WHERE status = '{$attr['status']}'" : "";
		$limit = " LIMIT ".$GLOBALS['pager']->limit();
	//	exit("SELECT * FROM branschguiden_requests{$status}{$limit} ORDER BY `sent` DESC");
		@$requests = mysql_query("SELECT * FROM branschguiden_requests{$status} ORDER BY `sent` DESC {$limit}");

		while($request = mysql_fetch_array($requests)) {
			$this->item = $request;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_companies_in_category($tag, $attr) {
		$html = "";
		
		// If we haven't loaded the companies within a category yet, lets do it!
		if(@empty($this->companiesInCategories[$attr['id']])) {
			
			// Check if this is a parent, then we load all the subcategories
			@$category = mysql_fetch_array(mysql_query("SELECT id,parent FROM branschguiden_categories WHERE id = '{$attr['id']}'"));
			if($category['parent'] == 0) {
				// It's a parent, let's load all sub categories
				@$subCategories = mysql_query("SELECT id FROM branschguiden_categories WHERE parent = '{$category['id']}'");
				while($subs = mysql_fetch_array($subCategories)) {
					$categories[] = $subs['id'];
				}
			}
			
			// Then we check for sub categories to sub categories
			foreach($categories as $cat) {
				// Load all sub categories
				@$subCategories = mysql_query("SELECT id FROM branschguiden_categories WHERE parent = '{$cat}'");
				while($subs = mysql_fetch_array($subCategories)) {
					$categories[] = $subs['id'];
				}
			}

			$count = 0;
			$search1 = " WHERE categories LIKE '%[{$attr['id']}]%' AND type != 'hidden' AND type = 'gold'";
			$search2 = " WHERE categories LIKE '%[{$attr['id']}]%' AND type != 'hidden' AND type = 'silver'";

			// Now we fix the sql search string
			foreach($categories as $cat) {
				$search1 .= " OR categories LIKE '%[{$cat}]%' AND type != 'hidden' AND type = 'gold'";
				$search2 .= " OR categories LIKE '%[{$cat}]%' AND type != 'hidden' AND type = 'silver'";
			}
			
			// GOLD
			@$companies = mysql_query("SELECT * FROM arbetsgivare_companies{$search1} ORDER BY name ASC");
			if(mysql_num_rows($companies) > 0) {
				while($company = mysql_fetch_array($companies)) {
					$count++;
					@$this->companiesInCategories[$attr['id']][] = $company;
				}
				@$this->totalCompaniesInCategory[$attr['id']] = $count;
			}
			
			// SILVER
			@$companies2 = mysql_query("SELECT * FROM arbetsgivare_companies{$search2} ORDER BY name ASC");
			if(mysql_num_rows($companies2) > 0) {
				while($company2 = mysql_fetch_array($companies2)) {
					$count2++;
					@$this->companiesInCategories[$attr['id']][] = $company2;
				}
				@$this->totalCompaniesInCategory[$attr['id']] = $count2;
			}
		}
		
		if(isset($this->companiesInCategories[$attr['id']]) && !empty($this->companiesInCategories[$attr['id']])) {
			foreach($this->companiesInCategories[$attr['id']] as $company) {
				$this->item = $company;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
			
		return $html;
	}
	
	public function parser_get_total_companies($tag, $attr) {
		return @$this->totalCompaniesInCategory[$attr['category']];
	}
	
	public function parser_from_url($tag, $attr) {
		if(isset($attr['id'])) {
			@$get = mysql_fetch_array(mysql_query("SELECT `{$attr['get']}`,`parent` FROM branschguiden_categories WHERE current_url = '{$attr['id']}'"));
			return isset($attr['lower']) ? strtolower($get[$attr['get']]) : $get[$attr['get']];
		}
		else {
			@$rurl = $GLOBALS['page']->get('rurl');
			if(!empty($attr['sub']) && !empty($rurl[3])) {
				if(!empty($rurl[3])) {
					@$get = mysql_fetch_array(mysql_query("SELECT `{$attr['get']}` FROM branschguiden_categories WHERE current_url = '". $rurl[3] ."'"));
					return isset($attr['lower']) ? strtolower($get[$attr['get']]) : $get[$attr['get']];
				}
			}
			else {
				if(!empty($attr['is_parent'])) {
					@$get = mysql_fetch_array(mysql_query("SELECT `{$attr['get']}` FROM branschguiden_categories WHERE current_url = '". $rurl[2] ."' AND parent = 0"));
					return isset($attr['lower']) ? strtolower($get[$attr['get']]) : $get[$attr['get']];
				}
				else if(!empty($rurl[2])) {
					@$get = mysql_fetch_array(mysql_query("SELECT `{$attr['get']}` FROM branschguiden_categories WHERE current_url = '". $rurl[2] ."'"));
					return isset($attr['lower']) ? strtolower($get[$attr['get']]) : $get[$attr['get']];
				}
			}
		}
	}
	
	public function url_friendly($string) {
		$url = str_replace("Ö","o",strtolower($string));
		$url = str_replace("ö","o",$url);
		$url = str_replace("Ä","a",$url);
		$url = str_replace("ä","a",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("å","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function parser_column_update($tag, $attr) {
		if(isset($_GET['column'])) {
		
			if($_GET['column'] == 1) {
				mysql_query("UPDATE branschguiden_categories SET on_startpage = 'true' WHERE id = '{$_GET['add']}'");
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else if($_GET['column'] == 2) {
				mysql_query("UPDATE branschguiden_categories SET on_startpage = 'false' WHERE id = '{$_GET['remove']}'");
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else if($_GET['column'] == 3) {
				@$company_id = $GLOBALS['parser']->parse("{page:rurl index='6'/}");
				if(!empty($company_id)) {
					@$company = mysql_fetch_array(mysql_query("SELECT categories FROM arbetsgivare_companies WHERE id = '{$company_id}'"));
					@$company_categories = $company['categories'];
					if(strstr($company_categories,"[{$_GET['toggle']}]")) {
						$company_categories = str_replace("[{$_GET['toggle']}]","",$company_categories);
					}
					else {
						$company_categories .= "[{$_GET['toggle']}]";
					}
					mysql_query("UPDATE arbetsgivare_companies SET categories = '{$company_categories}' WHERE id = '{$company_id}'");
				}
				else {
					@$get = mysql_fetch_array(mysql_query("SELECT on_startpage FROM branschguiden_categories WHERE id = '{$_GET['toggle']}'"));
					if($get['on_startpage'] == "true") {
						mysql_query("UPDATE branschguiden_categories SET on_startpage = 'false' WHERE id = '{$_GET['toggle']}'");
					}
					else {
						mysql_query("UPDATE branschguiden_categories SET on_startpage = 'true' WHERE id = '{$_GET['toggle']}'");
					}
				}
			}
		
		}
	}
	
	public function parser_sublist($tag, $attr) {
		$html = "";
		$onStartpage = (empty($attr['all'])) ?  "AND on_startpage = 'true'" : "";
		@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$attr['id']}'{$onStartpage} ORDER BY name ASC");
		while($category = mysql_fetch_array($categories)) {
			$this->subitem = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		return $html;
	}

	public function parser_list($tag, $attr) {
		$html = "";
		
		if(empty($this->startpageCategories)) {
			// Lets load the categories into the array
			@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '0' AND on_startpage = 'true' ORDER BY name ASC");
			$numCats = mysql_num_rows($categories);
			
			$items = array();
			$columns = 3;
			$column = 1;
			$i = 1;
			while($category = mysql_fetch_array($categories)) {
				if(($numCats/$columns+1) < $i) {
					// If we have reached 33% of total categories, we start on next column and reset $i
					$i = 1;
					$column++;
				}
				$items[$column][] = $category;
				$i++;
			}
			$this->startpageCategories = $items;
		}

		if(!empty($attr['column'])) {
			foreach($this->startpageCategories[$attr['column']] as $this->item) {
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else {
			foreach($this->startpageCategories as $column) {
				foreach($column as $this->item) {
					$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
		}
		
		return $html;
	}
	
	public function parser_parent_category($tag, $attr) {
		if(isset($_GET['parent']) && !empty($_GET['parent'])) {
			@$parent = mysql_fetch_array(mysql_query("SELECT name FROM branschguiden_categories WHERE id = '{$_GET['parent']}'"));
			$this->item = $parent;
		}
		else {
			$this->item['name'] = "Ingen kategori vald";
		}
		return $GLOBALS['parser']->parse($tag['innerhtml']);
	}
	
	public function parser_parent($tag, $attr) {
		return $_GET['parent'];
	}

	public function parser_sub_category_selected($tag, $attr) {
		if($this->subCategorySelected) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_column($tag, $attr) {
		$html = "";
		if($attr['index'] == 4) {
			// Load all main categories
			@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '0' ORDER BY name ASC");
			while($category = mysql_fetch_array($categories)) {
				$this->item = $category;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		if($attr['index'] == 1) {
			// Load the main categories who are not on the firstpage of branschguiden
			@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '0' AND on_startpage = 'false' ORDER BY name ASC");
			while($category = mysql_fetch_array($categories)) {
				$this->item = $category;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		if($attr['index'] == 2) {
			// Load the main categories
			@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '0' AND on_startpage = 'true' ORDER BY name ASC");
			while($category = mysql_fetch_array($categories)) {
				$this->item = $category;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		if($attr['index'] == 3) {
			
			@$company_id = $GLOBALS['parser']->parse("{page:rurl index='6'/}");
			
			if(!empty($company_id)) {
				if(isset($_GET['parent']) && !empty($_GET['parent'])) {
					@$company = mysql_fetch_array(mysql_query("SELECT categories FROM arbetsgivare_companies WHERE id = '{$company_id}'"));
					@$company_categories = $company['categories'];
					@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$_GET['parent']}' ORDER BY name ASC");
					
					if(mysql_num_rows($categories) > 0) {
						while($category = mysql_fetch_array($categories)) {
							$this->subCategorySelected = (strstr($company_categories,"[{$category['id']}]")) ? true : false;
							$this->item = $category;
							$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
						}
					}
					else {
						$html .= $GLOBALS['parser']->parse($attr['empty']);
					}
				}
				else {
					$html .= $GLOBALS['parser']->parse($attr['start']);
				}
			}
			else {
				if(isset($_GET['parent']) && !empty($_GET['parent'])) {
					@$categories = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$_GET['parent']}' ORDER BY name ASC");
					if(mysql_num_rows($categories) > 0) {
						while($category = mysql_fetch_array($categories)) {
							$this->subCategorySelected = ($category['on_startpage'] == "true") ? true : false;
							$this->item = $category;
							$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
						}
					}
					else {
						$html .= $GLOBALS['parser']->parse($attr['empty']);
					}
				}
				else {
					$html .= $GLOBALS['parser']->parse($attr['start']);
				}
			}
		}
		
		return $html;
	}
	
	public function get_children($parent) {
		// Load the children categories

        @$children = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$parent}' ORDER BY name ASC");

		if(mysql_num_rows($children) > 0) {
			// We have children
			$i = 1;
			while($child = mysql_fetch_array($children)) {
				$childs[$i] = $child;
				$childs[$i]['children'] = $this->get_children($child['id']);
			$i++;
			}
			return $childs; 
		}
		else {
			return 0;
		}
	}
	
	public function load_categories() {
		@$parents = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = 0 ORDER BY name ASC");
        if(!empty($parents)) {
			$i = 1;
			while($parent = mysql_fetch_array($parents)) {
				$this->categories[$i] = $parent;
				$this->categories[$i]['children'] = $this->get_children($parent['id']);
			$i++;
			}
		}
	}
	
	public function parser_get_this_tag($tag, $attr) {
		return $this->this_tag;
	}
	
	public function parser_get_tags($tag, $attr) {
		$html = "";
		$tags = explode("|",$this->item['tags']);
		
		foreach($tags as $tagz) {
			$this->this_tag = $tagz;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_categories($tag, $attr) {
		$html = "";
		// First we load the categories into the $this->categories array
		$this->load_categories();
		foreach($this->categories as $category) {
			$this->item = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_children($tag, $attr) {
		$this->html['children_innerhtml'] = (empty($this->html['children_innerhtml'])) ? $tag['innerhtml'] : $this->html['children_innerhtml'] ;
		$html = "";
		if(!empty($this->item['children'])) {
			//$html .= $this->parser_html("",array("get"=>"has_children_prefix"));
			$item = $this->item;
			$this->parentId = $item['id'];
            $html .=  $GLOBALS['parser']->parse(str_replace("[parent_id]",$item['id'],$this->parser_html("",array("get"=>"has_children_prefix"))));
			foreach($item['children'] as $child) {
				$this->item = $child;
				$html .= $this->parser_html("",array("get"=>"child_prefix")) . $GLOBALS['parser']->parse($this->html['children_innerhtml']) . $this->parser_html("",array("get"=>"child_suffix"));
				if(!empty($child['children'])) {
					$html .= $this->parser_children("","");
				}
				$this->parentId = $item['id'];
			}

           	$html .= $this->parser_html("",array("get"=>"has_children_suffix"));
            //$html .= $GLOBALS['parser']->parse(str_replace("[parent_id]",$item['id'],$this->parser_html("",array("get"=>"has_children_suffix"))));
		}
	return $html;
	}
	
	public function parser_has_children($tag, $attr) {
		if(!empty($this->item['children']) && $attr['is'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($this->item['children']) && $attr['is'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_parent_id($tag, $attr) {
		return $this->parentId;
	}
	
	public function parser_html($tag, $attr) {
		if(!empty($attr['set'])) {
			$this->html[$attr['set']] = $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!empty($attr['get'])) {
			return $this->html[$attr['get']];
		}
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			$get = $this->subitem[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			else {
				$get = $this->item[$attr['get']];
			}
			
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			
			// Make links open popup
			if(isset($attr['popup'])) {
				$get = str_replace('<a','<a target="_blank"',$get);
			}
			
			// New line to <br />
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
			
			// Cut the line and/or add something at the end
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				return $GLOBALS['text']->cut($get,$attr['cut']).$attr['add'];
			}
			else {
				return $get;
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_category($tag, $attr) {
		if(!empty($this->category[$attr['get']])) {
			return $this->category[$attr['get']];
		}
		else {
			return "";
		}
	}

}

?>