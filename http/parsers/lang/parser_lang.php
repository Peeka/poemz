<?php

class parser_lang {

	private $locale = 'en_US';
	private $defaultLocale = 'en_US';
	private $defaultTranslations = array();
	private $translations = array();

	function __construct() {
		// Check which translation we are using for the website
		$settings = mysql_fetch_array(mysql_query("SELECT locale FROM settings WHERE id = 1"));
		$this->locale = $settings['locale'];
		// Let's parse the default translations
		$getTranslations = mysql_query("SELECT * FROM language_translations WHERE locale = '{$this->defaultLocale}'");
		while($trans = mysql_fetch_array($getTranslations)) {
			$this->defaultTranslations[$this->defaultLocale][$trans['syntax']] = $trans['translation'];
		}
		// Let's parse the translations
		$getTranslations = mysql_query("SELECT * FROM language_translations WHERE locale = '{$this->locale}'");
		while($trans = mysql_fetch_array($getTranslations)) {
			$this->translations[$this->locale][$trans['syntax']] = $trans['translation'];
		}
	}
	
	public function parser_get($tag, $attr) {
		// Let's grab the translation
		if(!empty($this->translations[$this->locale][$attr['string']])) {
			return $this->translations[$this->locale][$attr['string']];
		}
		else {
			$this->defaultTranslations[$this->defaultLocale][$attr['string']];
		}
	}
	
}

?>