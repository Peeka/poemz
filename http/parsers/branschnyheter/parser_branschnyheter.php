<?php

class parser_branschnyheter {

	public $item = array();
	public $previous = array();
	public $next = array();

	function __construct() {
		$rurl = $GLOBALS['page']->get('rurl');
		// Accept any branschnyhet if requested
		if(isset($rurl[2]) && $rurl[2] == "accept") {
			if(isset($rurl[3]) && $rurl[3] > 0) {
				mysql_query("UPDATE arbetsgivare_branschnyheter SET status = 'accepted' WHERE id = '{$rurl[3]}'");
			}
		}
		// Decline any branschnyhet if requested
		if(isset($rurl[2]) && $rurl[2] == "decline") {
			if(isset($rurl[3]) && $rurl[3] > 0) {
				if(!empty($_POST['branschnyheter_status_comment'])) {
					mysql_query("UPDATE arbetsgivare_branschnyheter SET status = 'declined', status_comment = '{$_POST['branschnyheter_status_comment']}' WHERE id = '{$rurl[3]}'");
				}
				else {
					mysql_query("UPDATE arbetsgivare_branschnyheter SET status = 'declined' WHERE id = '{$rurl[3]}'");
				}
			}
		}
	}
	
	public function parser_get_company_name($tag, $attr) {
		$company = mysql_fetch_array(mysql_query("SELECT {$att['get']} FROM arbetsgivare_companies WHERE id = '{$attr['id']}'"));
		return $company[$att['get']];
	}
	
	public function parser_status_comment($tag, $attr) {
		if(isset($this->item['status_comment']) && !empty($this->item['status_comment']) && $this->item['status'] == "declined") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_status($tag, $attr) {
		if($attr['status'] == $attr['equals']) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_list_total_items($tag, $attr) {
		$total = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total_items FROM arbetsgivare_branschnyheter"));
		return $total['total_items'];
	}
	 
	public function parser_list($tag, $attr) {
		$html = "";
		$limit = " LIMIT ".$GLOBALS['pager']->limit();
        $status = (isset($attr['status'])) ? "WHERE status='".$attr['status']."'" : "";
		@$branschnyheter = mysql_query("SELECT * FROM arbetsgivare_branschnyheter {$status} ORDER BY added DESC {$limit}");

		while($branschnyhet = mysql_fetch_array($branschnyheter)) {
			$this->item = $branschnyhet;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			else {
				$get = $this->item[$attr['get']];
			}
			
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			
			// Make links open popup
			if(isset($attr['popup'])) {
				$get = str_replace('<a','<a target="_blank"',$get);
			}
			
			// New line to <br />
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
			
			// Cut the line and/or add something at the end
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				return $GLOBALS['text']->cut($get,$attr['cut']).$attr['add'];
			}
			else {
				return $get;
			}
		}
		else {
			return "";
		}
	}

}

?>