<?php

class parser_annonser {

	private $info = array();
	private $item = array();
	private $subitem = array();
	private $image = "";

	function __construct() {
		// Check if we want to add a category
		if(isset($_POST['add']) && !empty($_POST['name'])) {
			mysql_query("INSERT INTO arbetsgivare_job_categories SET name = '{$_POST['name']}'");
		}
		else if(isset($_POST['edit']) && !empty($_POST['name'])) {
			mysql_query("UPDATE arbetsgivare_job_categories SET name = '{$_POST['name']}' WHERE id = '{$_POST['id']}'");
		}
		else if(isset($_GET['delete'])) {
			// Delete the category
			mysql_query("DELETE FROM arbetsgivare_job_categories WHERE id = '{$_GET['delete']}'");
		}
		
		// Delete any jobb annons if requested
		$rurl = $GLOBALS['page']->get('rurl');
		if(isset($rurl[4]) && $rurl[4] == "radera") {
			@$owner = mysql_fetch_array(mysql_query("SELECT account_id FROM arbetsgivare_jobb_annonser WHERE id = '{$rurl[3]}'"));
			if($owner['account_id'] == account::user('id')) {
				mysql_query("DELETE FROM arbetsgivare_jobb_annonser WHERE id = '{$rurl[3]}'");
			}
		}
		// Accept any jobb annonser if requested
		if(isset($rurl[3]) && $rurl[3] == "accept") {
			if(isset($rurl[4]) && $rurl[4] > 0) {
				mysql_query("UPDATE arbetsgivare_jobb_annonser SET status = 'accepted' WHERE id = '{$rurl[4]}'");
			}
		}
		// Decline any jobb annonser if requested
		if(isset($rurl[3]) && $rurl[3] == "decline") {
			if(isset($rurl[4]) && $rurl[4] > 0) {
				@$status_comment = $_POST['jobbannons_status_comment'];
				mysql_query("UPDATE arbetsgivare_jobb_annonser SET status = 'declined', status_comment = '{$status_comment}' WHERE id = '{$rurl[4]}'");
			}
		}
		
		// first we need to get the profile
		if(account::user('id') > 0) {
			$this->parser_get_info("",array("get"=>"companies"));
			$this->parser_get_info("",array("get"=>"jobbannonser"));
			$this->parser_get_info("",array("get"=>"account"));
			$this->parser_get_info("",array("get"=>"category_id"));
			$this->parser_get_info("",array("get"=>"regions"));
		}
		
		// Resend any emails for job requests if requested
		if(isset($rurl[3]) && $rurl[3] == "resend") {
			if(isset($rurl[4]) && $rurl[4] > 0) {
				mysql_query("UPDATE arbetssokande_intresseanmalningar SET status = 'accepted' WHERE id = '{$rurl[4]}'");
				
				// Send a mail to the arbetsgivare
				@$request = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_intresseanmalningar WHERE id = '{$rurl[4]}'"));
				
				// First select all contact persons, and their emails
				@$contacts = unserialize($this->info['jobbannonser'][$request['job_id']]['contact_persons']);
				foreach($contacts as $contact) {
					// Send the actual mail
					// MAILTEMPLATE
					$GLOBALS['mail']->send($contact['email'],$request['title'],$request['message']);
				}
			}
		}
		// Accept any job requests if requested
		if(isset($rurl[3]) && $rurl[3] == "accept") {
			if(isset($rurl[4]) && $rurl[4] > 0) {
				mysql_query("UPDATE arbetssokande_intresseanmalningar SET status = 'accepted' WHERE id = '{$rurl[4]}'");
				
				// Send a mail to the arbetsgivare
				@$request = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_intresseanmalningar WHERE id = '{$rurl[4]}'"));
				
				// First select all contact persons, and their emails
				@$contacts = unserialize($this->info['jobbannonser'][$request['job_id']]['contact_persons']);
				if ($contacts) {
					foreach($contacts as $contact) {
						// Send the actual mail
						// MAILTEMPLATE
						$GLOBALS['mail']->send($contact['email'],$request['title'],$request['message']);
					}
				}
			}
		}
		// Decline any job requests if requested
		if(isset($rurl[3]) && $rurl[3] == "decline") {
			if(isset($rurl[4]) && $rurl[4] > 0) {
				mysql_query("UPDATE arbetssokande_intresseanmalningar SET status = 'declined' WHERE id = '{$rurl[4]}'");
			}
		}
		
		// Check if a job request has been sent
		if($GLOBALS['page']->contain("intresseanmalan")) {
			// Get the job application id
			@$job_id = $GLOBALS['page']->index(2);
			
			if($job_id > 0) {
				mysql_query("INSERT INTO arbetssokande_intresseanmalningar SET 
							job_id = '{$job_id}', 
							account_id = '". account::user('id') ."', 
							added = '". time() ."', 
							title = '{$_POST['application_title']}', 
							message = '{$_POST['application_message']}'");
			}
			else {
				// An error occured
				// Error coding here
			}
		}
	}
	
	public function parser_intresseanmalningar($tag, $attr) {
		$html = "";
		@$annonser = mysql_query("SELECT * FROM arbetssokande_intresseanmalningar WHERE status = '{$attr['status']}' ORDER BY status,added DESC");
		while($annons = mysql_fetch_array($annonser)) {
			$this->item = $annons;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_intresseanmalan($tag, $attr) {
		@$job_id = $GLOBALS['page']->index(2);
		@$check = mysql_fetch_array(mysql_query("SELECT status FROM arbetssokande_intresseanmalningar WHERE job_id = '{$job_id}' AND account_id = '". account::user('id') ."'"));
		
	//	exit("SELECT status FROM arbetssokande_intresseanmalningar WHERE job_id = '{$job_id}' AND account_id = '". account::user('id') ."'");
		
		if(empty($check['status']) && $attr['status'] == "empty") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($check['status'] == "waiting" && $attr['status'] == "waiting") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($check['status'] == "accepted" && $attr['status'] == "accepted") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($check['status'] == "declined" && $attr['status'] == "declined") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_publicera($tag, $attr) {
		mysql_query("UPDATE arbetsgivare_jobb_annonser SET status = 'waiting' WHERE id = '{$attr['id']}'");
		header("Location: {$attr['success_redirect']}");
		exit;
	}
	
	public function parser_has_image($tag, $attr) {
		@$id = $GLOBALS['parser']->parse("{page:rurl index='4'/}");
		if(!empty($this->info['jobbannonser'][$id]['image_big'])) {
			$this->image = $this->info['jobbannonser'][$id]['image_big'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_waiting_requests($tag, $attr) {
		@$check = mysql_query("SELECT id FROM arbetsgivare_jobb_annonser WHERE account_id = '". account::user('id') ."' AND status = 'declined'");
					
		if(mysql_num_rows($check) > 0) {
			$this->total = mysql_num_rows($check);
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_total($tag, $attr) {
		return $this->total = ($this->total > 0) ? $this->total : 0;
	}
	
	public function parser_extern_website($tag, $attr) {
		if(!empty($this->info['jobbannonser'][$attr['index']]['extern_website']) && $attr['state'] == "not_empty") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($this->info['jobbannonser'][$attr['index']]['extern_website']) && $attr['state'] == "empty") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_status_comment($tag, $attr) {
		if(isset($this->info['jobbannonser'][$attr['index']]['status_comment']) && !empty($this->info['jobbannonser'][$attr['index']]['status_comment']) && $this->info['jobbannonser'][$attr['index']]['status'] == "declined") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_status($tag, $attr) {
		if($attr['status'] == $attr['equals']) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_unserialize($tag, $attr) {
		$html = "";
		$contact_persons = unserialize($attr['string']);
		if ($contact_persons) {
			foreach($contact_persons as $contact_person) {
				$this->subitem = $contact_person;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	return $html;
	}
	
	public function parser_requests($tag, $attr) {
		$html = "";
		@$annonser = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE status = '{$attr['status']}' ORDER BY status,added DESC");
		while($annons = mysql_fetch_array($annonser)) {
			$this->item = $annons;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_category_info($tag, $attr) {
		@$info = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_job_categories WHERE id = '{$attr['id']}'"));
		return $info[$attr['get']];
	}
	
	public function parser_company_info($tag, $attr) {
		@$info = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$attr['id']}'"));
		return $info[$attr['get']];
	}
	
	public function parser_get_info($tag, $attr) {
		if($attr['get'] == "companies") {
			@$info = mysql_fetch_array(mysql_query("SELECT companies FROM arbetsgivare_accounts WHERE account_id = '". account::user('id') ."'"));
			if(isset($info['companies']) && !empty($info['companies'])) {
				$companies = explode(",",$info['companies']);
				foreach($companies as $id) {
					$company = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$id}'"));
					$this->info['companies'][$id] = $company;
				}
			}
		}
		else if($attr['get'] == "jobbannonser") {
			@$info = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE account_id = '". account::user('id') ."' and active='1'");
			while($annons = mysql_fetch_array($info)) {
				$this->info['jobbannonser'][$annons['id']] = $annons;
			}
		}
		else if($attr['get'] == "account") {
			@$info = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_accounts WHERE account_id = '". account::user('id') ."'"));
			$this->info['account'] = $info;
		}
		else if($attr['get'] == "category_id") {
			@$info = mysql_query("SELECT * FROM arbetsgivare_job_categories");
			while($cat = mysql_fetch_array($info)) {
				$this->info['category_id'][$cat['id']] = $cat;
			}
		}
		else if($attr['get'] == "regions") {
			@$info = mysql_query("SELECT * FROM regions ORDER BY type DESC, name ASC");
			while($region = mysql_fetch_array($info)) {
				$this->info['regions'][$region['id']] = $region;
			}
		}
	}
	
	public function parser_soon_last_day($tag, $attr) {
		$last_day = $this->info['jobbannonser'][$attr['index']]['last_day'];
		$days = 60 * 60 * 24 * $attr['days'];
		
		if($attr['state'] == "not_out" && time() >= ($last_day - $days) && time() < $last_day) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($attr['state'] == "out" && time() > $last_day) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_last_day($tag, $attr) {
		if(isset($attr['string']) && !empty($attr['string'])) {
			return date("m/d/Y",$attr['string']);
		}
		else {
			if (isset($attr['index'])){
				if(isset($this->info['jobbannonser'][$attr['index']]['last_day']) && !empty($this->info['jobbannonser'][$attr['index']]['last_day'])) {
					if(isset($attr['date'])) {
						return date($attr['date'],$this->info['jobbannonser'][$attr['index']]['last_day']);
					}
					else {
						return date("m/d/Y",$this->info['jobbannonser'][$attr['index']]['last_day']);
					}
				}
				else if(isset($_POST['last_day']) && !empty($_POST['last_day'])) {
					return $_POST['last_day'];
				}
			}
		}
	}
	
	public function parser_load_draft($tag, $attr) {
		@$get = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_jobb_annonser_drafts WHERE id = '{$attr['id']}' and active='1'"));
		$draft = unserialize($get['draft']);
		$this->item = $draft;
		$this->item['draft_id'] = $get['id'];
		$this->item['draft_parent_id'] = $get['parent_id'];
		exit(print_r($this->item));
	}
	
	public function parser_regions($tag, $attr) {
		$html = "";
		foreach($this->info['regions'] as $region) {
			$this->item = $region;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_all_job_categories($tag, $attr) {
		$html = "";
		@$get = mysql_query("SELECT * FROM arbetsgivare_job_categories ORDER BY name ASC");
		while($category = mysql_fetch_array($get)) {
			$this->item = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_job_categories($tag, $attr) {
		$html = "";
		foreach($this->info['category_id'] as $category) {
			$this->item = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_companies($tag, $attr) {
		$html = "";
		foreach($this->info['companies'] as $company) {
			$this->item = $company;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_allowed_companies($tag, $attr) {
		$companies = explode(",",$this->info['account']['companies']);
		$allowed = false;
		
		foreach($companies as $company) {
			if(!isset($attr['id'])) {
				if(!empty($company) && $company > 0) {
					$allowed = true;
				}
			}
			else {
				if($company == $attr['id']) {
					$allowed = true;
				}
			}
		}
		if($allowed && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$allowed && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_owns_annons($tag, $attr) {
		foreach($this->info['jobbannonser'] as $annons) {
			if(!isset($attr['id'])) {
				if($annons['account_id'] == account::user('id') && $attr['state'] == "true") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else if($annons['account_id'] != account::user('id') && $attr['state'] == "false") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
			else {
			if($annons['id'] == $attr['id']) {
				if($annons['account_id'] == account::user('id') && $attr['state'] == "true") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else if($annons['account_id'] != account::user('id') && $attr['state'] == "false") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
			}
		}
	}
	
	public function parser_contact_persons($tag, $attr) {
		$html = "";
		$id = (isset($attr['index'])) ? $attr['index'] : $GLOBALS['parser']->parse("{page:rurl index='4'/}");
		if(!empty($this->contacts)) {
			foreach($this->contacts as $contact) {
				$this->item = $contact;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if(!empty($id)) {
			@$get = mysql_fetch_array(mysql_query("SELECT contact_persons FROM  arbetsgivare_jobb_annonser WHERE id = '{$id}' and active='1'"));
			@$contacts = unserialize($get['contact_persons']);
			foreach($contacts as $contact) {
				$this->item = $contact;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	return $html;
	}
	
	public function parser_update_info($tag, $attr) {
	
		if(isset($_POST['jobbannonser_new_contact'])) {
			$i = 0;
			foreach($_POST['jobbannonser_new_contact_name'] as $contact_name) {
				if(!empty($contact_name) || !empty($_POST['jobbannonser_new_contact_telephone'][$i]) || !empty($_POST['jobbannonser_new_contact_email'][$i])) {
					$this->contacts[$i]['name'] = $contact_name;
					$this->contacts[$i]['telephone'] = $_POST['jobbannonser_new_contact_telephone'][$i];
					$this->contacts[$i]['email'] = $_POST['jobbannonser_new_contact_email'][$i];
				}
			$i++;
			}
		}
		else {

			if(isset($_POST['jobbannonser_submit']) || isset($_POST['jobbannonser_edit']) || isset($_POST['jobbannonser_preview'])) {
			
				$id = $GLOBALS['parser']->parse("{page:rurl index='4'/}");

				// get and parse all contact persons
				$i = 0;
				foreach($_POST['jobbannonser_new_contact_name'] as $contact_name) {
					if(!empty($contact_name) || !empty($_POST['jobbannonser_new_contact_telephone'][$i]) || !empty($_POST['jobbannonser_new_contact_email'][$i])) {
						$this->contacts[$i]['name'] = $contact_name;
						$this->contacts[$i]['telephone'] = $_POST['jobbannonser_new_contact_telephone'][$i];
						$this->contacts[$i]['email'] = $_POST['jobbannonser_new_contact_email'][$i];
					}
				$i++;
				}
				
				@$contacts = serialize($this->contacts);
				@$last_day = strtotime($_POST['jobbannonser_last_day']);
				@$extern_website = (isset($_POST['jobbannonser_extern_ansokning'])) ? $_POST['jobbannonser_extern_ansokning_website'] : '';
				
				// save the information to the database
				if(isset($_POST['jobbannonser_edit'])) {
					mysql_query("UPDATE arbetsgivare_jobb_annonser SET
									company_id = '{$_POST['jobbannonser_company_id']}', 
									category_id = '{$_POST['jobbannonser_category_id']}', 
									title = '{$_POST['jobbannonser_title']}', 
									description = '{$_POST['jobbannonser_description']}', 
									contact_persons = '{$contacts}', 
									region = '{$_POST['jobbannonser_region']}', 
									last_day = '{$last_day}', 
									extern_website = '{$extern_website}', 
									status = 'waiting' 
									WHERE id = '". $id ."' AND account_id = '". account::user('id') ."'");
				}
				else if(isset($_POST['jobbannonser_submit'])) {
					mysql_query("INSERT INTO arbetsgivare_jobb_annonser SET
									company_id = '{$_POST['jobbannonser_company_id']}', 
									account_id = '". account::user('id') ."', 
									category_id = '{$_POST['jobbannonser_category_id']}', 
									title = '{$_POST['jobbannonser_title']}', 
									description = '{$_POST['jobbannonser_description']}', 
									contact_persons = '{$contacts}', 
									last_day = '{$last_day}', 
									region = '{$_POST['jobbannonser_region']}', 
									added = '". time() ."', 
									extern_website = '{$extern_website}', 
									status = 'waiting'") or die(mysql_error());
						$id = mysql_insert_id();
				}
				else if(isset($_POST['jobbannonser_preview'])) {
					if($id <= 0) {
						mysql_query("INSERT INTO arbetsgivare_jobb_annonser SET
										company_id = '{$_POST['jobbannonser_company_id']}', 
										account_id = '". account::user('id') ."', 
										category_id = '{$_POST['jobbannonser_category_id']}', 
										title = '{$_POST['jobbannonser_title']}', 
										description = '{$_POST['jobbannonser_description']}', 
										contact_persons = '{$contacts}', 
										last_day = '{$last_day}', 
										region = '{$_POST['jobbannonser_region']}', 
										added = '". time() ."', 
										extern_website = '{$extern_website}', 
										status = 'draft'") or die(mysql_error());
						$id = mysql_insert_id();
					}
					else {
						mysql_query("UPDATE arbetsgivare_jobb_annonser SET
										company_id = '{$_POST['jobbannonser_company_id']}', 
										category_id = '{$_POST['jobbannonser_category_id']}', 
										title = '{$_POST['jobbannonser_title']}', 
										description = '{$_POST['jobbannonser_description']}', 
										contact_persons = '{$contacts}', 
										region = '{$_POST['jobbannonser_region']}', 
										last_day = '{$last_day}', 
										extern_website = '{$extern_website}', 
										status = 'draft' 
										WHERE id = '". $id ."' AND account_id = '". account::user('id') ."'");
					}
				}
				
				if(isset($_POST['jobbannonser_bild_radera'])) {
					@$image = mysql_fetch_array(mysql_query("SELECT image_big, image_thumb FROM arbetsgivare_jobb_annonser WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='4'/}") ."' and active='1'"));
					unlink(INTERNAL_PATH ."/public/images/jobbannonser/". $image['image_big']);
					unlink(INTERNAL_PATH ."/public/images/jobbannonser/". $image['image_thumb']);
					mysql_query("UPDATE arbetsgivare_jobb_annonser SET image_big = '', image_thumb = '' WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='4'/}") ."'");
				}
				
				// Check if we have an image to upload
				if(isset($_FILES["jobbannonser_bild"]["name"])) {
					
					@$allowedExts = array("jpg", "jpeg", "gif", "png");
					@$extension = end(explode(".", $_FILES["jobbannonser_bild"]["name"]));
					
					if(in_array($extension, $allowedExts)) {
						$newName = "jobbannons_".$id;
						
						if(file_exists(INTERNAL_PATH ."/public/images/jobbannonser/". $newName . $extension)) {
							unlink(INTERNAL_PATH ."/public/images/jobbannonser/". $newName . $extension);
						}
						
						$newFileBig = $newName ."_big.". $extension;
						$newFileThumb = $newName ."_thumb.". $extension;
						
						// Create the big image
						if(move_uploaded_file($_FILES["jobbannonser_bild"]["tmp_name"], INTERNAL_PATH ."/public/images/jobbannonser/". $newFileBig)) {
							image::resize(INTERNAL_PATH ."/public/images/jobbannonser/". $newFileBig,
												INTERNAL_PATH ."/public/images/jobbannonser/". $newFileBig,
												470,
												300);
												
							image::resize(INTERNAL_PATH ."/public/images/jobbannonser/". $newFileBig,
												INTERNAL_PATH ."/public/images/jobbannonser/". $newFileThumb,
												150,
												100);
						}
						mysql_query("UPDATE arbetsgivare_jobb_annonser SET image_big = '{$newFileBig}', image_thumb = '{$newFileThumb}' WHERE id = '{$id}'");
					}
				}
				
				if(isset($_POST['jobbannonser_preview'])) {
					header("Location: /konto/mina-annonser/jobb-annons/forhandsgranskning/{$id}");
					exit;
				}
				
				// now lets redirect if there is a redirect
				if(!empty($attr['success_redirect'])) {
					header("Location: {$attr['success_redirect']}");
					exit;
				}
			}
		}
	}
	
	public function parser_jobb($tag, $attr) {
		$html = "";
		foreach($this->info['jobbannonser'] as $jobb) {
			$this->item = $jobb;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_image($tag, $attr) {
		return @$this->image;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_subitem($tag, $attr) {
		return @$this->subitem[$attr['get']];
	}
	
	public function parser_info($tag, $attr) {
		// Posted values
		if(isset($attr['posted_first']) && !empty($attr['posted_first']) && isset($_POST["{$attr['get']}_{$attr['name']}"]) && !empty($_POST["{$attr['get']}_{$attr['name']}"])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $_POST["{$attr['get']}_{$attr['name']}"]) {
					return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
				}
				else {
					return "";
				}
			}
			else {
				return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
			}
		} // Stored values
		else if(isset($this->info[$attr['get']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['name']]) || 
		isset($attr['index']) && isset($this->info[$attr['get']][$attr['index']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['index']][$attr['name']])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $this->info[$attr['get']][$attr['name']] || isset($attr['index']) && $attr['value'] == $this->info[$attr['get']][$attr['index']][$attr['name']]) {
					if(isset($attr['index'])) {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
					}
					else {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
					}
				}
				else {
					return "";
				}
			}
			else {
				if(isset($attr['index'])) {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
				}
				else {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
				}
			}
		}
		else {
			return "";
		}
	}

}

?>