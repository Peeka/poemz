<?php

class parser_kalender {

	private $info = array();
	private $item = array();
	private $subitem = array();
	private $image = "";
	private $time = "";
	private $day = "";
	private $month = "";
	private $year = "";

	function __construct() {
		@$id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
		if($id > 0) {
			@$this->item = mysql_fetch_array(mysql_query("SELECT * FROM kalender WHERE id = '{$id}'"));
			@$this->date = date("m/d/Y",$this->item['date']);
		}
		
		// ADMIN CALENDAR
		
		@$radera = $GLOBALS['parser']->parse('{page:rurl index="2"/}');

        //checking if the image has been deleted
        if (isset($_POST['kalender_image_radera'])){
           	@$item = mysql_fetch_array(mysql_query("SELECT image FROM kalender WHERE id = '{$id}'"));
			if(file_exists(INTERNAL_PATH ."/public/images/kalender/". $item['image'])) {
				unlink(INTERNAL_PATH ."/public/images/kalender/". $item['image']);
			}

            mysql_query("UPDATE kalender SET image='' WHERE id = '{$id}'");   
        }

		if ( ($radera == "radera") )  {
			@$item = mysql_fetch_array(mysql_query("SELECT image FROM kalender WHERE id = '{$id}'"));
			if(file_exists(INTERNAL_PATH ."/public/images/kalender/". $item['image'])) {
				unlink(INTERNAL_PATH ."/public/images/kalender/". $item['image']);
			}
			mysql_query("DELETE FROM kalender WHERE id = '{$id}'");
		}

		if(isset($_POST['kalender_add'])) {
			@$date = strtotime($_POST['kalender_date']);
			mysql_query("INSERT INTO kalender SET 
							title = '{$_POST['kalender_title']}', 
							text = '{$_POST['kalender_text']}', 
							place = '{$_POST['kalender_place']}', 
							date = '{$date}', 
							time = '{$_POST['kalender_time']}'");
			@$id = mysql_insert_id();
			
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["kalender_image"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "kalender_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/kalender/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/kalender/". $newName . $extension);
				}

				$newFileBig = $newName .".". $extension;
				
				// Create the big image
				if(move_uploaded_file($_FILES["kalender_image"]["tmp_name"], INTERNAL_PATH ."/public/images/kalender/". $newFileBig)) {
					image::resize(INTERNAL_PATH ."/public/images/kalender/". $newFileBig,
										INTERNAL_PATH ."/public/images/kalender/". $newFileBig,
										215,
										140);
				}
				mysql_query("UPDATE kalender SET image = '{$newFileBig}' WHERE id = '{$id}'");
			}
			
			header("Location: /admin/kalender");
			exit;
		}
		else if(isset($_POST['kalender_edit'])) {
			@$date = strtotime($_POST['kalender_date']);
			@$id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
			
			mysql_query("UPDATE kalender SET 
							title = '{$_POST['kalender_title']}', 
							text = '{$_POST['kalender_text']}', 
							place = '{$_POST['kalender_place']}', 
							date = '{$date}', 
							time = '{$_POST['kalender_time']}' 
							WHERE id = '{$id}'");
			
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["kalender_image"]["name"]));


			if(in_array($extension, $allowedExts)) {
				$newName = "kalender_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/kalender/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/kalender/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the big image
				if(move_uploaded_file($_FILES["kalender_image"]["tmp_name"], INTERNAL_PATH ."/public/images/kalender/". $newFileBig)) {
					image::resize(INTERNAL_PATH ."/public/images/kalender/". $newFileBig,
										INTERNAL_PATH ."/public/images/kalender/". $newFileBig,
										215,
										140);
				}
				mysql_query("UPDATE kalender SET image = '{$newFileBig}' WHERE id = '{$id}'");
			}
			
			if (isset($_POST['kalender_image_radera']))
                header("Location: /admin/kalender/redigera/{$id}");
            else header("Location: /admin/kalender");
			exit;
		}
	}
	
	public function parser_list($tag, $attr) {
		$html = "";
		@$kalendern = mysql_query("SELECT * FROM kalender ORDER BY date ASC");
		while($kalender = mysql_fetch_array($kalendern)) {
			$this->item = $kalender;
			$this->date = date("m/d/Y",$kalender['date']);
			$this->day = date("d",$kalender['date']);
			$this->month = date("m",$kalender['date']);
			$this->year = date("Y",$kalender['date']);
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_day($tag, $attr) {
		return $this->day;
	}
	
	public function parser_month($tag, $attr) {
		return str_replace("0","",$this->month);
	}
	
	public function parser_year($tag, $attr) {
		return $this->year;
	}
	
	public function parser_date($tag, $attr) {
		return $this->date;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_loadnewitem($tag, $attr) {
		$this->item = mysql_fetch_array(mysql_query("SELECT * FROM kalender WHERE id='" . $attr['id'] . "' ORDER BY date ASC"));		
	}

    public function parser_kalendar_has_image($tag, $attr) {
		if(!empty($this->item['image']) && $attr['state'] == "true" ) {
			$this->image = $this->item['v'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		} else if ( $attr['state'] == "false" && empty($this->item['image'])) {
           return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

}

?>