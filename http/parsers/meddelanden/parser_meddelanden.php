<?php

class parser_meddelanden {

	public $item = array();
	public $total = 0;

	function __construct() {
		if(isset($_POST['message_reply'])) {
			@$id = $GLOBALS['parser']->parse("{page:rurl index='2'/}");
			@$parent = mysql_fetch_array(mysql_query("SELECT * FROM meddelanden WHERE id = '{$id}'"));
			
			if($parent['sender'] == account::user('id')) {
				$sender = account::user('id');
				$reciever = $parent['reciever'];
			}
			else {
				$sender = $parent['reciever'];
				$reciever = account::user('id');
			}
			
			mysql_query("INSERT INTO meddelanden SET 
							reciever = '{$reciever}', 
							sender = '{$sender}', 
							sent = '". time() ."', 
							title = '{$_POST['title']}', 
							message = '{$_POST['message']}', 
							parent = '{$id}'
						");
						
		}
	}
	
	public function parser_messages($tag, $attr) {
		$html = "";
		@$msgs = mysql_query("SELECT * FROM meddelanden WHERE 
								id = '{$attr['id']}' AND sender = '". account::user('id') ."' 
								OR id = '{$attr['id']}' AND reciever = '". account::user('id') ."' 
								OR parent = '{$attr['id']}' AND sender = '". account::user('id') ."' 
								OR parent = '{$attr['id']}' AND reciever = '". account::user('id') ."' 
								");
		
		while($msg = mysql_fetch_array($msgs)) {
			if($msg['reciever'] == account::user('id') && $msg['viewed'] == "false") {
				mysql_query("UPDATE meddelanden SET viewed = 'true' WHERE id = '{$msg['id']}'");
			}
			$this->item = $msg;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_got_new_message($tag, $attr) {
		@$msgs = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total FROM meddelanden WHERE 
												reciever = '". account::user('id') ."' AND parent = '{$attr['parent']}' AND viewed = 'false' 
												OR reciever = '". account::user('id') ."' AND id = '{$attr['parent']}' AND viewed = 'false' 
												OR sender = '". account::user('id') ."' AND parent = '{$attr['parent']}' AND viewed = 'false' 
												OR sender = '". account::user('id') ."' AND id = '{$attr['parent']}' AND viewed = 'false' 
												"));
		
		if($msgs['total'] > 0 && $attr['state'] == "true") {
			$this->total = $msgs['total'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($msgs['total'] <= 0 && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_new_messages($tag, $attr) {
		@$msgs = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total FROM meddelanden WHERE 
												reciever = '". account::user('id') ."' AND parent = '{$attr['parent']}' AND viewed = 'false' 
												OR reciever = '". account::user('id') ."' AND id = '{$attr['parent']}' AND viewed = 'false'"));
		
		if($msgs['total'] > 0 && $attr['state'] == "true") {
			$this->total = $msgs['total'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($msgs['total'] <= 0 && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_got_messages($tag, $attr) {
		@$msgs = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total FROM meddelanden WHERE 
													reciever = '". account::user('id') ."' AND parent = 0 
													OR sender = '". account::user('id') ."' AND parent = 0
													"));
		
		if($msgs['total'] > 0 && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($msgs['total'] <= 0 && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_list($tag, $attr) {
		$html = "";
		@$msgs = mysql_query("SELECT * FROM meddelanden WHERE 
								reciever = '". account::user('id') ."' AND parent = 0 
								OR sender = '". account::user('id') ."' AND parent = 0
								");
		
		while($msg = mysql_fetch_array($msgs)) {
			$this->item = $msg;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_total($tag, $attr) {
		return $this->total;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if($attr['get'] == "sent") {
				return date("H:i, d/m/Y", $this->item[$attr['get']]);
			}
			else if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

}

?>