<?php

class parser_pager {

	private $item = array();
	private $divider = "";
	private $inactive = "";
	private $active = "";
	private $prefix = "";
	private $suffix = "";
	private $previous = "";
	private $next = "";
	private $number_of_items = 0;

	function __construct() {
		
	}
	
	public function parser_pages($tag, $attr) {
		$html = "";
		$skip_divider = false;

		// Prefix
		// ... 2 3 [4]
		if($GLOBALS['pager']->page() > 1) {
			$html .= $this->fix($GLOBALS['parser']->parse($this->prefix));
			$html .= $this->fix($GLOBALS['parser']->parse($this->previous));
			$skip_divider = true;
		}
		
		// Previous
		//$html .= ($GLOBALS['pager']->page() > 1) ? $this->fix($GLOBALS['parser']->parse($this->previous)) : "";
		
		for($i=1;$i<=$GLOBALS['pager']->get("number_of_pages");$i++) {
			// Check if the page is within the range of the page margin
			// [1] 2 3 ...
			
			for($z=($GLOBALS['pager']->page()-$GLOBALS['pager']->get("page_margin")); $z<=($GLOBALS['pager']->page()+$GLOBALS['pager']->get("page_margin")); $z++) {
				$range[] = $z;
			}
			
			if(in_array($i,$range)) {
				$this->item['id'] = $i;
				
				// check the divider
				if(isset($skip_divider) && $skip_divider) {
					$skip_divider = false;
				}
				else {
					$html .= ($i > 1) ? $GLOBALS['parser']->parse($this->divider) : "";
				}
				$html .= ($GLOBALS['pager']->page() == $i) ? $this->fix($GLOBALS['parser']->parse($this->active)) : $this->fix($GLOBALS['parser']->parse($this->inactive));
			}
		}
		
		// Next
		//$html .= ($GLOBALS['pager']->page() < $GLOBALS['pager']->get("number_of_pages")) ? $this->fix($GLOBALS['parser']->parse($this->next)) : "";
		
		// Suffix
		// [1] 2 3 ...
		if($GLOBALS['pager']->page() < $GLOBALS['pager']->get("number_of_pages")) {
			$html .= $this->fix($GLOBALS['parser']->parse($this->next));
			$html .= $this->fix($GLOBALS['parser']->parse($this->suffix));
		}
		
	return $html;
	}
	
	
	public function parser_number_of_items($tag, $attr) {
		$GLOBALS['pager']->set("number_of_items",$attr['value']);
		// Calculate number of pages
		if(($GLOBALS['pager']->get("number_of_items") % $GLOBALS['pager']->get("items_per_page")) > 0) {
		
			// number_of_items: 13
			// items_per_page: 3
			// 
			
			$number_of_pages = ((($GLOBALS['pager']->get("number_of_items") - ($GLOBALS['pager']->get("number_of_items") % $GLOBALS['pager']->get("items_per_page"))) / $GLOBALS['pager']->get("items_per_page")) + 1);
			$GLOBALS['pager']->set("number_of_pages",$number_of_pages);
		}
		else {
			$number_of_pages = ($GLOBALS['pager']->get("number_of_items") / $GLOBALS['pager']->get("items_per_page"));
			$GLOBALS['pager']->set("number_of_pages",$number_of_pages);
		}
	}
	
	public function fix($string) {
		@$string = str_replace("[RURL]",$GLOBALS['page']->rurl(),$string);
		@$string = str_replace("[ID]",$this->item['id'],$string);
		@$string = str_replace("[NEXT]",($GLOBALS['pager']->page()+1),$string);
		@$string = str_replace("[PREVIOUS]",($GLOBALS['pager']->page()-1),$string);
		@$string = str_replace("[FIRST]",1,$string);
		return str_replace("[LAST]",$GLOBALS['pager']->get("number_of_pages"),$string);
	}
	
	public function parser_rurl($tag, $attr) {
		return $_SERVER['SERVER_NAME'];
	}
	
	public function parser_previous($tag, $attr) {
		$this->previous = $tag['innerhtml'];
	}
	
	public function parser_next($tag, $attr) {
		$this->next = $tag['innerhtml'];
	}
	
	public function parser_prefix($tag, $attr) {
		$this->prefix = $tag['innerhtml'];
	}
	
	public function parser_suffix($tag, $attr) {
		$this->suffix = $tag['innerhtml'];
	}
	
	public function parser_inactive($tag, $attr) {
		$this->inactive = $tag['innerhtml'];
	}
	
	public function parser_active($tag, $attr) {
		$this->active = $tag['innerhtml'];
	}
	
	public function parser_divider($tag, $attr) {
		$this->divider = $tag['innerhtml'];
	}
	
	public function parser_item($tag, $attr) {
		return $this->item[$attr['get']];
	}
	
	public function parser_get($tag, $attr) {
		return $GLOBALS['pager']->get($attr['attr']);
	}
	
	public function parser_set($tag, $attr) {
		$GLOBALS['pager']->set($attr['attr'],$attr['value']);
	}

}

?>