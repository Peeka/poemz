<?php

class parser_html {

	function __construct() {
	
	}

	public function parser_add($tag, $attr) {
		// Lets include the js for auto expanding textareas
	#	$GLOBALS['parser']->parsed = preg_replace("/(<head[^>]*>)/", "$1\r\n<script type=\"text/javascript\" charset=\"UTF-8\" src=\"js/jquery/jquery.textchange.min.js\"></script>", $GLOBALS['parser']->parsed);
		
		@$admin = $_GET['adminview'];
		@$page_id = (isset($_GET['page']) && !empty($_GET['page'])) ? mysql_real_escape_string($_GET['page']) : $GLOBALS['page_id'];
	#	@$page_id = (!empty($GLOBALS['page'])) ? $GLOBALS['page'] : $GLOBALS['page_id'];
		@$value_name = mysql_real_escape_string($attr['name']);
		
		// Check if the field exists, otherwise we add it
		@$check = mysql_fetch_array(mysql_query("SELECT id,value FROM nova_values_html WHERE page_id = '{$page_id}' AND value_name = '{$value_name}'"));	
		if($check['id'] <= 0) {
			// It didnt exist, so lets create it
			mysql_query("INSERT INTO nova_values_html SET page_id = '{$page_id}', value_name = '{$value_name}'");
		}
		
		if($admin == 1) {
			// Now we have set it up, and need to load the editor
			$value = str_replace("<br />","\n",$check['value']);
			$html = '<textarea class="autoexpand" name="htmlfield[]" id="htmlfield[]" rel="'. $value_name .'" type="text">'. $value .'</textarea>';
		}
		else {
			// Load the value for public view
			@$check = mysql_fetch_array(mysql_query("SELECT value FROM nova_values_html WHERE page_id = '{$page_id}' AND value_name = '{$value_name}'"));
			$html = str_replace("\n","<br />",$check['value']);
		}
		
	return $html;
	}
	
	public function parser_get($tag, $attr) {
		@$get = mysql_fetch_array(mysql_query("SELECT value FROM nova_values_html WHERE id = '{$attr['id']}'"));
		return $get['value'];
	}
	
	public function parser_auto_expand_fired() {
		return '<div id="autoexpand_fired" style="display:none;position:fixed;top:0px;left:50%-40px;height:20px;z-index:900;background-color:#d7ebf9;border:1px solid #aed0ea;color:#2779aa;padding:5px;border-bottom-right-radius:5px;padding-left:10px;padding-right:10px;">Autosave</div>';
	}
	
	public function parser_auto_expand() {
		@$page_id = $GLOBALS['page'];
		
		if(isset($_GET['adminview'])) {
		$html = "
				<script type=\"text/javascript\" charset=\"UTF-8\" src=\"js/jquery/jquery-1.7.2.js\"></script>
				<script type=\"text/javascript\" charset=\"UTF-8\" src=\"parsers/html/js/jquery.autosize.js\"></script>
				<script type=\"text/javascript\" charset=\"UTF-8\" src=\"parsers/html/js/jquery.textchange.min.js\"></script>
				<link href=\"parsers/html/css/autoexpand.css\" rel=\"stylesheet\" type=\"text/css\" />
				<script>
					$('a').live('click', function() { return false;});
				
					$(function() {
						$('.autoexpand').autosize({append: '\\n'});
					});
					
					$(document).ready(function(){
						var timeout;
						$('.autoexpand').bind('textchange', function () {
							clearTimeout(timeout);
							$('#autoexpand_fired').html('<strong>Typing...</strong>');
							var self = this;
							timeout = setTimeout(function () {					
								$('#autoexpand_fired').css('display','block');
								
								var rel = $(self).attr('rel');
								var xmlhttp;
								if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
									xmlhttp=new XMLHttpRequest();
								}
								else {// code for IE6, IE5
									xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
								}
								xmlhttp.onreadystatechange=function() {
									if (xmlhttp.readyState==4 && xmlhttp.status==200) {
										var res = xmlhttp.responseText;
										$('#autoexpand_fired').html('<strong>Draft saved.</strong>');
									}
								}
								var val = $(self).val();
								var val_send = val.split('\\n').join('<br />');
								xmlhttp.open('GET','ajax.php?action=savedraft&page_id={$page_id}&value_name='+rel+'&value='+val_send,true);
								xmlhttp.send();
							}, 1000);
						});
					});
				</script>";
		}
	return $html;
	}

}

?>