<?php

class parser_template {

	private $areas	= array();

	function __construct() {
	
	}
	
	public function parser_file($tag, $attr) {
		return file_get_contents(ROOT_PATH . $attr['path']);
	}
	
	public function parser_replace($tag, $attr) {
		if(!empty($this->areas[$attr['id']])) {
			return @$this->areas[$attr['id']];
		}
		else if(!empty($attr['default'])) {
			return $attr['default'];
		}
	}
	
	public function parser_area($tag, $attr) {
		if(!empty($attr['id'])) {
			if(empty($this->areas[$attr['id']])) {
				$this->areas[$attr['id']] = $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else {
				$this->areas[$attr['id']] .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}
	
}

?>