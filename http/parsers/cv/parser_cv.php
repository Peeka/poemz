<?php

class parser_cv {

	private $item = array();
	private $subitem = array();
	private $category = "";

	function __construct() {
	
	}
	
	public function parser_load_profile($tag, $attr) {
		if(isset($attr['id'])) {
			@$this->item =  mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_profil WHERE account_id = '{$attr['id']}'"));
		}
	}
	
	public function parser_category($tag, $attr) {
		return $this->category;
	}
	
	public function parser_categories($tag, $attr) {
		$html = "";
		
		$cats = explode("]",str_replace("[","",$this->item['categories']));
		foreach($cats as $cat) {
			@$cat = mysql_fetch_array(mysql_query("SELECT name FROM arbetsgivare_job_categories WHERE id = '{$cat}'"));
			$this->category = $cat['name'];
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_email($tag, $attr) {
		@$get = mysql_fetch_array(mysql_query("SELECT email FROM accounts WHERE id = '". $this->item['account_id'] ."'"));
		return $get['email'];
	}
	
	public function parser_anstallningar($tag, $attr) {
		$html = "";
		
		@$info = mysql_query("SELECT * FROM arbetssokande_anstallningar WHERE account_id = '". $this->item['account_id'] ."'");
		while($anstallning = mysql_fetch_array($info)) {
			$this->subitem = $anstallning;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_utbildningar($tag, $attr) {
		$html = "";
		
		@$info = mysql_query("SELECT * FROM arbetssokande_utbildningar WHERE account_id = '". $this->item['account_id'] ."'");
		while($utbildning = mysql_fetch_array($info)) {
			$this->subitem = $utbildning;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_age($tag, $attr) {
		// calculate year
		$years = date("Y") - $this->item['birth_year'];
		// check if we've had a birthday this year
		if(strtotime(date("d-m-Y")) < strtotime($this->item['birth_day'].'-'.$this->item['birth_month'].'-'.date("Y"))) {
			$years--;
		}
		return $years;
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->subitem[$attr['get']]) > $attr['cut']) {
				return substr($this->subitem[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				if($attr['get'] == "from_month" || $attr['get'] == "to_month") {
					return ($this->subitem[$attr['get']] < 10) ? "0".$this->subitem[$attr['get']] : $this->subitem[$attr['get']];
				}
				else if($attr['get'] == "education_level") {
					switch ($this->subitem[$attr['get']]) {
						case 'grundskola':
							return "Grundskola"; break;
						case 'gymnasie':
							return "Gymnasie"; break;
						case 'hogskola':
							return "Högskola"; break;
					}
				}
				else {
					return $this->subitem[$attr['get']];
				}
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_profile_image($tag, $attr) {
		// check if our image status is 3 (complete), otherwise load the default user_no_image.jpg
		@$bild = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_bild WHERE account_id = '{$this->item['account_id']}'"));
		
		if($bild['step'] == 3) {
			return "user_{$this->item['account_id']}.jpg";
		}
		else {
			return "user_no_image.jpg";
		}
	}

}

?>