<?php

class parser_admin_language {

	private $item = array();
	private $subitem = array();
	private $rurl = array();
	private $translations = array();
	private $syntaxes = array();
	private $locales = array();
	private $translation = "";
	private $locale = "";
	private $syntax = "";

	function __construct() {
	
		if(isset($_POST['new_syntax_submit'])) {
			mysql_query("INSERT INTO language_syntaxes SET syntax = '{$_POST['new_syntax_syntax']}', description = '{$_POST['new_syntax_description']}'");
			mysql_query("INSERT INTO language_translations SET locale = 'sv_SE', syntax = '{$_POST['new_syntax_syntax']}', translation = '{$_POST['new_syntax_swedish']}'");
		}
		else if(isset($_POST)) {
			foreach($_POST as $key => $translation) {
			//	echo "$key => $value<br />";
				$temp = explode("__",$key);
				$syntax = $temp[0];
				$locale = $temp[1];
			/*
				exit("UPDATE language_translations
										SET translation = '{$translation}' 
										WHERE locale = '{$locale}' 
										AND syntax = '{$syntax}'");
			*/
				
				$check = mysql_fetch_array(mysql_query("SELECT id FROM language_translations  
										WHERE locale = '{$locale}' 
										AND syntax = '{$syntax}'"));
										
				if($check['id'] > 0) {
					mysql_query("UPDATE language_translations
									SET translation = '{$translation}' 
									WHERE locale = '{$locale}' 
									AND syntax = '{$syntax}'");
			
				}
				else {
					// There was no row already to update, let's create the translation
					mysql_query("INSERT INTO language_translations
									SET translation = '{$translation}', 
									locale = '{$locale}', 
									syntax = '{$syntax}'");
				}
			}
		}
	}
	
	public function parser_syntaxes($tag, $attr) {
		// Load the syntaxes
		$html = "";
		@$syntaxes = mysql_query("SELECT * FROM language_syntaxes ORDER BY id DESC");
		
		while($syntax = mysql_fetch_array($syntaxes)) {
			$this->syntax = $syntax;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_locales($tag, $attr) {
		// Load the locales
		$html = "";
		@$locales = mysql_query("SELECT * FROM language_locales ORDER BY name ASC");
		
		while($locale = mysql_fetch_array($locales)) {
			$this->locale = $locale;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_translation($tag, $attr) {
		@$translation = mysql_fetch_array(mysql_query("SELECT * FROM language_translations WHERE syntax = '{$this->syntax['syntax']}' AND locale = '{$this->locale['locale']}'"));
		$return = (isset($translation['translation']) && !empty($translation['translation'])) ? $translation['translation'] : '';
	return $return;
	}
	
	public function parser_locale($tag, $attr) {
		return $this->locale[$attr['get']];
	}
	
	public function parser_syntax($tag, $attr) {
		return $this->syntax[$attr['get']];
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			$get = $this->subitem[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}
	
	
}

?>