<?php

class parser_functions {

	public $item = array();
	public $months = array(1=>"Januari",
							2=>"Februari",
							3=>"Mars",
							4=>"April",
							5=>"Maj",
							6=>"Juni",
							7=>"Juli",
							8=>"Augusti",
							9=>"September",
							10=>"Oktober",
							11=>"November",
							12=>"December");

	function __construct() {
	
	}
	
	public function parser_upper($tag, $attr) {
		return mb_strtoupper($attr['string'],'UTF-8');
	}
	
	public function parser_date($tag, $attr) {
		if(is_numeric($attr['time'])) {
			return date("{$attr['date']}",$attr['time']);
		}
		else {
			$time = strtotime($attr['time']);
			return date("{$attr['date']}",$time);
		}
	}
	
	public function parser_proper_url($tag, $attr) {
		if(strstr("http://",$attr['url'])) {
			return $attr['url'];
		}
		else {
			return "http://".$attr['url'];
		}
	}
	
	public function parser_url_friendly($tag, $attr) {
		$url = str_replace("ö","o",strtolower($attr['string']));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = str_replace("ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function parser_month($tag, $attr) {
		return $this->months[$attr['number']];
	}
	
	public function parser_months($tag, $attr) {
		$html = "";
		foreach($this->months as $number => $name) {
			$this->item['number'] = $number;
			$this->item['name'] = $name;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_year($tag, $attr) {
		return date("Y");
	}
	
	public function parser_item($tag, $attr) {
		return $this->item[$attr['get']];
	}
	
	public function parser_loop($tag, $attr) {
		$html = "";
		if($attr['from'] > $attr['to']) {
			for($i=$attr['from'];$i>=$attr['to'];$i--) {
				$this->item['index'] = $i;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else {
			for($i=$attr['from'];$i<=$attr['to'];$i++) {
				$this->item['index'] = $i;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	return $html;
	}

}

?>