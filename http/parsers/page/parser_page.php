<?php

class parser_page {

	public $page = array();

	function __construct() {
		$this->page['rurl'] = $GLOBALS['page']->get('rurl');
	}
	
	public function parser_server_name($tag, $attr) {
		return SERVER_NAME;
	}
	
	public function parser_url_encode($tag, $attr) {
		return urlencode("http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}");
	}
	
	public function parser_internal_path($tag, $attr) {
		return $GLOBALS['page']->get("internal_path");
	}
	
	public function parser_external_path($tag, $attr) {
		return $GLOBALS['page']->get("external_path");
	}
	
	public function parser_rurl($tag, $attr) {
		if(isset($attr['equals'])) {
			if(@$this->page['rurl'][$attr['index']] == $attr['equals']) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if(isset($attr['not_equal'])) {
			if(@$this->page['rurl'][$attr['index']] != $attr['not_equal']) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if(isset($attr['is_int'])) {
			if(intval($this->page['rurl'][$attr['index']]) > 0) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if(isset($attr['not_contain'])) {
			if(!in_array($attr['not_contain'],$this->page['rurl'])) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else {
				return "";
			}
		}
		else if(isset($attr['contain'])) {
			if(in_array($attr['contain'],$this->page['rurl'])) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else {
				return "";
			}
		}
		else if(isset($attr['not_empty'])) {
			if(isset($attr['index']) && isset($this->page['rurl'][$attr['index']])) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else {
				return "";
			}
		}
		else if(isset($attr['empty'])) {
			if(isset($attr['index']) && empty($this->page['rurl'][$attr['index']])) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else {
				return "";
			}
		}
		else if(isset($attr['index'])) {
			return mysql_real_escape_string($this->page['rurl'][$attr['index']]);
		}
		else {
			return implode("/",$this->page['rurl']);
		}
	}

}

?>