<?php

class parser_events {

	public $categories 		= array();
	public $category 		= array();
	public $sub_category 	= array();
	public $event 			= array();
	public $shown_category 	= array();
	public $defined 		= array();
	public $errors 			= array();
	public $checked 		= false;
	public $selected 		= false;
	public $created 		= false;
	public $showCategories 	= "";
	public $error 			= "";

	function __construct() {
	
	}
	
	// EDIT
	
	
	
	
	
	
	
	
	
	
	
	// CREATE
	
	public function parser_create($tag, $attr) {
		if(isset($_POST['create_event_submit'])) {
			$createError = false;
		
			@$this->defined['create_event_category'] = mysql_real_escape_string($_POST['create_event_category']);
			@$this->defined['create_event_category_sub'] = mysql_real_escape_string($_POST['create_event_category_sub']);
			@$this->defined['create_event_name'] = mysql_real_escape_string($_POST['create_event_name']);
			@$this->defined['create_event_description'] = mysql_real_escape_string($_POST['create_event_description']);
			@$this->defined['create_event_tags'] = mysql_real_escape_string($_POST['create_event_tags']);
			
			if(empty($this->defined['create_event_category'])) {
				$this->errors[] = "You need to pick a category.";
			}
			if(empty($this->defined['create_event_category_sub'])) {
				$this->errors[] = "You need to pick a sub-category.";
			}
			if(empty($this->defined['create_event_name'])) {
				$this->errors[] = "You need to specify a name.";
			}
			if(empty($this->defined['create_event_description'])) {
				$this->errors[] = "You need to specify a description.";
			}
			
			if(!$createError) {
				$tagsHolder = "";
				
				// Everything passed the checks, now lets create the event
				// First we convert the tags
				
				$tags = explode(",",$this->defined['create_event_tags']);
				foreach($tags as $tag) { $tagsHolder[] = (!empty($tag)) ? "[". trim($tag) ."]" : ""; }
				$tags = ""; $tags = implode(",",$tagsHolder);
				
				mysql_query("INSERT INTO p_events_event SET 
							area = '". account::user('area') ."',
							name = '{$this->defined['create_event_name']}',
							description = '{$this->defined['create_event_description']}',
							category = '{$this->defined['create_event_category']}',
							sub_category = '{$this->defined['create_event_category_sub']}',
							creator = '". account::user('id') ."',
							tags = '{$tags}'");
							
				$this->created = true;
			}
		}
	}
	
	public function parser_show_errors($tag, $attr) {
		$html = "";
		if(!empty($this->errors)) {
			foreach($this->errors as $error) {
				$this->error = $error;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	return $html;
	}
	
	public function parser_list_events($tag, $attr) {
		$html = "";
		$sqlWhere = "";
		$sqlSuffix = " area = '". account::user('area') ."'";
		
		if($attr['own'] == "true") {
			$sqlWhere = $sqlSuffix ." AND creator = '". account::user('id') ."'";
		}
		else {
			// Check if we have selected specific categories
			if(isset($_POST['categories'])) {
				$nrCats = 0;
				foreach($_POST['categories'] as $chosenCategory) {
					$nrCats++;
					$sqlWhere .= ($nrCats >= 2) ? "OR {$sqlSuffix} AND category = '{$chosenCategory}'" : "{$sqlSuffix} AND category = '{$chosenCategory}'";
					$this->showCategories .= "All";
				}
			}
			else {
				$sqlWhere = $sqlSuffix;
				$this->showCategories .= "All";
			}
		}
		
		// Retrieve all the events and parse them
		$events = mysql_query("SELECT * FROM `p_events_event` WHERE{$sqlWhere}");
		while($event = mysql_fetch_array($events)) {
			// Save all events in an array, if we need them later
			$this->events[] = $event;
			
			// Process the current event
			$this->event = $event;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
		// Return them
		return $html;
	}
	
	public function parser_sub_categories($tag, $attr) {
		$html = "";
	
		if(isset($_POST['create_event_category']) && !empty($_POST['create_event_category'])) {
			$parent_category = mysql_real_escape_string($_POST['create_event_category']);
			// Retrieve all the sub_categories and parse them
			$sub_categories = mysql_query("SELECT * FROM `p_events_categories_sub` WHERE `parent` = '{$parent_category}' ORDER BY `name` ASC");
			while($sub_category = mysql_fetch_array($sub_categories)) {
				$this->selected = false;
				
				// Process the current sub_category
				$this->sub_category = $sub_category;
				
				// Check if its been defined in the create form
				if(isset($_POST['create_event_category_sub']) && $_POST['create_event_category_sub'] == $sub_category['id']) {
					$this->selected = true;
				}
				
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else {
			$html = $attr['empty'];
		}
		
		// Return them
		return $html;
	}
	
	public function parser_list_categories($tag, $attr) {
		$html = "";
	
		// Retrieve all the categories and parse them
		$categories = mysql_query("SELECT * FROM `p_events_categories` ORDER BY `name` ASC");
		while($category = mysql_fetch_array($categories)) {
			$this->checked = false;
			$this->selected = false;
			
			// Save all categories in an array, if we need them later
			$this->categories[$category['id']] = $category;
			
			// Process the current category
			$this->category = $category;
			
			// Check if its been defined in the create form
			if(isset($_POST['create_event_category']) && $_POST['create_event_category'] == $category['id']) {
				$this->selected = true;
			}
			
			// Check if it has been checked
			if(isset($_POST['categories'])) {
				if(in_array($category['id'],$_POST['categories'])) {
					$this->checked = true;
				}
			}
			
			@$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
		// Return them
		return $html;
	}
	
	public function parser_shown_categories($tag, $attr) {
		$html = "";
		
		// Check if we have selected specific categories
		if(isset($_POST['categories'])) {
			$nrCats = 0;
			foreach($_POST['categories'] as $chosenCategory) {
				$nrCats++;
				$this->shown_category = $this->categories[$chosenCategory];
				$html .= ($nrCats >= 2) ? $attr['divider'] ." ". $GLOBALS['parser']->parse($tag['innerhtml']) : $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else {
			$html .= "All categories";
		}
		
	return $html;
	}
	
	public function parser_selected($tag, $attr) {
		return ($this->selected) ? $attr['true'] : "";
	}
	
	public function parser_error($tag, $attr) {
		return $this->error;
	}
	
	public function parser_defined($tag, $attr) {
		return @$this->defined[$attr['get']];
	}
	
	public function parser_get_category($tag, $attr) {
		if(empty($this->categories[$attr['id']][$attr['get']])) { $this->parser_list_categories("",""); }
		return $this->categories[$attr['id']][$attr['get']];
	}
	
	public function parser_checked($tag, $attr) {
		return ($this->checked) ? $attr['true'] : "";
	}
	
	public function parser_shown_category($tag, $attr) {
		return $this->shown_category[$attr['get']];
	}
	
	public function parser_event($tag, $attr) {
		return $this->event[$attr['get']];
	}
	
	public function parser_category($tag, $attr) {
		return $this->category[$attr['get']];
	}
	
	public function parser_sub_category($tag, $attr) {
		return $this->sub_category[$attr['get']];
	}
	
}

?>