<?php

class parser_login {

	private $postedLogin;
	private $loginFailed;

	function __construct() {
		$this->postedLogin = false;
		$this->loginFailed = false;
	}
	
	public function parser_fb_update_account($tag, $attr) {
		@$first_name = account::user('first_name');
		@$last_name = account::user('last_name');
		@$groups = account::user('groups');
		if(empty($first_name) || empty($last_name) || empty($groups)) {
			if(isset($attr['redirect']) && !empty($attr['redirect'])) {
				header("Location: {$attr['redirect']}");
				exit;
			}
			else {
				return $tag['innerhtml'];
			}
		}
	}
	
	public function parser_facebook($tag, $attr) {
		   $code = mysql_real_escape_string($_REQUEST["code"]);
		   $app_id = 529919107039784;
		   $app_secret = "48ea75212d04733d78f29a6a3db6e8f9";
		   $my_url = "http://www.hrtorget.se".$attr['facebook_redirect'];

			if(empty($code)) {
				 $facebook_state = md5(uniqid(rand(), TRUE)); // CSRF protection
				 setcookie("facebook_state", $facebook_state, 0, "/");
				 
				 $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=" 
				   . $app_id . "&redirect_uri=" . urlencode($my_url) . "&state="
				   . $_SESSION['facebook_state'] . "&scope=email";

				header("Location: {$dialog_url}");
				exit;
			}
			else {
				if($_SESSION['facebook_state'] && ($_SESSION['facebook_state'] === $_REQUEST['state']) || isset($code) && !empty($code)) {
					// Vi har lyckats logga in
					$token_url = "https://graph.facebook.com/oauth/access_token?"
					   . "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
					   . "&client_secret=" . $app_secret . "&code=" . $code;

					$response = file_get_contents($token_url);
					$params = null;
					parse_str($response, $params);
					
					setcookie("facebook_access_token", $params['access_token'], 0, "/");

					$graph_url = "https://graph.facebook.com/me?access_token=" 
					   . $params['access_token'];

					$user = json_decode(file_get_contents($graph_url));
					if(!empty($user->email)) {
						$login = database::fetch("SELECT id,email FROM accounts WHERE email = '{$user->email}'");
		
						if($login['id'] > 0) {
							$uniqueHash = false;
							while(!$uniqueHash) {
								$rand = rand(0,999999);
								$loginHash = md5("hrtorget2387 {$login['id']} {$login['email']} {$rand}");
								@$check = database::fetch("SELECT id FROM accounts WHERE login_hash = '{$loginHash}'");
								if($check['id'] <= 0) {
									$uniqueHash = true;
								}
							}
							database::query("UPDATE accounts SET login_hash = '{$loginHash}', last_ip = '". $_SERVER['REMOTE_ADDR'] ."' WHERE id = '{$login['id']}'");
							setcookie("logincookie", $loginHash, 0, "/");
							
							header("Location: {$attr['redirect']}");
							exit;
						}
						else {
							// Create an account for the user
							database::query("INSERT INTO accounts SET email = '{$user->email}'");
							
							// And then try to login again
							$login = database::fetch("SELECT id,email FROM accounts WHERE email = '{$user->email}'");
		
							if($login['id'] > 0) {
								$uniqueHash = false;
								while(!$uniqueHash) {
									$rand = rand(0,999999);
									$loginHash = md5("hrtorget2387 {$login['id']} {$login['email']} {$rand}");
									@$check = database::fetch("SELECT id FROM accounts WHERE login_hash = '{$loginHash}'");
									if($check['id'] <= 0) {
										$uniqueHash = true;
									}
								}
								database::query("UPDATE accounts SET login_hash = '{$loginHash}', last_ip = '". $_SERVER['REMOTE_ADDR'] ."' WHERE id = '{$login['id']}'");
								setcookie("logincookie", $loginHash, 0, "/");
								
								header("Location: {$attr['redirect']}");
								exit;
							}
						}
					}
				}
				else {
					
				}
			}
	}
	
	public function parser_logout($tag, $attr) {
		setcookie("logincookie", $loginHash, -3600, "/");
		if(isset($attr['redirect']) && !empty($attr['redirect'])) {
			header("Location: {$attr['redirect']}");
		}
		else {
			header("Location: /");
		}
		exit;
	}
	
	public function parser_logged_in($tag, $attr) {
		if(isset($attr['redirect'])) {
			if(!isset($_COOKIE['logincookie']) && $attr['state'] == "false") {
				header("Location: {$attr['redirect']}");
				exit;
			}
			else if(isset($_COOKIE['logincookie']) && $attr['state'] == "true") {
				header("Location: {$attr['redirect']}");
				exit;
			}
		}
		else {
			if(isset($attr['state']) && $attr['state'] == "true" && isset($_COOKIE['logincookie'])) {
				return $tag['innerhtml'];
			}
			else if(isset($attr['state']) && $attr['state'] == "false" && !isset($_COOKIE['logincookie'])) {
				return $tag['innerhtml'];
			}
		}
	}
	
	public function parser_admin_access($tag, $attr) {
		$access = $GLOBALS['permissions']->check(1);
		if(!$access && $attr['state'] == "false") {
			header("Location: {$attr['redirect']}");
			exit;
		}
		else if($access && $attr['state'] == "true") {
			header("Location: {$attr['redirect']}");
			exit;
		}
	}

	public function parser_check_login($tag, $attr) {
		if(isset($_POST['login_submit']) && !isset($_COOKIE['logincookie'])) {
			$this->postedLogin = true;
		
			$loginEmail = database::escape($_POST['login_email']);
			$loginPassword = md5(database::escape($_POST['login_password']));
			$login = database::fetch("SELECT id,email FROM accounts WHERE email = '{$loginEmail}' AND password = '{$loginPassword}' AND status = 'active'");
			
			if($login['id'] > 0) {
				$uniqueHash = false;
				while(!$uniqueHash) {
					$rand = rand(0,999999);
					$loginHash = md5("hrtorget2387 {$login['id']} {$login['email']} {$rand}");
					@$check = database::fetch("SELECT id FROM accounts WHERE login_hash = '{$loginHash}'");
					if($check['id'] <= 0) {
						$uniqueHash = true;
					}
				}
				database::query("UPDATE accounts SET login_hash = '{$loginHash}', last_ip = '". $_SERVER['REMOTE_ADDR'] ."' WHERE id = '{$login['id']}'");
				setcookie("logincookie", $loginHash, 0, "/");
			//	setcookie("logincookie", $loginHash, time()+3600*24, "/");
				if(isset($attr['redirect']) && !empty($attr['redirect'])) {
					header("Location: {$attr['redirect']}");
				}
				else {
					header("Location: /admin");
				}
				exit;
			}
			else {
				$this->loginFailed = true;
			}
		}
	return "";
	}
	
	public function parser_failed($tag, $attr) {
		if(empty($_COOKIE['logincookie']) && $this->loginFailed && $this->postedLogin) {
			return $tag['innerhtml'];
		}
		else {
			return "";
		}
	}
	
	public function parser_success($tag, $attr) {
		if(!empty($_COOKIE['logincookie'])) {
			return $tag['innerhtml'];
		}
		else {
			return "";
		}
	}
	
	public function parser_form($tag, $attr) {
		if(empty($_COOKIE['logincookie'])) {
			return $tag['innerhtml'];
		}
		else {
			return "";
		}
	}

}

?>