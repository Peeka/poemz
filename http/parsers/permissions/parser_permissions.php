<?php

class parser_permissions {

	function __construct() {
	
	}
	
	public function parser_check($tag, $attr) {
		@$access = $GLOBALS['permissions']->check($attr['id']);
		
		if($attr['action'] == "redirect") {
			if(!$access && $attr['state'] == "false") {
				header("Location: {$attr['redirect']}");
				exit;
			}
			else if($access && $attr['state'] == "true") {
				header("Location: {$attr['redirect']}");
				exit;
			}
		}
		else if($attr['action'] == "innerhtml") {
			if(!$access && $attr['state'] == "false") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else if($access && $attr['state'] == "true") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}

}

?>