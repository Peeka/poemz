<?php

class parser_kop_salj {

	private $info = array();
	private $item = array();
	private $subitem = array();
	private $image = "";
	private $sql_category = "";
	private $sql_region = "";

	function __construct() {
	
		// Convert the post to our url friendly url instead
		$rurl = $GLOBALS['page']->get('rurl');
		$redirect = false;
		$reg = "_all";
		$cat = "_all";
		$page = isset($_POST['pager_button']) ? $_POST['pager_button'] : 1 ;
		
		// CHECK POST
		// REGION
		if(isset($_POST['search_regions']) && !empty($_POST['search_regions']) && $_POST['search_regions'] != '_all') {
			$reg = mysql_fetch_array(mysql_query("SELECT url_friendly FROM regions WHERE id = '{$_POST['search_regions']}'"));
			$reg = $reg['url_friendly'];
			$redirect = true;
		}
		// CATEGORY
		if(isset($_POST['search_categories']) && !empty($_POST['search_categories']) && $_POST['search_categories'] != '_all') {
			$cat = mysql_fetch_array(mysql_query("SELECT url_friendly FROM kop_salj_categories WHERE id = '{$_POST['search_categories']}'"));
			$cat = $cat['url_friendly'];
			$redirect = true;
		}
		
		// CHECK GET
		// REGION
		if(isset($rurl[3]) && !empty($rurl[3]) && $rurl[3] != '_all') {
			$reg = $rurl[3];
		}
		// CATEGORY
		if(isset($rurl[2]) && !empty($rurl[2]) && $rurl[2] != '_all') {
			$cat = $rurl[2];
		}
		
		// Check if we want to load a category
		@$name = $GLOBALS['page']->index(2);
		
		if(isset($name) && !empty($name) && $name != "_all") {
			$name = $this->url_friendly($name);
			$cat1 = mysql_fetch_array(mysql_query("SELECT id FROM kop_salj_categories WHERE url_friendly = '{$name}'"));
			if($cat1['id'] > 0) {
				@$this->sql_category = " category = '{$cat1['id']}' AND ";
			}
		}
		else {
			@$this->sql_category = (isset($_POST['search_categories']) && !empty($_POST['search_categories']) && $_POST['search_categories'] != "_all") ? " category = '{$_POST['search_categories']}' AND " : "";
		}
		
		// Check if we want to load a region
		@$name = $GLOBALS['page']->index(3);
		
		if(isset($name) && !empty($name) && $name != "_all") {
			$name = $this->url_friendly($name);
			$reg1 = mysql_fetch_array(mysql_query("SELECT id FROM regions WHERE url_friendly = '{$name}'"));
			if($reg1['id'] > 0) {
				@$this->sql_region = " AND region = '{$reg1['id']}'";
			}
		}
		else {
			@$this->sql_region = (isset($_POST['search_regions']) && !empty($_POST['search_regions']) && $_POST['search_regions'] != "_all") ? " AND region = '{$_POST['search_regions']}'" : "";
		}

		if(isset($_POST['pager_change']) || isset($_POST['search_submit'])) {
			header("Location: /till-salu/category/{$cat}/{$reg}/{$page}");
			exit;
		}

		if($rurl[0] == "till-salu" && empty($rurl[1])) {
			header("Location: /till-salu/category/_all/_all/1");
			exit;
		}
	
		// Delete any annons if requested
		if ($rurl[2] != "meddelanden") {
			if(isset($rurl[4]) && $rurl[4] == "radera") {
				@$owner = mysql_fetch_array(mysql_query("SELECT account_id FROM kop_salj WHERE id = '{$rurl[3]}'"));
			  	if($owner['account_id'] == account::user('id')) {
					mysql_query("DELETE FROM kop_salj WHERE id = '{$rurl[3]}'");
					header("Location: /konto/mina-annonser/kop-salj");
					exit;
				}
			}
			else if(isset($rurl[3]) && $rurl[3] == "forhandsgranska") {
				@$this->item = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj WHERE id = '{$rurl[4]}'"));
			}
			else if(isset($rurl[3]) && $rurl[3] == "accept") {
				mysql_query("UPDATE kop_salj SET status = 'accepted', status_comment = '{$_POST['kop_salj_status_comment']}' WHERE id = '{$rurl[4]}'");
			}
			else if(isset($rurl[3]) && $rurl[3] == "decline") {
				mysql_query("UPDATE kop_salj SET status = 'declined', status_comment = '{$_POST['kop_salj_status_comment']}' WHERE id = '{$rurl[4]}'");
			} else if (isset($rurl[3]) && $rurl[3] == "delete") {
                @$owner = mysql_fetch_array(mysql_query("SELECT account_id FROM kop_salj WHERE id = '{$rurl[4]}'"));
                if($owner['account_id'] == account::user('id')) {
					mysql_query("DELETE FROM kop_salj WHERE id = '{$rurl[4]}'");
				}
			} else if (isset($rurl[3]) && $rurl[3] == "redigera"){
			}
		}
		/* ccordina commented out php code since the meddelanden page has been removed due to request 
			else if($rurl[2] == "meddelanden") {
			if(isset($rurl[3]) && $rurl[3] == "accept") {
				@$message = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj_messages WHERE id = '{$rurl[4]}'"));
				@$annons = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj WHERE id = '{$message['annons_id']}'"));
				
				// mail the annons�r
				@$mail_template_1 = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 4"));
				$tags_1['annons_title'] = $annons['title'];
				$tags_1['name'] = $message['name'];
				$tags_1['message'] = $message['message'];
				$template_1['mail_title'] = $mail_template_1['mail_title'];
				$template_1['mail_text'] = $mail_template_1['mail_text'];
				
				// replace the tags
				// [link] = the link to the new password url
				$mail_1 = $GLOBALS['mail']->tags($tags_1,$template_1);
				$GLOBALS['mail']->send($annons['contact_email'],$mail_1['mail_title'],$mail_1['mail_text']);
				
				
				// mail sender
				@$mail_template_2 = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 3"));
				$tags_2['annons_title'] = $annons['title'];
				$tags_2['name'] = $message['name'];
				$tags_2['message'] = $message['message'];
				$template_2['mail_title'] = $mail_template_2['mail_title'];
				$template_2['mail_text'] = $mail_template_2['mail_text'];
				
				// replace the tags
				// [link] = the link to the new password url
				$mail_2 = $GLOBALS['mail']->tags($tags_2,$template_2);
				$GLOBALS['mail']->send($message['email'],$mail_2['mail_title'],$mail_2['mail_text']);
				
				mysql_query("UPDATE kop_salj_messages SET status = 'accepted' WHERE id = '{$rurl[4]}'");
			}
			else if(isset($rurl[3]) && $rurl[3] == "decline") {
				mysql_query("UPDATE kop_salj_messages SET status = 'declined' WHERE id = '{$rurl[4]}'");
			}
			else if(isset($rurl[3]) && $rurl[3] == "resend") {
				@$message = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj_messages WHERE id = '{$rurl[4]}'"));
				@$annons = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj WHERE id = '{$message['annons_id']}'"));
				
				// mail the annons�r
				@$mail_template_1 = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 4"));
				$tags_1['annons_title'] = $annons['title'];
				$tags_1['name'] = $message['name'];
				$tags_1['message'] = $message['message'];
				$template_1['mail_title'] = $mail_template_1['mail_title'];
				$template_1['mail_text'] = $mail_template_1['mail_text'];
				
				// replace the tags
				// [link] = the link to the new password url
				$mail_1 = $GLOBALS['mail']->tags($tags_1,$template_1);
				$GLOBALS['mail']->send($annons['contact_email'],$mail_1['mail_title'],$mail_1['mail_text']);
				
				// mail sender
				@$mail_template_2 = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 3"));
				$tags_2['annons_title'] = $annons['title'];
				$tags_2['name'] = $message['name'];
				$tags_2['message'] = $message['message'];
				$template_2['mail_title'] = $mail_template_2['mail_title'];
				$template_2['mail_text'] = $mail_template_2['mail_text'];
			}
		}*/
		
		// first we need to get the profile
		if(account::user('id') > 0) {
			$this->parser_get_info("",array("get"=>"kop_salj"));
		}
		
		if($rurl[1] >= 1) {
			@$this->item = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj WHERE id = '{$rurl[1]}'"));
		}
		
		if(isset($_POST['application_submit'])) {
			$id = account::user('id');
			@$account_id = ($id > 0) ? $id : 0;
			mysql_query("INSERT INTO kop_salj_messages SET 
							annons_id = '{$rurl[1]}', 
							account_id = '{$account_id}', 
							name = '{$_POST['application_name']}', 
							email = '{$_POST['application_email']}', 
							telephone = '{$_POST['application_telephone']}', 
							message = '{$_POST['application_message']}'");
		}
		
		
	}
	
	public function url_friendly($string) {
		$url = str_replace("�","o",strtolower($string));
		$url = str_replace("�","o",$url);
		$url = str_replace("�","a",$url);
		$url = str_replace("�","a",$url);
		$url = str_replace("�","a",$url);
		$url = str_replace("�","a",$url);
		$url = str_replace("�","e",$url);
		$url = str_replace("�","e",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function parser_meddelanden($tag, $attr) {
		$html = "";
		
		@$meddelanden = mysql_query("SELECT * FROM kop_salj_messages WHERE status = '{$attr['status']}'");
		while($meddelande = mysql_fetch_array($meddelanden)) {
			$this->item = $meddelande;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_status_comment($tag, $attr) {
		if(isset($this->info['kop_salj'][$attr['index']]['status_comment']) && !empty($this->info['kop_salj'][$attr['index']]['status_comment']) && $this->info['kop_salj'][$attr['index']]['status'] == "declined") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_total_items($tag, $attr) {
		$total = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS count FROM kop_salj WHERE{$this->sql_category} status = 'accepted'{$this->sql_region}"));
		return $total['count'];
	}
	
	public function parser_total_requests($tag, $attr) {
		$total = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total_items FROM kop_salj"));
		return $total['total_items'];
	}
	
	public function parser_requests($tag, $attr) {
		$html = "";
		$limit = " LIMIT ".$GLOBALS['pager']->limit();

        if(isset($attr['status']) && $attr['status'] != "")
            @$annonser = mysql_query("SELECT * FROM kop_salj WHERE status = '{$attr['status']}' ORDER BY Added DESC {$limit}");
	    else @$annonser = mysql_query("SELECT * FROM kop_salj ORDER BY Added DESC {$limit}");

		while($annons = mysql_fetch_array($annonser)) {
			$this->item = $annons;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_category($tag, $attr) {
		@$category = mysql_fetch_array(mysql_query("SELECT * FROM kop_salj_categories WHERE id = '{$attr['id']}'"));
		if(isset($attr['get'])) {
			return $category[$attr['get']];
		}
		else {
			return $category['title'];
		}
	}
	
	public function parser_region($tag, $attr) {
		@$region = mysql_fetch_array(mysql_query("SELECT * FROM regions WHERE id = '{$attr['id']}'"));
		if(isset($attr['get'])) {
			return $region[$attr['get']];
		}
		else {
			return $region['name'];
		}
	}
	
	public function parser_selected_region($tag, $attr) {
		$reg = $GLOBALS['page']->index(3);
		$reg = mysql_fetch_array(mysql_query("SELECT id FROM regions WHERE url_friendly = '{$reg}'"));
		if(intval($attr['id']) == intval($reg['id'])) {
			return $GLOBALS['parser']->parse($attr['return']);
		}
	}
	
	public function parser_selected_category($tag, $attr) {
		$cat = $GLOBALS['page']->index(2);
		$cat = mysql_fetch_array(mysql_query("SELECT id FROM kop_salj_categories WHERE url_friendly = '{$cat}'"));
		if(intval($attr['id']) == intval($cat['id'])) {
			return $GLOBALS['parser']->parse($attr['return']);
		}
	}
	
	public function parser_all_annonser($tag, $attr) {
		$html = "";
		
		if(isset($attr['limit'])) {
			@$limit = (isset($attr['limit']) && !empty($attr['limit']) && $attr['limit'] != "_all") ? " LIMIT 0,{$attr['limit']}" : "";
		}
		else {
			$limit = " LIMIT ".$GLOBALS['pager']->limit();
		}
	
	//	exit("SELECT * FROM kop_salj WHERE{$this->sql_category} status = 'accepted'{$this->sql_region} ORDER BY added DESC{$limit}");
		@$annonser = mysql_query("SELECT * FROM kop_salj WHERE{$this->sql_category} status = 'accepted'{$this->sql_region} ORDER BY added DESC{$limit}");
		while($annons = mysql_fetch_array($annonser)) {
			$this->item = $annons;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_get_image($tag, $attr) {
		if(strstr($this->item[$attr['get']],"http")) {
			return $this->item[$attr['get']];
		}
		else {
			return $attr['prefix'].$this->item['image_1'];
		}
	}
	
	public function parser_show_image($tag, $attr) {
		if(!empty($attr['image']) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($attr['image']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_has_image($tag, $attr) {
		@$image = $this->info['kop_salj'][$attr['id']]["image_{$attr['index']}"];
		if(!empty($image)) {
			$this->image['image'] = $image;
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_publicera($tag, $attr) {
		mysql_query("UPDATE kop_salj SET status = 'waiting' WHERE id = '{$attr['id']}' AND account_id = '". account::user('id') ."'");
		header("Location: {$attr['success_redirect']}");
		exit;
	}
	
	public function parser_owns_annons($tag, $attr) {
		foreach($this->info['kop_salj'] as $annons) {
			if(!isset($attr['id'])) {
				if($annons['account_id'] == account::user('id') && $attr['state'] == "true") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else if($annons['account_id'] != account::user('id') && $attr['state'] == "false") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
			else {
			if($annons['id'] == $attr['id']) {
				if($annons['account_id'] == account::user('id') && $attr['state'] == "true") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else if($annons['account_id'] != account::user('id') && $attr['state'] == "false") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
			}
		}
	}
	
	public function parser_images($tag, $attr) {
		$gotImage = false;
		$html = "";
		$images[0] = $this->item['image_1'];
		$images[1] = $this->item['image_2'];
		$images[2] = $this->item['image_3'];
		$images[3] = $this->item['image_4'];
		$images[4] = $this->item['image_5'];
		$images[5] = $this->item['image_6'];
		$images[6] = $this->item['image_7'];
		$images[7] = $this->item['image_8'];
		$images[8] = $this->item['image_9'];
		$images[9] = $this->item['image_10'];
		
		foreach($images as $i => $image) {
			if(!empty($image)) {
				$gotImage = true;
				$this->image['index'] = $i;
				$this->image['image'] = $image;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		
		if(isset($attr['check']) && $gotImage == true) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $html;
		}
	}
	
	public function parser_image($tag, $attr) {
		if($attr['get'] == "thumb") {
			@$extension = end(explode(".", $this->image['image']));
			return str_replace(".".$extension,"",$this->image['image'])."_thumb.". $extension;
		}
		else {
			return @$this->image[$attr['get']];
		}
	}
	
	public function parser_regions($tag, $attr) {
		$html = "";
		@$regions = mysql_query("SELECT * FROM regions ORDER BY sort ASC");
		while($region = mysql_fetch_array($regions)) {
			$this->item = $region;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_categories($tag, $attr) {
		$html = "";
		@$categories = mysql_query("SELECT * FROM kop_salj_categories ORDER BY title ASC");
		while($category = mysql_fetch_array($categories)) {
			$this->item = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_get_info($tag, $attr) {
		if($attr['get'] == "kop_salj") {
			@$info = mysql_query("SELECT * FROM kop_salj WHERE account_id = '". account::user('id') ."'");
			while($item = mysql_fetch_array($info)) {
				$this->info['kop_salj'][$item['id']] = $item;
			}
		}
	}
	
	public function parser_annonser($tag, $attr) {
		$html = "";
		foreach($this->info['kop_salj'] as $annons) {
			$this->item = $annons;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_status($tag, $attr) {
		if($attr['status'] == $attr['equals']) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_update_info($tag, $attr) {
		if(isset($_POST['kop_salj_submit']) || isset($_POST['kop_salj_edit']) || isset($_POST['kop_salj_preview'])) {
		
			$id = $GLOBALS['parser']->parse("{page:rurl index='4'/}");
			
			// save the information to the database
			if(isset($_POST['kop_salj_edit'])) {
				mysql_query("UPDATE kop_salj SET 
								category = '{$_POST['kop_salj_category']}', 
								title = '{$_POST['kop_salj_title']}', 
								price = '{$_POST['kop_salj_price']}', 
								description = '{$_POST['kop_salj_description']}', 
								contact_email = '{$_POST['kop_salj_contact_email']}', 
								contact_name = '{$_POST['kop_salj_contact_name']}', 
								contact_phone_1 = '{$_POST['kop_salj_contact_phone_1']}', 
								contact_phone_2 = '{$_POST['kop_salj_contact_phone_2']}', 
								city = '{$_POST['kop_salj_city']}', 
								region = '{$_POST['kop_salj_region']}', 
								status = 'waiting' 
								WHERE id = '". $id ."' AND account_id = '". account::user('id') ."'");
			}
			else if(isset($_POST['kop_salj_submit'])) {
				mysql_query("INSERT INTO kop_salj SET 
								account_id = '". account::user('id') ."', 
								category = '{$_POST['kop_salj_category']}', 
								title = '{$_POST['kop_salj_title']}', 
								price = '{$_POST['kop_salj_price']}', 
								description = '{$_POST['kop_salj_description']}', 
								contact_email = '{$_POST['kop_salj_contact_email']}', 
								contact_name = '{$_POST['kop_salj_contact_name']}', 
								contact_phone_1 = '{$_POST['kop_salj_contact_phone_1']}', 
								contact_phone_2 = '{$_POST['kop_salj_contact_phone_2']}', 
								city = '{$_POST['kop_salj_city']}', 
								region = '{$_POST['kop_salj_region']}', 
								added = '". time() ."'
								");
				$id = mysql_insert_id();
			}
			else if(isset($_POST['kop_salj_preview'])) {
				if($id <= 0) {
					mysql_query("INSERT INTO kop_salj SET 
								account_id = '". account::user('id') ."', 
								category = '{$_POST['kop_salj_category']}', 
								title = '{$_POST['kop_salj_title']}', 
								price = '{$_POST['kop_salj_price']}', 
								description = '{$_POST['kop_salj_description']}', 
								contact_email = '{$_POST['kop_salj_contact_email']}', 
								contact_name = '{$_POST['kop_salj_contact_name']}', 
								contact_phone_1 = '{$_POST['kop_salj_contact_phone_1']}', 
								contact_phone_2 = '{$_POST['kop_salj_contact_phone_2']}', 
								city = '{$_POST['kop_salj_city']}', 
								region = '{$_POST['kop_salj_region']}', 
								added = '". time() ."', 
								status = 'draft' 
								");
					$id = mysql_insert_id();
				}
				else {
					mysql_query("UPDATE kop_salj SET 
								category = '{$_POST['kop_salj_category']}', 
								title = '{$_POST['kop_salj_title']}', 
								price = '{$_POST['kop_salj_price']}', 
								description = '{$_POST['kop_salj_description']}', 
								contact_email = '{$_POST['kop_salj_contact_email']}', 
								contact_name = '{$_POST['kop_salj_contact_name']}', 
								contact_phone_1 = '{$_POST['kop_salj_contact_phone_1']}', 
								contact_phone_2 = '{$_POST['kop_salj_contact_phone_2']}', 
								city = '{$_POST['kop_salj_city']}', 
								region = '{$_POST['kop_salj_region']}', 
								status = 'draft' 
								WHERE id = '". $id ."' AND account_id = '". account::user('id') ."'");
				}
			}
			
			if(isset($_POST['kop_salj_delete_image']) && !empty($_POST['kop_salj_delete_image'])) {
				foreach($_POST['kop_salj_delete_image'] as $index) {
					@$get = mysql_fetch_array(mysql_query("SELECT `image_{$index}` FROM kop_salj WHERE id = '". $id ."' AND account_id = '". account::user('id') ."'"));
					if(!empty($get["image_{$index}"])) {
						if(file_exists(INTERNAL_PATH ."/public/images/kop_salj/". $get["image_{$index}"])) {
							unlink(INTERNAL_PATH ."/public/images/kop_salj/". $get["image_{$index}"]);
						
							// then we delete the thumb image
							@$extension = end(explode(".", $get["image_{$index}"]));
							$thumb = str_replace(".".$extension,"",$get["image_{$index}"])."_thumb.". $extension;
							unlink(INTERNAL_PATH ."/public/images/kop_salj/". $thumb);
							
							mysql_query("UPDATE kop_salj SET `image_{$index}` = '' WHERE id = '". $id ."' AND account_id = '". account::user('id') ."'");
						}
					}
				}
			}

			if(isset($_FILES)) {
				@$files = $this->fix_files_array($_FILES['kop_salj_bild2']);
			#	exit(var_dump($_FILES["kop_salj_bild"]["name"][0]));
				$i = 1;
				foreach($files as $file) { exit("hm");
				
					@$allowedExts = array("jpg", "jpeg", "gif", "png");
					@$extension = end(explode(".", $file["name"]));
					
					if(in_array($extension, $allowedExts)) {
						$newName = "kop_salj_{$id}_{$i}.{$extension}";
						$thumbName = "kop_salj_{$id}_{$i}_thumb.{$extension}";
						
						if(file_exists(INTERNAL_PATH ."/public/images/kop_salj/". $newName)) {
							unlink(INTERNAL_PATH ."/public/images/kop_salj/". $newName);
						}
						
						// Create the big image
						if(move_uploaded_file($file["tmp_name"], INTERNAL_PATH ."/public/images/kop_salj/". $newName)) {
							image::resize(INTERNAL_PATH ."/public/images/kop_salj/". $newName,
												INTERNAL_PATH ."/public/images/kop_salj/". $newName,
												471,
												360);
												
							// then we create the thumb aswell
							image::resize(INTERNAL_PATH ."/public/images/kop_salj/". $newName,
												INTERNAL_PATH ."/public/images/kop_salj/". $thumbName,
												90,
												60);
							
							mysql_query("UPDATE kop_salj SET image_{$i} = '{$newName}' WHERE id = '{$id}' AND account_id = '". account::user('id') ."'");
						}
							
					}
					
				$i++;
				}
			}
			
			if(isset($_POST['kop_salj_preview'])) {
				header("Location: /konto/mina-annonser/kop-salj/forhandsgranska/{$id}");
				exit;
			}
			
			// now lets redirect if there is a redirect
			if(!empty($attr['success_redirect'])) {
				header("Location: {$attr['success_redirect']}");
				exit;
			}
		}
	}
	
	public function fix_files_array($arr) {
		$i = 0;
		foreach($arr as $key => $all) {
			foreach($all as $i => $val) {
				$new[$i][$key] = $val;   
			}   
		}
		return $new;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			
			if(isset($attr['if_not_exist']) && !strstr($this->item[$attr['get']],$attr['if_not_exist'])) {
				$get = $this->item[$attr['get']] . $attr['add'];
			}
			else {
				$get = $this->item[$attr['get']];
			}
		
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			
			$get = str_replace("float: left;","",$get);
			$get = str_replace("float: left","",$get);
			$get = str_replace("float:left","",$get);
			
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				return $GLOBALS['text']->cut($get, intval($attr['cut'])).$attr['add'];
			}
			else {
				return $get;
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			if(isset($attr['remove_br'])) {
				$get = strip_tags($this->subitem[$attr['get']]);
			}
			else {
				$get = $this->subitem[$attr['get']];
			}
			
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				return $GLOBALS['text']->cut($get, intval($attr['cut'])).$attr['add'];
			}
			else {
				return $get;
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_info($tag, $attr) {
		// Posted values
		if(isset($attr['posted_first']) && !empty($attr['posted_first']) && isset($_POST["{$attr['get']}_{$attr['name']}"]) && !empty($_POST["{$attr['get']}_{$attr['name']}"])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $_POST["{$attr['get']}_{$attr['name']}"]) {
					return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
				}
				else {
					return "";
				}
			}
			else {
				return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
			}
		} // Stored values
		else if(isset($this->info[$attr['get']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['name']]) || 
		isset($attr['index']) && isset($this->info[$attr['get']][$attr['index']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['index']][$attr['name']])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $this->info[$attr['get']][$attr['name']] || isset($attr['index']) && $attr['value'] == $this->info[$attr['get']][$attr['index']][$attr['name']]) {
					if(isset($attr['index'])) {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
					}
					else {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
					}
				}
				else {
					return "";
				}
			}
			else {
				if(isset($attr['index'])) {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
				}
				else {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
				}
			}
		}
		else {
			return "";
		}
	}

}

?>