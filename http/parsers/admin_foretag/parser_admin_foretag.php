<?php

class parser_admin_foretag {

	private $item = array();
	private $subitem = array();
	private $category = "";
	private $sort_order = "";
	private $sort_column = "";
	private $loaded_img = 0;
	private $has_images = false;

	function __construct() {
		@$id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
		@$id4 = $GLOBALS['parser']->parse("{page:rurl index='4'/}");
		if($id > 0) {
			$this->item = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$id}'"));
			$this->image_count = mysql_fetch_array(mysql_query("SELECT count(company_id) AS image_count FROM arbetsgivare_companies_images_new WHERE company_id = '{$id}' and active='1'"));
		}
		
		@$company_id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
		
		// COMPANY PRODUCTS
		if(isset($_POST['product_new'])) {
			@$last = mysql_fetch_array(mysql_query("SELECT position FROM arbetsgivare_products WHERE company_id = '{$company_id}' ORDER BY position DESC LIMIT 0,1"));
			@$position = $last['position'] + 1;
			mysql_query("INSERT INTO arbetsgivare_products SET
										company_id = '{$company_id}', 
										title = '{$_POST['product_title']}', 
										text = '{$_POST['product_text']}', 
										url = '{$_POST['product_url']}',
										position = '{$position}'
										");

			@$id = mysql_insert_id();

			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["product_bild"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "product_".$id;
				
				if(file_exists(INTERNAL_PATH."/public/images/products/". $newName . $extension)) {
					unlink(INTERNAL_PATH."/public/images/products/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the big image
				if(move_uploaded_file($_FILES["product_bild"]["tmp_name"], INTERNAL_PATH."/public/images/products/". $newFileBig)) {
					image::resize(INTERNAL_PATH."/public/images/products/". $newFileBig,
										INTERNAL_PATH."/public/images/products/". $newFileBig,
										170,
										200);
					mysql_query("UPDATE arbetsgivare_products SET image = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			header("Location: /admin/branschguiden/foretag/".$company_id);
			exit;
		}
		else if(isset($_POST['product_edit'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
			@$last = mysql_fetch_array(mysql_query("SELECT position FROM arbetsgivare_products WHERE company_id = '{$company_id}' ORDER BY position DESC LIMIT 0,1"));
			@$position = $last['position'] + 1;
			mysql_query("UPDATE arbetsgivare_products SET  
										title = '{$_POST['product_title']}', 
										text = '{$_POST['product_text']}', 
										url = '{$_POST['product_url']}'
										WHERE id = '{$_POST['product_id']}'");
			$id = $_POST['product_id'];
			
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["product_image"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "product_".$id;
				
				if(file_exists(INTERNAL_PATH."/public/images/products/". $newName . $extension)) {
					unlink(INTERNAL_PATH."/public/images/products/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the big image
				if(move_uploaded_file($_FILES["product_image"]["tmp_name"], INTERNAL_PATH."/public/images/products/". $newFileBig)) {
					image::resize(INTERNAL_PATH."/public/images/products/". $newFileBig,
										INTERNAL_PATH."/public/images/products/". $newFileBig,
										170,
										200);
					mysql_query("UPDATE arbetsgivare_products SET image = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			header("Location: /admin/branschguiden/foretag/".$company_id);
			exit;
		}
		else if(isset($_GET['delete_product'])) {
			@$delete_product = $_GET['delete_product'];
			@$last = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies_images_new WHERE id = '{$delete_product}'"));
			
			if(file_exists(INTERNAL_PATH."/public/images/products/". $last['image'])) {
				unlink(INTERNAL_PATH."/public/images/products/". $last['image']);
			}
			
			mysql_query("DELETE FROM arbetsgivare_companies_images_new WHERE id = '{$delete_product}'");
			
			header("Location: /admin/branschguiden/redigera/".$company_id);
			exit;
		}
		else if(isset($_POST['product_delete'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
			@$last = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'"));
			
			if(file_exists(INTERNAL_PATH."/public/images/products/". $last['image'])) {
				unlink(INTERNAL_PATH."/public/images/products/". $last['image']);
			}
			
			mysql_query("DELETE FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'");
			
			header("Location: /admin/branschguiden/foretag/".$company_id);
			exit;
		}
		else if(isset($_POST['product_move_up'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
			
			@$product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'"));
			@$other_position = $product['position'] - 1;
			@$other_product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE company_id = '{$company_id}' AND position = '{$other_position}'"));


			mysql_query("UPDATE arbetsgivare_products SET position = '{$other_product['position']}' WHERE id = '{$product['id']}'");
			mysql_query("UPDATE arbetsgivare_products SET position = '{$product['position']}' WHERE id = '{$other_product['id']}'");
			
			header("Location: /admin/branschguiden/foretag/".$company_id);
			exit;
		}
		else if(isset($_POST['product_move_down'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
			
			@$product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'"));
			@$other_position = $product['position'] + 1;
			@$other_product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE company_id = '{$company_id}' AND position = '{$other_position}'"));


			mysql_query("UPDATE arbetsgivare_products SET position = '{$other_product['position']}' WHERE id = '{$product['id']}'");
			mysql_query("UPDATE arbetsgivare_products SET position = '{$product['position']}' WHERE id = '{$other_product['id']}'");
			
			header("Location: /konto/arbetsgivare/foretag/redigera/".$company_id);
			exit;
		}
		else if($id4 == 'delete_banner_flash') {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
			@$flash = mysql_fetch_array(mysql_query("SELECT banner_flash FROM arbetsgivare_companies WHERE id = '{$company_id}'"));
			
			if(file_exists(INTERNAL_PATH."/public/images/advertisement/". $flash['banner_flash'])) {
				unlink(INTERNAL_PATH."/public/images/advertisement/". $flash['banner_flash']);
			}
			
			mysql_query("UPDATE arbetsgivare_companies SET banner_flash = '' WHERE id = '{$company_id}'");
			
			header("Location: /admin/branschguiden/redigera/".$company_id);
			exit;
		}
		else if($id4 == 'delete_banner_fallback') {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
			@$flash = mysql_fetch_array(mysql_query("SELECT banner_fallback FROM arbetsgivare_companies WHERE id = '{$company_id}'"));
			
			if(file_exists(INTERNAL_PATH."/public/images/advertisement/". $flash['banner_fallback'])) {
				unlink(INTERNAL_PATH."/public/images/advertisement/". $flash['banner_fallback']);
			}
			
			mysql_query("UPDATE arbetsgivare_companies SET banner_fallback = '' WHERE id = '{$company_id}'");
			
			header("Location: /admin/branschguiden/redigera/".$company_id);
			exit;
		}

		if($GLOBALS['parser']->parse("{page:rurl index='2'/}") == "radera") {
			$id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
			@$check = mysql_fetch_array(mysql_query("SELECT owner FROM arbetsgivare_companies WHERE id = '{$id}'"));
			$owner = $check['owner'];

			mysql_query("DELETE FROM arbetsgivare_companies WHERE id = '{$id}'");
			$this->update_companies($id,"remove",$owner);

			header("Location: /admin/branschguiden/foretag");
			exit;
		}

	}

    public function createCompanyMobiles($company_mobiles,$company_id) {
        for($i=1;$i<sizeOf($company_mobiles);$i++){
            mysql_query("INSERT INTO arbetsgivare_companies_mobile
                        (company_id , mobile, active)
                        VALUES
                        ('".$company_id."','".$company_mobiles[$i]."','1')");
        }
    }
	
	public function updateCompanyMobiles($company_id) {
        mysql_query("DELETE FROM arbetsgivare_companies_mobile WHERE company_id='".$company_id."'");
    }
	
	public function createCompanyTelephones($company_telephones,$company_id) {
        for($i=1;$i<sizeOf($company_telephones);$i++){
            mysql_query("INSERT INTO arbetsgivare_companies_telephone
                        (company_id , telephone, active)
                        VALUES
                        ('".$company_id."','".$company_telephones[$i]."','1')");
        }
    }

    public function updateCompanyTelephones($company_id) {
        mysql_query("DELETE FROM arbetsgivare_companies_telephone WHERE company_id='".$company_id."'");
    }
	
	public function createCompanyWebsites($company_websites,$company_id) {
        for($i=1;$i<sizeOf($company_websites,$company_id);$i++){
            mysql_query("INSERT INTO arbetsgivare_companies_website
                        (company_id , website, active)
                        VALUES
                        ('".$company_id."','".$company_websites[$i]."','1')");
        }
    }

    public function updateCompanyWebsites($company_id){
        mysql_query("DELETE FROM arbetsgivare_companies_website WHERE company_id='".$company_id."'");
    }
	
	public function createCompanyAddresses($company_streets,$company_zips,$company_cities,$company_sections,$company_id){
        for($i=1;$i<=sizeOf($company_sections);$i++){
            mysql_query("INSERT INTO arbetsgivare_companies_address
                        (company_id , section, street, city, zip, active)
                        VALUES
                        ('".$company_id."','".$company_sections[$i-1]."','".$company_streets[$i]."','".$company_cities[$i]."','".$company_zips[$i]."','1')");
        }
    }

    public function updateCompanyAddresses($company_id) {
        mysql_query("DELETE FROM arbetsgivare_companies_address WHERE company_id='".$company_id."'");
    }

	public function parser_update_info($tag, $attr) {

		// update the personuppgifter
		if(isset($_POST['companies_submit'])) {
		
			$url_friendly = $this->url_friendly($_POST['companies_name']);

			// Fix the categories
			$categories = "";
			foreach($_POST['companies_categories'] as $category) {
				if(!empty($category)) { $categories .= "[{$category}]"; }
			}

			$companies_owner = (intval($_POST['companies_owner']) > 0) ? $_POST['companies_owner'] : 0;

			$access_branschguiden = (isset($_POST['companies_access_branschguiden'])) ? "true" : "false";
			$access_platsbanken = (isset($_POST['companies_access_platsbanken'])) ? "true" : "false";

            //ccordina
            $date_created = date("Y-m-d H:i:s");
			
			/*
			$tags = json_decode($_POST['companies_tags']);
			foreach($tags AS $tag) {
				if(!empty($tag) && $tag != "[]" && $tag != '""') {
					$tagz[] = $tag;
				}
			}
			$tagz = (sizeOf($tagz) > 0) ? $tagz : '';
			*/
			
			$tagz = explode("\r\n",$_POST['companies_tags']);
			$tagz = implode("|",$tagz);
			
			// save the information to the database
			if(isset($_POST['companies_edit']) && !empty($_POST['companies_edit'])) {
			
				@$check = mysql_fetch_array(mysql_query("SELECT owner FROM arbetsgivare_companies WHERE id = '{$_POST['companies_edit']}'"));
				if($check['owner'] != $companies_owner) {
					$this->update_companies($_POST['companies_edit'],"remove",$check['owner']);
				}

				mysql_query("UPDATE arbetsgivare_companies SET
								owner = '{$companies_owner}',
								name = '{$_POST['companies_name']}',
								type = '{$_POST['companies_type']}',
								description = '{$_POST['companies_description']}',
								org_number = '{$_POST['companies_org_number']}',
								street = '{$_POST['companies_street'][0]}',
								city = '{$_POST['companies_city'][0]}',
								zip = '{$_POST['companies_zip'][0]}',
								website = '{$_POST['companies_website'][0]}',
								categories = '{$categories}',
								contact_mail = '{$_POST['companies_contact_mail']}',
								telephone = '{$_POST['companies_telephone'][0]}',
								mobile = '{$_POST['companies_mobile'][0]}',
								url_friendly = '{$url_friendly}',
								fax = '{$_POST['companies_fax']}',
								facebook = '{$_POST['companies_facebook']}',
								instagram = '{$_POST['companies_instagram']}',
								twitter = '{$_POST['companies_twitter']}',
								access_branschguiden = '{$access_branschguiden}',
								access_platsbanken = '{$access_platsbanken}',
                                tags= '". $tagz ."',
                                banner_url= '{$_POST['companies_banner_url']}' 
								WHERE id = '{$_POST['companies_edit']}'");

                //delete company telephones
                $this->updateCompanyTelephones($_POST['companies_edit']);
				
				//delete company mobiles
                $this->updateCompanyMobiles($_POST['companies_edit']);

				//delete company websites
                $this->updateCompanyWebsites($_POST['companies_edit']);
				
				//delete company addresses
                $this->updateCompanyAddresses($_POST['companies_edit']);
				
                //re-create company telephones
                $this->createCompanyTelephones($_POST['companies_telephone'],$_POST['companies_edit']);
				
				//re-create company mobiles
                $this->createCompanyMobiles($_POST['companies_mobile'], $_POST['companies_edit']);
				
				//re-create company websites
                $this->createCompanyWebsites($_POST['companies_website'], $_POST['companies_edit']);
				
				//re-create company addresses
                $this->createCompanyAddresses($_POST['companies_street'], $_POST['companies_zip'], $_POST['companies_city'], $_POST['companies_section'], $_POST['companies_edit']);
			}
			else {
				mysql_query("INSERT INTO arbetsgivare_companies SET
								owner = '{$companies_owner}',
								name = '{$_POST['companies_name']}',
								type = '{$_POST['companies_type']}',
								description = '{$_POST['companies_description']}',
								org_number = '{$_POST['companies_org_number']}',
								street = '{$_POST['companies_street'][0]}',
								city = '{$_POST['companies_city'][0]}',
								zip = '{$_POST['companies_zip'][0]}',
								website = '{$_POST['companies_website'][0]}',
								categories = '{$categories}',
								contact_mail = '{$_POST['companies_contact_mail']}',
								telephone = '{$_POST['companies_telephone'][0]}',
								mobile = '{$_POST['companies_mobile'][0]}',
								url_friendly = '{$url_friendly}',
								fax = '{$_POST['companies_fax']}',
								facebook = '{$_POST['companies_facebook']}',
								instagram = '{$_POST['companies_instagram']}',
								twitter = '{$_POST['companies_twitter']}',
								access_branschguiden = '{$access_branschguiden}',
								access_platsbanken = '{$access_platsbanken}',
                                date_created = '{$date_created}',
                                tags ='". $tagz ."',
                                banner_url= '{$_POST['companies_banner_url']}'");

				// Then we update the arbetsgivare_accounts, and set the new company id
				$company_id = mysql_insert_id();

                $this->createCompanyTelephones($_POST['companies_telephone'],$company_id);
				
                $this->createCompanyMobiles($_POST['companies_mobile'],$company_id);
				
				$this->createCompanyWebsites($_POST['companies_website'], $company_id);
				
				$this->createCompanyAddresses($_POST['companies_street'], $_POST['companies_zip'], $_POST['companies_city'], $_POST['companies_section'], $_POST['companies_edit']);
			}

			$id = (!empty($company_id)) ? $company_id : $_POST['companies_edit'];

			$this->update_companies($id,"add",$_POST['companies_owner']);

			if(isset($_POST['company_logo_radera']) && ($_POST['company_logo_radera'] != "")) {
				@$image = mysql_fetch_array(mysql_query("SELECT image_big, image_thumb FROM arbetsgivare_companies WHERE id = '". $_POST['company_logo_radera'] ."'"));
				unlink(INTERNAL_PATH."/public/images/products/". $image['image_big']);
				unlink(INTERNAL_PATH."/public/images/products/". $image['image_thumb']);
				mysql_query("UPDATE arbetsgivare_companies SET image_big = '', image_thumb = '' WHERE id = '". $_POST['company_logo_radera'] ."'");
			}

			
			if(isset($_POST['company_image_radera']) && ($_POST['company_image_radera'] != "")) {
				$image = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies_images_new WHERE company_id = '". $id ."' and id='". $_POST['company_image_radera'] ."'"));
				if ((!empty($image['image_thumb'])) && (!empty($image['image_big']))) {			
			
					unlink(INTERNAL_PATH."/public/images/products/{$image['image_thumb']}");
					unlink(INTERNAL_PATH."/public/images/products/{$image['image_big']}");
					
					mysql_query("DELETE FROM arbetsgivare_companies_images_new 
								  WHERE company_id='".$id."' and id='".$_POST['company_image_radera']."'");
				}
			}

			
			// Check if we have a banner to upload
			if (isset($_FILES['company_banner_flash']) || isset($_FILES['company_banner_fallback'])) {
				$this->createCompanyBanner($_FILES,$id);
			}
			
			// Check if we have an image to upload
			if (isset($_FILES['company_logo'])) {
				$this->createCompanyLogo($_FILES,$id);
			}
			
			if (isset($_POST['images_id'])) { 
				$this->editCompanyImage($_FILES,$_POST['companies_bild_title'],$_POST['companies_bild_desc'],$_POST['companies_bild_rank'],$_POST['companies_bild_link'],$id);			
			}
			
			if (isset($_FILES['company_bild_new'])) {
				$this->createCompanyImage($_FILES,$_POST['companies_bild_title_new'],$_POST['companies_bild_desc_new'],$_POST['companies_bild_rank_new'],$_POST['companies_bild_link_new'],$id);			
			}
			else {
				for( $i=0 ; $i < sizeOf($files["company_bild"]["name"]) ; $i++ ) {
					mysql_query("INSERT INTO arbetsgivare_companies_images_new (company_id,title,description,`order`,link) VALUES (
					'".$id."',
					'".$_POST['companies_bild_title'][$i]."',
					'".$_POST['companies_bild_desc'][$i]."',
					'".$_POST['companies_bild_rank'][$i]."',
					'".$_POST['companies_bild_link'][$i]."'");
				$i++;
				}
			}

			// now lets redirect if there is a redirect
			if(!empty($attr['success_redirect'])) {
				header("Location: {$attr['success_redirect']}".$company_id);
				exit;
			}
		}
	}
	
	private function createCompanyBanner($files, $company_id)
	{
		if(isset($_FILES["company_banner_flash"]["name"])) {
			@$allowedExts = array("swf");
			@$extension = end(explode(".", $_FILES["company_banner_flash"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "company_banner_flash_".$company_id;

				if(file_exists(INTERNAL_PATH."/public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH."/public/images/advertisement/". $newName . $extension);
				}

				$newFileBig = $newName .".". $extension;

				// Save the flash
				if(move_uploaded_file($_FILES["company_banner_flash"]["tmp_name"], INTERNAL_PATH."/public/images/advertisement/". $newFileBig)) {
					mysql_query("UPDATE arbetsgivare_companies SET banner_flash = '{$newFileBig}' WHERE id = '{$company_id}'");
				}
			}
		}
		
		if(isset($_FILES["company_banner_fallback"]["name"])) {
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["company_banner_fallback"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "company_banner_fallback_".$company_id;

				if(file_exists(INTERNAL_PATH."/public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH."/public/images/advertisement/". $newName . $extension);
				}

				$newFileBig = $newName .".". $extension;

				// Save the banner image
				if(move_uploaded_file($_FILES["company_banner_fallback"]["tmp_name"], INTERNAL_PATH."/public/images/advertisement/". $newFileBig)) {
					image::resize(INTERNAL_PATH."/public/images/advertisement/". $newFileBig,
										INTERNAL_PATH."/public/images/advertisement/". $newFileBig,
										980,
										120);
										
					mysql_query("UPDATE arbetsgivare_companies SET banner_fallback = '{$newFileBig}' WHERE id = '{$company_id}'");
				}

				
			}
		}
	}
	
	private function createCompanyImage($files, $titles, $descriptions, $ranks, $links, $company_id) {
		@$allowedExts = array("jpg", "jpeg", "gif", "png");
	
        for( $i=0 ; $i < sizeOf($files["company_bild_new"]["name"]) ; $i++ ) {
		
			@$extension = end(explode(".", $files["company_bild_new"]["name"][$i]));

            if(in_array($extension, $allowedExts)) {
				
               //get next incremental number
              $next_rnum = mysql_fetch_array(mysql_query("SELECT `order` FROM arbetsgivare_companies_images_new WHERE active = '1' AND company_id = '{$company_id}' ORDER BY `order` DESC LIMIT 0,1"));

              $next_rnum_value = intval($next_rnum['order']) + 1;
			
              $newNameBig = "product_".$company_id."_".$next_rnum_value."_big";
              $newNameThumb = "product_".$company_id."_".$next_rnum_value."_thumb";              

              $newFileBig = $newNameBig .".". $extension;
              $newFileThumb = $newNameThumb .".". $extension;
			  
			  if(file_exists(INTERNAL_PATH."/public/images/products/". $newFileBig)) {
                    unlink(INTERNAL_PATH."/public/images/products/". $newFileBig);
              }
			  
			  if(file_exists(INTERNAL_PATH."/public/images/products/". $newFileThumb)) {
                    unlink(INTERNAL_PATH."/public/images/products/". $newFileThumb);
              }

              if(move_uploaded_file($files["company_bild_new"]["tmp_name"][$i], INTERNAL_PATH."/public/images/products/". $newFileBig)) {

				image::resize(INTERNAL_PATH."/public/images/products/". $newFileBig,
									INTERNAL_PATH."/public/images/products/". $newFileBig,
									471,
									360);

				// Create the thumb
				image::resize(INTERNAL_PATH."/public/images/products/". $newFileBig,
							INTERNAL_PATH."/public/images/products/". $newFileThumb,
							150,
							100);

					mysql_query("INSERT INTO arbetsgivare_companies_images_new SET 
									company_id = '{$company_id}', 
									title = '". $titles[$i] ."', 
									description = '". $descriptions[$i] ."', 
									`order` = '". $ranks[$i] ."', 
									link = '". $links[$i] ."', 
									image_thumb = '{$newFileThumb}', 
									image_big = '{$newFileBig}', 
									added = '". time() ."' 
								") or die(mysql_error());
              }
            }
        }
	}
	
	private function editCompanyImage($files, $titles, $descriptions, $ranks, $links, $company_id) {
	
		@$allowedExts = array("jpg", "jpeg", "gif", "png");
		
	
        for( $i=0 ; $i < sizeOf($_POST["images_id"]) ; $i++ ) {
		
			@$extension = end(explode(".", $files["company_bild_{$_POST["images_id"][$i]}"]["name"]));
				
            if(isset($files["company_bild_{$_POST["images_id"][$i]}"]["name"]) && in_array($extension, $allowedExts)) {

				//get next incremental number
				$next_rnum = mysql_fetch_array(mysql_query("SELECT `order`,image_thumb,image_big FROM arbetsgivare_companies_images_new WHERE id = '". $_POST["images_id"][$i] ."'"));

				$next_rnum_value = intval($next_rnum['order']);

				$newFileBig = $next_rnum['image_thumb'];
				$newFileThumb = $next_rnum['image_big'];

				if(file_exists(INTERNAL_PATH."/public/images/products/". $newFileBig)) {
					unlink(INTERNAL_PATH."/public/images/products/". $newFileBig);
				}

				if(file_exists(INTERNAL_PATH."/public/images/products/". $newFileThumb)) {
					unlink(INTERNAL_PATH."/public/images/products/". $newFileThumb);
				}

				if(move_uploaded_file($files["company_bild_{$_POST["images_id"][$i]}"]["tmp_name"], INTERNAL_PATH."/public/images/products/". $newFileBig)) {
					image::resize(INTERNAL_PATH."/public/images/products/". $newFileBig,
										INTERNAL_PATH."/public/images/products/". $newFileBig,
										471,
										360);

					// Create the thumb
					image::resize(INTERNAL_PATH."/public/images/products/". $newFileBig,
								INTERNAL_PATH."/public/images/products/". $newFileThumb,
								150,
								100);
				}
            }
			
			mysql_query("UPDATE arbetsgivare_companies_images_new SET 
							title = '". $titles[$i] ."', 
							description = '". $descriptions[$i] ."', 
							`order` = '". $ranks[$i] ."', 
							link = '". $links[$i] ."' 
							WHERE id = '". $_POST['images_id'][$i] ."'
						") or die(mysql_error());
		}
	}
	
	private function createCompanyLogo($files, $company_id)
	{
		if (isset($_FILES["company_logo"]["name"])){
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["company_logo"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "company_".$company_id;

				if(file_exists(INTERNAL_PATH."/public/images/companies/". $newName . $extension)) {
					unlink(INTERNAL_PATH."/public/images/companies/". $newName . $extension);
				}

				$newFileBig = $newName ."_big.". $extension;
				$newFileThumb = $newName ."_thumb.". $extension;

				// Create the big image
				if(move_uploaded_file($_FILES["company_logo"]["tmp_name"], INTERNAL_PATH."/public/images/companies/". $newFileBig)) {
					image::resize(INTERNAL_PATH."/public/images/companies/". $newFileBig,
										INTERNAL_PATH."/public/images/companies/". $newFileBig,
										215,
										145);
										
					image::resize(INTERNAL_PATH."/public/images/companies/". $newFileBig,
										INTERNAL_PATH."/public/images/companies/". $newFileThumb,
										150,
										100);
										
					mysql_query("UPDATE arbetsgivare_companies SET image_big = '{$newFileBig}', image_thumb = '{$newFileThumb}' WHERE id = '{$company_id}'");
				}

				
			}
		}
	}
	
	public function parser_decoded_tags($tag, $attr) {
		return implode("\r\n",explode("|",$this->item['tags']));
	}
	
	public function parser_owner_name($tag, $attr) {
		$foretag = mysql_fetch_array(mysql_query("SELECT owner FROM arbetsgivare_companies WHERE id = '{$attr['id']}'"));
		$owner = mysql_fetch_array(mysql_query("SELECT first_name,last_name FROM accounts WHERE id = '{$foretag['owner']}'"));
		return "{$owner['first_name']} {$owner['last_name']}";
	}
	
	public function parser_is_pb($tag, $attr) {
		$foretag = mysql_fetch_array(mysql_query("SELECT access_platsbanken FROM arbetsgivare_companies WHERE id = '{$attr['id']}'"));
		if($foretag['access_platsbanken'] == "true" && $attr['status'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($foretag['access_platsbanken'] == "false" && $attr['status'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_is_bg($tag, $attr) {
		$foretag = mysql_fetch_array(mysql_query("SELECT access_branschguiden FROM arbetsgivare_companies WHERE id = '{$attr['id']}'"));
		if($foretag['access_branschguiden'] == "true" && $attr['status'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($foretag['access_branschguiden'] == "false" && $attr['status'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_div_class($tag, $attr) {
		return $this->div_class;
	}
	
	public function parser_is_parent($tag, $attr) {
		if($this->is_parent && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->is_parent && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_in_category($tag, $attr) {
		if(empty($this->in_categories)) {
			@$check = mysql_fetch_array(mysql_query("SELECT categories FROM arbetsgivare_companies WHERE id = '{$attr['company_id']}'"));
			$cats = explode("]",str_replace("[","",$check['categories']));
			foreach($cats as $cat) {
				$this->in_categories[] = $cat;
			}
		}
		
		if(in_array($attr['id'],$this->in_categories) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!in_array($attr['id'],$this->in_categories) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_padding($tag, $attr) {
		return $attr['padding'] * $attr['pixels'];
	}
	
	public function calculate_padding($id) {
		@$category = mysql_fetch_array(mysql_query("SELECT parent FROM branschguiden_categories WHERE id = '{$id}'"));

		$count = 0;
		while(!$parent_found) {
			if($category['parent'] != 0) {
				$count++;
				@$category = mysql_fetch_array(mysql_query("SELECT parent FROM branschguiden_categories WHERE id = '{$category['parent']}'"));
			}
			else {
				$parent_found = true;
			}
		}

	return $count;
	}
	
	public function sub_categories($id) {
		@$childs = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$id}' ORDER BY name ASC");
		if(mysql_num_rows($childs) > 0) {
			while($child = mysql_fetch_array($childs)) {
				$child['padding'] = $this->calculate_padding($child['id']);
				$this->all_cats[] = $child;
				$this->sub_categories($child['id']);
			}
		}
	}
	
	public function parser_all_categories($tag, $attr) {
		$html = "";
		
		@$parents = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = 0 ORDER BY name ASC");
		
		while($parent = mysql_fetch_array($parents)) {
			$this->all_categories[] = $parent;
		}
		
		foreach($this->all_categories as $parent) {
			@$childs = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$parent['id']}' ORDER BY name ASC");
			$this->all_cats[] = $parent;
			while($child = mysql_fetch_array($childs)) {
				$child['padding'] = $this->calculate_padding($child['id']);
				$this->all_cats[] = $child;
				$this->sub_categories($child['id']);
			}
		}
		
		foreach($this->all_cats as $category) {
			$this->div_class = ($category['parent'] == 0) ? "all_cats_parent" : "all_cats_child";
			$this->is_parent = ($category['parent'] == 0) ? true : false;
			$this->subitem = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_product_position($tag, $attr) {
	
		if($attr['position'] == "not_first") {
			@$first = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_products WHERE company_id = '{$attr['company_id']}' ORDER BY position ASC LIMIT 0,1"));
			if($attr['id'] != $first['id']) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if($attr['position'] == "not_last") {
			@$last = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_products WHERE company_id = '{$attr['company_id']}' ORDER BY position DESC LIMIT 0,1"));
			if($attr['id'] != $last['id']) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}

	public function parser_products($tag, $attr) {
		$html = "";
		
		@$id = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
		@$products = mysql_query("SELECT * FROM arbetsgivare_products WHERE company_id = '{$id}' ORDER BY position ASC");

		while($product = mysql_fetch_array($products)) {
			$this->item = $product;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function update_companies($id,$action,$account=0) {
		if($account > 0) {
		
			// check if there is an arbetsgivare_accounts row for the user
			@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_accounts WHERE account_id = '{$account}'"));
			if($check['id'] <= 0) {
				// There is none, lets create one
				mysql_query("INSERT INTO arbetsgivare_accounts SET account_id = '{$account}'");
			}
		
			$get = mysql_fetch_array(mysql_query("SELECT companies FROM arbetsgivare_accounts WHERE account_id = '{$account}'"));
			if($action == "add") {
				$companies = explode(",",$get['companies']);
				if(!in_array($id,$companies)) {
					$companies[] = $id;
				}
				foreach($companies as $company) {
					if(!empty($company)) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '{$account}'");
			}
			else if($action == "remove") {
				$companies = explode(",",$get['companies']);
				foreach($companies as $company) {
					if($company != $id) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '{$account}'");
			}
		}
	}
	
	public function parser_accounts($tag, $attr) {
		$html = "";
		@$accounts = mysql_query("SELECT id,email,first_name,last_name FROM accounts WHERE groups < 3 ORDER BY first_name ASC");
		while($account = mysql_fetch_array($accounts)) {
			$this->subitem = $account;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_fix_url($tag, $attr) {
		if (isset($attr['url']) && !empty($attr['url'])) {
			return (strstr("http://",$attr['url'])) ? $attr['url'] : "http://".$attr['url'] ;
		}
	}

	public function parser_get_requests($tag, $attr) {
		if(!empty($this->item['id'])) {
			$requests = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total FROM arbetsgivare_requests WHERE company_id = '{$this->item['id']}'"));
			return $requests['total'];
		}
		return 0;
	}

    public function parser_get_news($tag, $attr) {
		if(!empty($this->item['id'])) {
			$requests = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total FROM arbetsgivare_branschnyheter WHERE company_id = '{$this->item['id']}'"));
			return $requests['total'];
		}
		return 0;
	}
	
	public function parser_list($tag, $attr) {
		$html = "";

        $sql = "SELECT c.*,DATE_FORMAT(c.date_created,'%d-%m-%Y') as date_created_fmt,concat(a.first_name,' ',a.last_name) as contact_person
                FROM arbetsgivare_companies c
                LEFT JOIN accounts a on a.id=c.owner WHERE 1 ORDER BY c.name ASC";

        //search filter
		if(isset($_POST['search_foretag']) && !empty($_POST['search_foretag'])) {
			$sql .= " AND name LIKE '%{$_POST['search_foretag']}%' ";
		}

        //sort order
        if (isset($_POST['sort_order'])) $this->sort_order = $_POST['sort_order'];
        if (isset($_POST['sort_column'])) $this->sort_column = $_POST['sort_column'];

        if ( isset($_POST['sort_order']) && isset($_POST['sort_column']) && $_POST['sort_column'] != "" && $_POST['sort_order'] != "")  {
           $sql .= "ORDER BY " . $_POST['sort_column'] . " " . $_POST['sort_order'];
        }

        //execute query
        @$companies = mysql_query($sql);

		while($company = mysql_fetch_array($companies)) {
			$this->item = $company;

			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}

	return $html;
	}

	public function url_friendly($string) {
		$url = str_replace("ö","o",strtolower($string));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("ä","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			$get = $this->subitem[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}

	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}

    public function parser_get_current_sort_order($tag, $attr){
           return $this->sort_order;
    }

    public function parser_get_current_sort_column($tag, $attr){
           return $this->sort_column;
    }
	
	public function parser_company_telephones($tag, $attr) {
		$html = "";
		
		$sql = "SELECT telephone
                FROM arbetsgivare_companies_telephone 
                WHERE company_id='".$attr['company_id']."' and active='1'";

        //execute query
        $company_telephones = mysql_query($sql);

		while($company_telephone = mysql_fetch_array($company_telephones)) {
			$this->subitem = $company_telephone;

			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
		return $html;
	}
	
	public function parser_company_mobiles($tag, $attr) {
		$html = "";
		
		$sql = "SELECT mobile
                FROM arbetsgivare_companies_mobile 
                WHERE company_id='".$attr['company_id']."' and active='1';";

        //execute query
        $company_mobiles = mysql_query($sql);

		while($company_mobile = mysql_fetch_array($company_mobiles)) {
			$this->subitem = $company_mobile;

			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
		return $html;
	}
	
	public function parser_company_websites($tag, $attr) {
		$html = "";
		
		$sql = "SELECT website
                FROM arbetsgivare_companies_website
                WHERE company_id='".$attr['company_id']."' and active='1';";

        //execute query
        $company_websites = mysql_query($sql);

		while($company_website = mysql_fetch_array($company_websites)) {
			$this->subitem = $company_website;

			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
		return $html;
	}
	
	public function parser_company_addresses($tag, $attr) {
		$html = "";
		
		$sql = "SELECT section,street,zip,city
                FROM arbetsgivare_companies_address
                WHERE company_id='".$attr['company_id']."' and active='1';";

        //execute query
        $company_addresses = mysql_query($sql);

		while($company_address = mysql_fetch_array($company_addresses)) {
			$this->subitem = $company_address;

			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
		return $html;
	}

	public function parser_has_image($tag, $attr) {  
        if($this->has_images && $attr['state'] == "true" ) {
       		return $GLOBALS['parser']->parse($tag['innerhtml']);
		} else if ( $attr['state'] == "false" && !$this->has_images) {
           return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_has_logo($tag, $attr) {
        if(!empty($this->item['image_big']) && !empty($this->item['image_thumb']) && $attr['state'] == "true" ) {
			$this->image = $this->item['image_thumb'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		} else if ( $attr['state'] == "false" && empty($this->item['image_big']) && empty($this->item['image_thumb']))  {
           return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_get_company_images($tag, $attr){
        if(isset($attr['id'])) {
            $limit = (isset($attr['limit'])) ? "LIMIT ".$attr['limit'] : '';
            $min   = (isset($attr['min'])) ? "AND `order` >= ".$attr['min'] : '';
			
		//	exit("SELECT * FROM arbetsgivare_companies_images_new WHERE company_id='{$attr['id']}' {$min} AND active='1' ORDER BY `order` {$limit}");
			$images = mysql_query("SELECT * FROM arbetsgivare_companies_images_new WHERE company_id='{$attr['id']}' {$min} AND active='1' ORDER BY `order` ASC {$limit}") or die(mysql_error());
            while($image = mysql_fetch_array($images)) {
    		    $this->has_images = true;
    			$this->subitem = $image;
    			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
    		}
            return $html;
        }
    }
}

?>