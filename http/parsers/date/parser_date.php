<?php

class parser_date {

    public function parser_unix_to_format($tag, $attr){
       if (isset($attr['unix']) && $attr['unix'] > 0){
           return date( $attr['format'], $attr['unix']);
       }
	   else {
		   return "";
	   }
    }
}
?>