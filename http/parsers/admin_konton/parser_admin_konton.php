<?php

class parser_admin_konton {

	private $item = array();
	private $subitem = array();
	private $category = "";
	private $message = "";

	function __construct() {
		@$id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
		if($id > 0) {
			$this->item = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE id = '{$id}'"));
		}
		
		
		if($GLOBALS['parser']->parse("{page:rurl index='2'/}") == "radera") {
			$id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
			mysql_query("DELETE FROM accounts WHERE id = '{$id}'");
			
			header("Location: /admin/konton");
			exit;
		}
		else if($GLOBALS['parser']->parse("{page:rurl index='2'/}") == "nytt_losenord") {
			$this->parser_send_new_password("","");
		}
		
		// edit an account if requested
		if(isset($_POST['konton_edit'])) {
			mysql_query("UPDATE accounts SET 
							email = '{$_POST['konton_email']}', 
							first_name = '{$_POST['konton_first_name']}', 
							last_name = '{$_POST['konton_last_name']}', 
							groups = '{$_POST['konton_groups']}' 
							WHERE id = '{$id}'");
			$this->item = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE id = '{$id}'"));
		}
		else if(isset($_POST['konton_create'])) {
			// check if the email already exist
			@$check = mysql_fetch_array(mysql_query("SELECT id FROM accounts WHERE email = '{$_POST['konton_email']}'"));
			if($check['id'] > 0) {
				$this->message = "- Det finns redan ett konto med den angivna emailen.";
			}
			else {
				mysql_query("INSERT INTO accounts SET 
								email = '{$_POST['konton_email']}', 
								first_name = '{$_POST['konton_first_name']}', 
								last_name = '{$_POST['konton_last_name']}', 
								status = 'active', 
								groups = '{$_POST['konton_groups']}'");
				
				// send an email which lets the user to set their new password
				// make sure the hash is unique
				$unique = false;
				while(!$unique) {
					$new_password_hash = md5("HRtorget1304 PASSWORD {$id} {$email}". rand(0,999999));
					@$check = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE new_password_hash = '{$new_password_hash}'"));
					if(empty($check['id'])) {
						$unique = true;
					}
				}
			
				// get the mail template, which is ID 5
				@$mail_template = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 5"));
				$tags['link'] = SERVER_NAME ."/konto/nytt-losenord/$new_password_hash";
				$template['mail_title'] = $mail_template['mail_title'];
				$template['mail_text'] = $mail_template['mail_text'];
				
				// replace the tags
				// [link] = the link to the new password url
				$mail = $GLOBALS['mail']->tags($tags,$template);
				
				mysql_query("UPDATE accounts SET new_password_hash = '{$new_password_hash}' WHERE email = '{$_POST['konton_email']}'");
				$GLOBALS['mail']->send($_POST['konton_email'],$mail['mail_title'],$mail['mail_text']);
				
				header("Location: /admin/konton");
				exit;
			}
		}
	}
	
	public function parser_send_new_password($tag, $attr) {
		@$id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
		if($id > 0) {
			@$check = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE id = '{$id}'"));
			$email = $check['email'];
			
			$unique = false;
			while(!$unique) {
				$new_password_hash = md5("HRtorget1304 PASSWORD {$id} {$email}". rand(0,999999));
				@$check = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE new_password_hash = '{$new_password_hash}'"));
				if(empty($check['id'])) {
					$unique = true;
				}
			}
		
			// get the mail template, which is ID 5
			@$mail_template = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 2"));
			$tags['link'] = SERVER_NAME ."/konto/nytt-losenord/$new_password_hash";
			$template['mail_title'] = $mail_template['mail_title'];
			$template['mail_text'] = $mail_template['mail_text'];
			
			// replace the tags
			// [link] = the link to the new password url
			$mail = $GLOBALS['mail']->tags($tags,$template);
			
			mysql_query("UPDATE accounts SET new_password_hash = '{$new_password_hash}' WHERE email = '{$email}'");
			$GLOBALS['mail']->send($email,$mail['mail_title'],$mail['mail_text']);
		}
	}
	
	public function update_companies($id,$action,$account=0) {
		if($account > 0) {
		
			// check if there is an arbetsgivare_accounts row for the user
			@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_accounts WHERE account_id = '{$account}'"));
			if($check['id'] <= 0) {
				// There is none, lets create one
				mysql_query("INSERT INTO arbetsgivare_accounts SET account_id = '{$account}'");
			}
		
			$get = mysql_fetch_array(mysql_query("SELECT companies FROM arbetsgivare_accounts WHERE account_id = '{$account}'"));
			if($action == "add") {
				$companies = explode(",",$get['companies']);
				if(!in_array($id,$companies)) {
					$companies[] = $id;
				}
				foreach($companies as $company) {
					if(!empty($company)) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '{$account}'");
			}
			else if($action == "remove") {
				$companies = explode(",",$get['companies']);
				foreach($companies as $company) {
					if($company != $id) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '{$account}'");
			}
		}
	}
	
	public function parser_message($tag, $attr) {
		if(isset($attr['check']) && !empty($this->message)) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $this->message;
		}
	}
	
	public function parser_post($tag, $attr) {
		return $_POST[$attr['get']];
	}
	
	public function parser_image($tag, $attr) {
		return $this->image;
	}
	
	public function parser_got_image($tag, $attr) {
		@$check = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_bild WHERE account_id = '{$this->item['id']}'"));
		if(isset($check['step']) && $check['step'] == 3) {
			$this->image = $check['name'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_group($tag, $attr) {
		switch($this->item['groups']) {
			case 1:
				return "Administratör";
			break;
			case 3:
				return "Arbetsgivare";
			break;
			default:
			case 0:
			case 4:
				return "Medlem";
			break;
		}
	}
	
	public function parser_accounts($tag, $attr) {
		$html = "";
		@$accounts = mysql_query("SELECT id,email,first_name,last_name FROM accounts WHERE groups < 3 ORDER BY first_name ASC");
		while($account = mysql_fetch_array($accounts)) {
			$this->subitem = $account;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_has_image($tag, $attr) {
		if(!empty($this->item['image_big'])) {
			$this->image = $this->item['image_big'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_list($tag, $attr) {
		$html = "";
		if(isset($_POST['search_konton']) && !empty($_POST['search_konton'])) {
			@$accounts = mysql_query("SELECT * FROM accounts WHERE 
				first_name LIKE '%{$_POST['search_konton']}%' 
				OR last_name LIKE '%{$_POST['search_konton']}%' 
				OR email LIKE '%{$_POST['search_konton']}%' 
				ORDER BY first_name ASC");
		}
		else {
			@$accounts = mysql_query("SELECT * FROM accounts ORDER BY first_name ASC");
		}
		while($account = mysql_fetch_array($accounts)) {
			$this->item = $account;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function url_friendly($string) {
		$url = str_replace("ö","o",strtolower($string));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("ä","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->subitem[$attr['get']]) > $attr['cut']) {
				return substr($this->subitem[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->subitem[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

}

?>