<?php

class parser_account {

	public $user = array();
	public $by_id = array();

	function __construct() {
		if(empty($this->user)) {
			$this->user = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE id = '". account::user('id') ."'"));
		}
	}

	public function parser_is_admin($tag, $attr) {
		if($this->user['groups'] == 1) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			@$ip = $_SERVER['SERVER_ADDR'];
			@$date = time();
			mysql_query("INSERT INTO admin_logs SET ip = '{$ip}', date = '{$date}', server_vars = '". serialize($_SERVER) ."'");
			return "";
		}
	}

	public function parser_by_id($tag, $attr) {
		$this->by_id = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE id = '{$attr['id']}'"));
		return $this->by_id[$attr['get']];
	}

	public function parser_user($tag, $attr) {
		return $this->user[$attr['get']];
	}

	public function parser_group($tag, $attr) {
		$groups = explode(",",$this->user['groups']);
		if(in_array($attr['id'],$groups)) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

}

?>