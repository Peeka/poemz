<?php

class parser_startpage {

	public $item = array();
	public $subitem = array();
	public $previous = array();
	public $next = array();
	public $increment = 0;

	function __construct() {
		@$id = $GLOBALS['parser']->parse("{page:rurl index='1'/}");
		if(!empty($id)) {
			@$this->item = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '{$id}'"));
			// Load the previous news article
			@$nyheter = mysql_query("SELECT id,url_friendly FROM arbetsgivare_branschnyheter ORDER BY added ASC");
			
			$next = false;
			$ended = false;
			$previous_id = "";
			while($nyhet = mysql_fetch_array($nyheter)) {
				if(!$ended) {
					if($nyhet['id'] == $id && !$next) {
						if(!empty($previous_id)) {
							$this->previous['id'] = $previous_id['id'];
							$this->previous['url_friendly'] = $previous_id['url_friendly'];
						}
						$next = true;
					}
					else if($next) {
						$this->next['id'] = $nyhet['id'];
						$this->next['url_friendly'] = $nyhet['url_friendly'];
						$ended = true;
					}
					
					$previous_id = $nyhet;
				}
			}
		}
	}
	
	public function parser_get_product_image($tag, $attr) {
		$image = "";
		$image = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies_images WHERE company_id = '{$attr['company_id']}' AND rnum = '{$attr['index']}' AND active = '1'"));
		return $image[$attr['return']];
	}
	
	public function parser_products($tag, $attr) {
		$html = "";
		
		@$products = mysql_query("SELECT * FROM arbetsgivare_companies_images_new WHERE company_id = '{$attr['id']}' AND active = '1' ORDER BY `order` ASC");

		if(isset($attr['not_empty']) && mysql_num_rows($products) > 0) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			while($product = mysql_fetch_array($products)) {
				$this->subitem = $product;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			return $html;
		}
	}
	
	public function parser_total_items($tag, $attr) {
		$total = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS total_items FROM arbetsgivare_branschnyheter"));
		return $total['total_items'];
	}
	
	public function parser_get_image($tag, $attr) {
		$image = "";
		$image = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter_images_2 WHERE branschnyheter_id = '{$attr['branschnyheter_id']}' AND active = '1'"));
		return $image[$attr['return']];
	}
	
	public function parser_branschnyheter($tag, $attr) {
		$html = "";
		$where = " WHERE status = 'accepted'";
		if(isset($attr['exclude'])) {
			$where .= " AND id != '{$attr['exclude']}'";
		}
		@$id = (!empty($attr['id'])) ? "{$where} AND company_id = {$attr['id']}" : $where;
		
		if(isset($attr['limit'])) {
			$limit = " LIMIT 0,{$attr['limit']}";
		}
		else {
			$limit = " LIMIT ".$GLOBALS['pager']->limit();
		}
		
		@$branschnyheter = mysql_query("SELECT * FROM arbetsgivare_branschnyheter{$id} ORDER BY added DESC{$limit}");
		if(isset($attr['not_empty']) && mysql_num_rows($branschnyheter) > 0) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			while($branschnyhet = mysql_fetch_array($branschnyheter)) {
				$this->increment++;
				$this->item = $branschnyhet;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			return $html;
		}
	}
	
	public function parser_increment($tag, $attr) {
		return $this->increment;
	}
	
	public function parser_has_image($tag, $attr) {
		if(!empty($this->item[$attr['image']]) && isset($attr['check'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $this->item[$attr['image']];
		}
	}
	
	public function parser_next($tag, $attr) {
		if(isset($attr['check']) && !empty($this->next)) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $this->next[$attr['get']];
		}
	}
	
	public function parser_previous($tag, $attr) {
		if(isset($attr['check']) && !empty($this->previous)) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $this->previous[$attr['get']];
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			else {
				$get = $this->item[$attr['get']];
			}
			
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			
			// Make links open popup
			if(isset($attr['popup'])) {
				$get = str_replace('<a','<a target="_blank"',$get);
			}
			
			// New line to <br />
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
			
			// Cut the line and/or add something at the end
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				return $GLOBALS['text']->cut($get,$attr['cut']).$attr['add'];
			}
			else {
				return $get;
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_subitem($tag, $attr) {
		$get = $this->subitem[$attr['get']];
		if(!empty($get)) {
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = $GLOBALS['text']->cut($get,$attr['cut']).$attr['add'];
			}
		}
	return $get;
	}

}

?>