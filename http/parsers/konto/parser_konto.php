<?php

class parser_konto {

	public $failed = false;
	public $profile_mandatory_fields = array("first_name","last_name","street_name","zip_code","birth_year","birth_month","birth_day","gender");
	public $is_empty = array();
	public $profile = array();
	public $item = array();
	public $progress = array();
	public $errors = array();
	public $error_messages = array();
	public $registration_success = false;
	public $resend_success = false;
	public $new_password_success = false;
	public $create_password_success = false;
	public $create_password_hash = false;
	public $andra_losenord_success = false;

	function __construct() {
		// first we need to get the profile
		if(account::user('id') > 0) {
			$this->parser_get_profile("",array("get"=>"personuppgifter"));
			$this->parser_get_profile("",array("get"=>"utbildningar"));
			$this->parser_get_profile("",array("get"=>"anstallningar"));
			$this->parser_get_profile("",array("get"=>"bild"));
			
			if($GLOBALS['parser']->parse("{page:rurl index='4'/}") == "radera") {
				if($GLOBALS['parser']->parse("{page:rurl index='3'/}") > 0) {
					// utbildningar
					if($GLOBALS['parser']->parse("{page:rurl index='2'/}") == "utbildningar") {
						mysql_query("DELETE FROM arbetssokande_utbildningar WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='3'/}") ."' AND account_id = '". account::user('id') ."'");
						header("Location: /konto/mitt-cv/utbildningar");
						exit;
					} // anstallningar
					else if($GLOBALS['parser']->parse("{page:rurl index='2'/}") == "anstallningar") {
						mysql_query("DELETE FROM arbetssokande_anstallningar WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='3'/}") ."' AND account_id = '". account::user('id') ."'");
						header("Location: /konto/mitt-cv/anstallningar");
						exit;
					}
				}
			}
		}
	}
	
	public function parser_andra_losenord($tag, $attr) {
		// check if we are logged in
		@$acc_id = account::user("id");
		if(isset($acc_id) && $acc_id > 0) {
			if(isset($_POST['andra_password']) && !empty($_POST['andra_password']) && isset($_POST['andra_password_check']) && !empty($_POST['andra_password_check']) && $_POST['andra_password'] == $_POST['andra_password_check']) {
				$new_pass = md5($_POST['andra_password']);
				
				mysql_query("UPDATE accounts SET password = '{$new_pass}' WHERE id = '{$acc_id}'");
				$this->andra_losenord_success = true;
			}
		}
	}
	
	public function parser_andra_losenord_success($tag, $attr) {
		if($this->andra_losenord_success && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->andra_losenord_success && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_create_password($tag, $attr) {
		@$new_password_hash = $GLOBALS['parser']->parse('{page:rurl index="2"/}');
		// check if the change password hash exists
		@$acc = mysql_fetch_array(mysql_query("SELECT id FROM accounts WHERE new_password_hash = '{$new_password_hash}'"));
		if($acc['id'] > 0) {
			$this->create_password_hash = true;
			if(isset($_POST['create_password']) && !empty($_POST['create_password']) && isset($_POST['create_password_check']) && !empty($_POST['create_password_check']) && $_POST['create_password'] == $_POST['create_password_check']) {
				$new_pass = md5($_POST['create_password']);
				
				mysql_query("UPDATE accounts SET password = '{$new_pass}', new_password_hash = '' WHERE id = '{$acc['id']}'");
				$this->create_password_success = true;
			}
		}
	}
	
	public function parser_create_password_success($tag, $attr) {
		if($this->create_password_success && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->create_password_success && $attr['state'] == "false") {
			if(!$this->create_password_hash && $attr['hash'] == "false") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			else if($this->create_password_hash && $attr['hash'] == "true") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}
	
	public function parser_new_password($tag, $attr) {
		if(isset($_POST['new_password_email']) && !empty($_POST['new_password_email'])) {
			// check if the email exists, and if so get the activation hash
			@$acc = mysql_fetch_array(mysql_query("SELECT id,email FROM accounts WHERE email = '{$_POST['new_password_email']}'"));
			if($acc['id'] > 0) {
			
				// make sure the hash is unique
				$unique = false;
				while(!$unique) {
					$new_password_hash = md5("HRtorget1304 PASSWORD {$id} {$email}". rand(0,999999));
					@$check = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE new_password_hash = '{$new_password_hash}'"));
					if(empty($check['id'])) {
						$unique = true;
					}
				}
			
				// get the mail template, which is ID 2
				@$mail_template = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 2"));
				$tags['link'] = "http://www.hrtorget.se/konto/nytt-losenord/$new_password_hash";
				$template['mail_title'] = $mail_template['mail_title'];
				$template['mail_text'] = $mail_template['mail_text'];
				
				// replace the tags
				// [link] = the link to the new password url
				$mail = $GLOBALS['mail']->tags($tags,$template);
				
				mysql_query("UPDATE accounts SET new_password_hash = '{$new_password_hash}' WHERE email = '{$_POST['new_password_email']}'");
				$GLOBALS['mail']->send($acc['email'],$mail['mail_title'],$mail['mail_text']);
				$this->new_password_success = true;
			}
		}
	}
	
	public function parser_resend_activation($tag, $attr) {
		if(isset($_POST['resend_email']) && !empty($_POST['resend_email'])) {
			// check if the email exists, and if so get the activation hash
			@$check = mysql_fetch_array(mysql_query("SELECT id,email,activation_hash FROM accounts WHERE email = '{$_POST['resend_email']}' AND status = 'activation'"));
			if($check['id'] > 0) {
				// get the mail template, which is ID 1
				$activation_hash = $check['activation_hash'];
				@$mail_template = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 1"));
				$tags['link'] = "http://www.hrtorget.se/konto/aktivera/$activation_hash";
				$template['mail_title'] = $mail_template['mail_title'];
				$template['mail_text'] = $mail_template['mail_text'];
				
				// replace the tags
				// [link] = the link to the account activation url
				$mail = $GLOBALS['mail']->tags($tags,$template);
				
				$GLOBALS['mail']->send($check['email'],$mail['mail_title'],$mail['mail_text']);
				$this->resend_success = true;
			}
		}
	}
	
	public function parser_new_password_success($tag, $attr) {
		if($this->new_password_success && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->new_password_success && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_resend_success($tag, $attr) {
		if($this->resend_success && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->resend_success && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_registration_success($tag, $attr) {
		if($this->registration_success && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->registration_success && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_activation_success($tag, $attr) {
		if($this->activation_success && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->activation_success && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_aktivera($tag, $attr) {
		@$activation_hash = $GLOBALS['parser']->parse('{page:rurl index="2"/}');
		
		if(!empty($activation_hash)) {
			// check if the activation hash exists
			@$check = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE activation_hash = '{$activation_hash}'"));
			if(!empty($check)) {
				mysql_query("UPDATE accounts SET status = 'active', activation_hash = '' WHERE activation_hash = '{$activation_hash}'");
				$this->activation_success = true;
			}
			else {
				$this->activation_success = false;
			}
		}
		else {
			$this->activation_success = false;
		}
	}
	
	public function parser_register_check($tag, $attr) {
		$first_name = $_POST['register_first_name'];
		$last_name = $_POST['register_last_name'];
		$email = $_POST['register_email'];
		$password = $_POST['register_password'];
		$password_retype = $_POST['register_password_retype'];
		
		// check if email exists
		@$check = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE email = '{$email}'"));
		if(empty($check)) {
			if(!empty($first_name) && !empty($last_name) && !empty($email) && !empty($password) && !empty($password_retype) && $password == $password_retype) {
				$activation_hash = md5("HRtorget1304 {$first_name} {$last_name} {$email}");
				
				// make sure the hash is unique
				$unique = false;
				while(!$unique) {
					$activation_hash = md5("HRtorget1304 {$first_name} {$last_name} {$email}". rand(0,999999));
					@$check = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE activation_hash = '{$activation_hash}'"));
					if(empty($check['id'])) {
						$unique = true;
					}
				}
				// get the mail template, which is ID 1
				@$mail_template = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 1"));
				$tags['link'] = "http://www.hrtorget.se/konto/aktivera/$activation_hash";
				$template['mail_title'] = $mail_template['mail_title'];
				$template['mail_text'] = $mail_template['mail_text'];
				
				// replace the tags
				// [link] = the link to the account activation url
				$mail = $GLOBALS['mail']->tags($tags,$template);
				
				mysql_query("INSERT INTO accounts SET email = '{$email}', password = '". md5($password) ."', first_name = '{$first_name}', last_name = '{$last_name}', activation_hash = '{$activation_hash}'");
				$GLOBALS['mail']->send($email,$mail['mail_title'],$mail['mail_text']);
				$this->registration_success = true;
			}
		}
	}
	
	public function parser_in_category($tag, $attr) {
		if(strstr($this->profile['personuppgifter']['categories'],"[{$attr['id']}]") && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!strstr($this->profile['personuppgifter']['categories'],"[{$attr['id']}]") && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_all_categories($tag, $attr) {
		$html = "";

		@$childs = mysql_query("SELECT * FROM arbetsgivare_job_categories ORDER BY name ASC");
		while($child = mysql_fetch_array($childs)) {
			$this->all_cats[] = $child;
		}
		
		foreach($this->all_cats as $category) {
			$this->item = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_errors($tag, $attr) {
		$html = "";
		
		if(@$attr['action'] == "set") {
			$this->error_messages[$attr['parent']][$attr['child']] = $attr['text'];
		}
		else if($attr['action'] == "list") {
			foreach($this->error_messages as $pval => $parent) {
				foreach($parent as $cval => $child) {
					if($this->errors[$pval][$cval]) {
						$this->item['text'] = $child;
						$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
					}
				}
			}
			return $html;
		}
		else if($attr['action'] == "check" && !empty($this->error_messages)) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_progress($tag, $attr) {
	
		if(empty($this->progress)) {
			// If finished = true
			$this->progress[1] = false;
			$this->progress[2] = false;
			$this->progress[3] = false;
			$this->progress[4] = false;
			$this->is_empty = "";
			
			// First we check so that all values in Personuppgifter has been filled out
			foreach($this->profile_mandatory_fields as $field) {
				if(empty($this->profile["personuppgifter"][$field])) {
					$this->is_empty[$field] = true;
				}
			}
			if(empty($this->is_empty)) {
				$this->progress[1] = true;
			}
			// Then check utbildningar
			if(!empty($this->profile['utbildningar'])) {
				$this->progress[2] = true;
			}
			// Check anstallningar
			if(!empty($this->profile['anstallningar'])) {
				$this->progress[3] = true;
			}
			// Finally, do we have a picture?
			if($this->profile['bild']['step'] == 3) {
				$this->progress[4] = true;
			}
		}
		
		$return = true;
		$step = 1;
		
		if(!empty($attr['redirect'])) {
			while($step <= 4) {
				if(!$this->progress[$step]) {
					header("Location: {$attr['prefix']}{$attr[$step]}");
					exit;
				}
			$step++;
			}
		}
		
		if(!empty($attr['step'])) {
			return ($this->progress[$attr['step']]) ? $GLOBALS['parser']->parse($attr['true']) : $GLOBALS['parser']->parse($attr['false']);
		}
	}
	
	public function parser_profile($tag, $attr) { #exit("{$attr['get']}-{$attr['index']}-{$attr['name']}");
		// Posted values
		if(isset($attr['posted_first']) && !empty($attr['posted_first']) && isset($_POST["{$attr['get']}_{$attr['name']}"]) && !empty($_POST["{$attr['get']}_{$attr['name']}"])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $_POST["{$attr['get']}_{$attr['name']}"]) {
					return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
				}
				else {
					return "";
				}
			}
			else {
				return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
			}
		} // Stored values
		else if(isset($this->profile[$attr['get']][$attr['name']]) && !empty($this->profile[$attr['get']][$attr['name']]) || 
		isset($attr['index']) && isset($this->profile[$attr['get']][$attr['index']][$attr['name']]) && !empty($this->profile[$attr['get']][$attr['index']][$attr['name']])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $this->profile[$attr['get']][$attr['name']] || isset($attr['index']) && $attr['value'] == $this->profile[$attr['get']][$attr['index']][$attr['name']]) {
					if(isset($attr['index'])) {
						return (isset($attr['return'])) ? $attr['return'] : $this->profile[$attr['get']][$attr['index']][$attr['name']];
					}
					else {
						return (isset($attr['return'])) ? $attr['return'] : $this->profile[$attr['get']][$attr['name']];
					}
				}
				else {
					return "";
				}
			}
			else {
				if(isset($attr['index'])) {
					return (isset($attr['return'])) ? $attr['return'] : $this->profile[$attr['get']][$attr['index']][$attr['name']];
				}
				else {
					return (isset($attr['return'])) ? $attr['return'] : $this->profile[$attr['get']][$attr['name']];
				}
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_get_profile($tag, $attr) {
		//get personuppgifter
		if($attr['get'] == "personuppgifter") {
			@$info = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_profil WHERE account_id = '". account::user('id') ."'"));
			@$this->profile['personuppgifter'] = $info;
			@$this->profile['personuppgifter']['first_name'] = account::user('first_name');
			@$this->profile['personuppgifter']['last_name'] = account::user('last_name');
		}
		else if($attr['get'] == "utbildningar") {
			@$info = mysql_query("SELECT * FROM arbetssokande_utbildningar WHERE account_id = '". account::user('id') ."'");
			while($utbildning = mysql_fetch_array($info)) {
				@$this->profile['utbildningar'][$utbildning['id']] = $utbildning;
			}
		}
		else if($attr['get'] == "anstallningar") {
			@$info = mysql_query("SELECT * FROM arbetssokande_anstallningar WHERE account_id = '". account::user('id') ."'");
			while($anstallning = mysql_fetch_array($info)) {
				@$this->profile['anstallningar'][$anstallning['id']] = $anstallning;
			}
		}
		else if($attr['get'] == "bild") {
			@$bild = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_bild WHERE account_id = '". account::user('id') ."'"));
			@$this->profile['bild'] = $bild;
		}
	}
	
	public function parser_utbildningar($tag, $attr) {
		$html = "";
		
		//get anställningar
		if(!empty($this->profile['utbildningar'])) {
			foreach($this->profile['utbildningar'] as $utbildningar) {
				$this->item = $utbildningar;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		
	return $html;
	}
	
	public function parser_anstallningar($tag, $attr) {
		$html = "";
		
		//get anställningar
		if(!empty($this->profile['anstallningar'])) {
			foreach($this->profile['anstallningar'] as $anstallning) {
				$this->item = $anstallning;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		
	return $html;
	}
	
	public function parser_profile_image($tag, $attr) {
		// check if our image status is 3 (complete), otherwise load the default user_no_image.jpg
		if($this->profile['bild']['step'] == 3) {
			if(isset($attr['id'])) {
				return "user_". $attr['id'] .".jpg";
			}
			else {
				return "user_". account::user("id") .".jpg";
			}
		}
		else if($this->profile['bild']['step'] == 2) {
			return $this->profile['bild']['name'];
		}
		else {
			return "user_no_image.jpg";
		}
	}
	
	public function parser_item($tag, $attr) {
		return @$this->item[$attr['get']];
	}
	
	public function parser_update_profile($tag, $attr) {
		$this->is_empty = false;
		
		// update the personuppgifter
		if(isset($_POST['personuppgifter_submit'])) {
			foreach($this->profile_mandatory_fields as $field) {
				if(empty($_POST["personuppgifter_{$field}"])) {
					$this->is_empty[$field] = true;
					echo "personuppgifter_{$field}";
				}
			}
			
			if(empty($this->is_empty)) {
				// save the information to the database
				@$check = mysql_fetch_array(mysql_query("SELECT account_id FROM arbetssokande_profil WHERE account_id = '". account::user('id') ."'"));
				
				$drivers_b_card 	= (isset($_POST['personuppgifter_drivers_b_card']) && !empty($_POST['personuppgifter_drivers_b_card'])) ? "true" : "false";
				$drivers_have_car 	= (isset($_POST['personuppgifter_drivers_have_car']) && !empty($_POST['personuppgifter_drivers_have_car'])) ? "true" : "false";
				
				// Fix the categories
				$categories = "";
				foreach($_POST['companies_categories'] as $category) {
					$categories .= "[{$category}]";
				}
				
				if(empty($check['account_id'])) {
					// we have no entry yet, so lets create one and populate it with the entered information
					
					mysql_query("INSERT INTO arbetssokande_profil SET 
								account_id = '". account::user('id') ."', 
								street_name = '{$_POST['personuppgifter_street_name']}', 
								zip_code = '{$_POST['personuppgifter_zip_code']}', 
								city = '{$_POST['personuppgifter_city']}', 
								county = '{$_POST['personuppgifter_county']}', 
								phone_number = '{$_POST['personuppgifter_phone_number']}', 
								cell_number = '{$_POST['personuppgifter_cell_number']}', 
								birth_year = '{$_POST['personuppgifter_birth_year']}', 
								birth_month = '{$_POST['personuppgifter_birth_month']}', 
								birth_day = '{$_POST['personuppgifter_birth_day']}', 
								drivers_b_card = '{$drivers_b_card}', 
								drivers_have_car = '{$drivers_have_car}', 
								gender = '{$_POST['personuppgifter_gender']}', 
								categories = '{$categories}', 
								personal_letter = '{$_POST['personuppgifter_personal_letter']}'");
								
				} // update the database
				else {
					
					mysql_query("UPDATE arbetssokande_profil SET 
								street_name = '{$_POST['personuppgifter_street_name']}', 
								zip_code = '{$_POST['personuppgifter_zip_code']}', 
								city = '{$_POST['personuppgifter_city']}', 
								county = '{$_POST['personuppgifter_county']}', 
								phone_number = '{$_POST['personuppgifter_phone_number']}', 
								cell_number = '{$_POST['personuppgifter_cell_number']}', 
								birth_year = '{$_POST['personuppgifter_birth_year']}', 
								birth_month = '{$_POST['personuppgifter_birth_month']}', 
								birth_day = '{$_POST['personuppgifter_birth_day']}', 
								drivers_b_card = '{$drivers_b_card}', 
								drivers_have_car = '{$drivers_have_car}', 
								gender = '{$_POST['personuppgifter_gender']}', 
								categories = '{$categories}', 
								personal_letter = '{$_POST['personuppgifter_personal_letter']}' 
								WHERE account_id = '". account::user('id') ."'");
				}
				
				// and update the name in the account
				mysql_query("UPDATE accounts SET 
							first_name = '{$_POST['personuppgifter_first_name']}', 
							last_name = '{$_POST['personuppgifter_last_name']}' 
							WHERE id = '". account::user('id') ."'") or die(mysql_error());
			
				// now lets redirect if there is a redirect
				if(!empty($attr['success_redirect'])) {
					header("Location: {$attr['success_redirect']}");
					exit;
				}
			}
			else {
				
			}
		}
		// update the personuppgifter
		else if(isset($_POST['utbildningar_submit'])) {
		
			// Check if the dates are correct
			$from = strtotime("1-{$_POST['utbildningar_from_month']}-{$_POST['utbildningar_from_year']}");
			$to = strtotime("1-{$_POST['utbildningar_to_month']}-{$_POST['utbildningar_to_year']}");
			
			if($from <= $to) {
				// save the information to the database
				if(isset($_POST['utbildningar_edit']) && !empty($_POST['utbildningar_edit'])) {
					mysql_query("UPDATE arbetssokande_utbildningar SET 
									school = '{$_POST['utbildningar_school']}', 
									program = '{$_POST['utbildningar_program']}', 
									education_level = '{$_POST['utbildningar_education_level']}', 
									from_year = '{$_POST['utbildningar_from_year']}', 
									from_month = '{$_POST['utbildningar_from_month']}', 
									to_year = '{$_POST['utbildningar_to_year']}', 
									to_month = '{$_POST['utbildningar_to_month']}', 
									comments = '{$_POST['utbildningar_comments']}' 
									WHERE id = '{$_POST['utbildningar_edit']}' AND account_id = '". account::user('id') ."'");
				}
				else {
					mysql_query("INSERT INTO arbetssokande_utbildningar SET 
									account_id = '". account::user('id') ."', 
									school = '{$_POST['utbildningar_school']}', 
									program = '{$_POST['utbildningar_program']}', 
									education_level = '{$_POST['utbildningar_education_level']}', 
									from_year = '{$_POST['utbildningar_from_year']}', 
									from_month = '{$_POST['utbildningar_from_month']}', 
									to_year = '{$_POST['utbildningar_to_year']}', 
									to_month = '{$_POST['utbildningar_to_month']}', 
									comments = '{$_POST['utbildningar_comments']}'");
				}
				
				// now lets redirect if there is a redirect
				if(!empty($attr['success_redirect'])) {
					header("Location: {$attr['success_redirect']}");
					exit;
				}
			}
			else {
				$this->errors['utbildningar']['from_to'] = true;
			}
		}
		// update the anstallningar
		else if(isset($_POST['anstallningar_submit'])) {
		
			// Check if the dates are correct
			$from = strtotime("1-{$_POST['anstallningar_from_month']}-{$_POST['anstallningar_from_year']}");
			$to = strtotime("1-{$_POST['anstallningar_to_month']}-{$_POST['anstallningar_to_year']}");
			
			if($from <= $to) {
				// save the information to the database
				if(isset($_POST['anstallningar_edit']) && !empty($_POST['anstallningar_edit'])) {
					mysql_query("UPDATE arbetssokande_anstallningar SET 
									company = '{$_POST['anstallningar_company']}', 
									city = '{$_POST['anstallningar_city']}', 
									tasks = '{$_POST['anstallningar_tasks']}', 
									from_year = '{$_POST['anstallningar_from_year']}', 
									from_month = '{$_POST['anstallningar_from_month']}', 
									to_year = '{$_POST['anstallningar_to_year']}', 
									to_month = '{$_POST['anstallningar_to_month']}', 
									comments = '{$_POST['anstallningar_comments']}' 
									WHERE id = '{$_POST['anstallningar_edit']}' AND account_id = '". account::user('id') ."'");
				}
				else {
					mysql_query("INSERT INTO arbetssokande_anstallningar SET 
									account_id = '". account::user('id') ."', 
									company = '{$_POST['anstallningar_company']}', 
									city = '{$_POST['anstallningar_city']}', 
									tasks = '{$_POST['anstallningar_tasks']}', 
									from_year = '{$_POST['anstallningar_from_year']}', 
									from_month = '{$_POST['anstallningar_from_month']}', 
									to_year = '{$_POST['anstallningar_to_year']}', 
									to_month = '{$_POST['anstallningar_to_month']}', 
									comments = '{$_POST['anstallningar_comments']}'");
				}
				
				// now lets redirect if there is a redirect
				if(!empty($attr['success_redirect'])) {
					header("Location: {$attr['success_redirect']}");
					exit;
				}
			}
			else {
				$this->errors['anstallningar']['from_to'] = true;
			}
		}
		// update the image
		else if(isset($_POST['bild_submit'])) {
		
			$allowedExts = array("jpg", "jpeg", "gif", "png");
			$filesize = 200000; // bytes, 20kb
			
			$extension = end(explode(".", $_FILES["bild_image"]["name"]));
			
			if (in_array($extension, $allowedExts)) {
				if ($_FILES["bild_image"]["error"] > 0) {
					//echo "Ett fel uppstod: " . $_FILES["bild_image"]["error"];
				}
				else {

					if(file_exists(INTERNAL_PATH ."/public/images/uploads/". $_FILES["bild_image"]["name"])) {
						// the image already exists, remove it
						unlink(INTERNAL_PATH ."/public/images/uploads/". $_FILES["bild_image"]["name"]);
					}
					
					// a new image has been uploaded, lets flag for cropping
					// Steps
					// 1 = upload picture
					// 2 = crop image
					// 3 = complete
					
					// if the width or height extends the maximum allowed ones, redirect the user to the crop page
					if(move_uploaded_file($_FILES["bild_image"]["tmp_name"], INTERNAL_PATH ."/public/images/uploads/". $_FILES["bild_image"]["name"])) { 
						$info = getimagesize(INTERNAL_PATH ."/public/images/uploads/". $_FILES["bild_image"]["name"]);
						$width = $info[0];
						$height = $info[1];
						
						// and if the image extends 560 width or 560 height, we crop it immediately to fit the page for cropping
						if($width > 560 || $height > 560) {
							
							image::resize(INTERNAL_PATH ."/public/images/uploads/". $_FILES["bild_image"]["name"],
												INTERNAL_PATH ."/public/images/uploads/". $_FILES["bild_image"]["name"],
												560,
												560);
							
						}
						
					//	if($width > $GLOBALS['parser']->parse("{settings:get value='profile_image_width'/}") || $height > $GLOBALS['parser']->parse("{settings:get value='profile_image_height'/}")) {
						// check if we already uploaded an image before, and should replace it
							@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetssokande_bild WHERE account_id = '". account::user('id') ."'"));
							
							if($check['id'] > 0) {
								mysql_query("UPDATE arbetssokande_bild SET name = '{$_FILES["bild_image"]["name"]}', step = '2' WHERE account_id = '". account::user('id') ."'");
							}
							else {
								mysql_query("INSERT INTO arbetssokande_bild SET account_id = '". account::user('id') ."', name = '{$_FILES["bild_image"]["name"]}', step = '2'");
							}
							header("Location: /konto/mitt-cv/bild/crop");
							exit;
					//	}
					}
				}
			}
		}
		else if(isset($_POST['bild_crop'])) {
			// check if the image already exists in the image database
			$image = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_bild WHERE account_id = '". account::user('id') ."'"));
			
			image::crop(INTERNAL_PATH ."/public/images/uploads/". $image['name'],
								INTERNAL_PATH ."/public/images/users/user_". account::user('id') .".jpg",
								185,
								215,
								$_POST['x'],
								$_POST['y'],
								$_POST['w'],
								$_POST['h'],
								$watermark = true);
								
			// and then we remove the original image to free server space
			image::delete(INTERNAL_PATH ."/public/images/uploads/". $image['name']);
								
			mysql_query("UPDATE arbetssokande_bild SET name = 'user_". account::user('id') .".jpg', step = '3' WHERE account_id = '". account::user('id') ."'");
			
			header("Location: /konto/mitt-cv");
			exit;
		}
	}
	
	public function parser_check_profile_update($tag, $attr) {
		$profile_failed = false;
	
		// first we check the arbetssokande_profil database for a match
		@$check = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_profil WHERE account_id = '". account::user('id') ."'"));
	
		if(!empty($check)) {
			// Now lets check if we have values in the required fields
			foreach($this->profile_mandatory_fields as $field) {
				if(empty($check[$field])) {
					$profile_failed = true;
				}
			}
		}
		else {
			$profile_failed = true;
		}
		
		$redirect = true;
		
		if(isset($attr['allowed_pages']) && !empty($attr['allowed_pages'])) {
			if($_SERVER['REQUEST_URI'] == $attr['redirect']) {
				$redirect = false;
			}
			$allowed_pages = explode("|",$attr['allowed_pages']);
			foreach($allowed_pages as $ap) {
				if($ap == $_SERVER['REQUEST_URI'] || strstr($_SERVER['REQUEST_URI'],$ap)) {
					$redirect = false;
				}
			}
		}
		
		if($profile_failed && $redirect) {
			header("Location: {$attr['redirect']}");
			exit;
		}
	}

	public function parser_fb_check_update($tag, $attr) {
		@$first_name = $_POST['platsbanken_first_name'];
		@$last_name = $_POST['platsbanken_last_name'];
		@$group = $_POST['platsbanken_group'];
		if(isset($_POST['platsbanken_update']) && !empty($_POST['platsbanken_update'])) {
			if(!empty($first_name) && !empty($last_name) && !empty($group)) {
				// They are not empty, lets also check group, has to be 3 or 4, nothing else
				// Group 3 = Employer
				// Group 4 = Employee
				if($group != 3) {
					if($group != 4) {
						$this->failed = true;
					}
				}
				
				if(!$this->failed) {
					@$check = database::query("UPDATE accounts SET first_name = '{$first_name}', last_name = '{$last_name}', groups = '{$group}' WHERE id = '". account::user('id') ."'");
					header("Location: {$attr['success']}");
					exit;
				}
			}
			else {
				$this->failed = true;
			}
		}
	}
	
	public function parser_failed($tag, $attr) {
		if($this->failed) {
			return $tag['innerhtml'];
		}
	}

}

?>