<?php

class parser_arbetsgivare {

	private $accounts = array();
	private $companies = array();
	private $item = array();
	private $subitem = array();
	private $info = array();
	private $failed = array();
	private $existing_company = array();
	private $searched = false;
	private $success = false;
	private $company_id = 0;
	private $total = 0;
	private $search = "";
	private $image = "";
	private $org_number = "";
	private $org_number_state = "empty";
	private $first = false;
	private $last = false;

	function __construct() {
	
		// first we need to get the profile
		if(account::user('id') > 0) {
			$this->parser_get_info("",array("get"=>"companies"));
			$this->parser_get_info("",array("get"=>"account"));
			$this->parser_get_info("",array("get"=>"branschnyheter"));
		}
		
		// COMPANY PRODUCTS
		if(isset($_POST['product_new'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
			@$last = mysql_fetch_array(mysql_query("SELECT position FROM arbetsgivare_products WHERE company_id = '{$company_id}' ORDER BY position DESC LIMIT 0,1"));
			@$position = $last['position'] + 1;
			mysql_query("INSERT INTO arbetsgivare_products SET 
										company_id = '{$company_id}', 
										title = '{$_POST['product_title']}', 
										text = '{$_POST['product_text']}', 
										url = '{$_POST['product_url']}', 
										position = '{$position}'
										");
			@$id = mysql_insert_id();
			
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["product_image"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "product_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/products/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/products/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the big image
				if(move_uploaded_file($_FILES["product_image"]["tmp_name"], INTERNAL_PATH ."/public/images/products/". $newFileBig)) {
					image::resize(INTERNAL_PATH ."/public/images/products/". $newFileBig,
										INTERNAL_PATH ."/public/images/products/". $newFileBig,
										170,
										200);
					mysql_query("UPDATE arbetsgivare_products SET image = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			header("Location: /konto/arbetsgivare/foretag/redigera/".$company_id);
			exit;
		}
		else if(isset($_POST['product_edit'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
			@$last = mysql_fetch_array(mysql_query("SELECT position FROM arbetsgivare_products WHERE company_id = '{$company_id}' ORDER BY position DESC LIMIT 0,1"));
			@$position = $last['position'] + 1;
			mysql_query("UPDATE arbetsgivare_products SET  
										title = '{$_POST['product_title']}', 
										text = '{$_POST['product_text']}', 
										url = '{$_POST['product_url']}'
										WHERE id = '{$_POST['product_id']}'");
			$id = $_POST['product_id'];
			
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["product_image"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "product_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/products/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/products/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the big image
				if(move_uploaded_file($_FILES["product_image"]["tmp_name"], INTERNAL_PATH ."/public/images/products/". $newFileBig)) {
					image::resize(INTERNAL_PATH ."/public/images/products/". $newFileBig,
										INTERNAL_PATH ."/public/images/products/". $newFileBig,
										170,
										200);
					mysql_query("UPDATE arbetsgivare_products SET image = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			header("Location: /konto/arbetsgivare/foretag/redigera/".$company_id);
			exit;
		}
		else if(isset($_POST['product_delete'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
			@$last = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'"));
			
			if(file_exists(INTERNAL_PATH ."/public/images/products/". $last['image'])) {
				unlink(INTERNAL_PATH ."/public/images/products/". $last['image']);
			}
			
			mysql_query("DELETE FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'");
			
			header("Location: /konto/arbetsgivare/foretag/redigera/".$company_id);
			exit;
		}
		else if(isset($_POST['product_move_up'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
			
			@$product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'"));
			@$other_position = $product['position'] - 1;
			@$other_product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE company_id = '{$company_id}' AND position = '{$other_position}'"));


			mysql_query("UPDATE arbetsgivare_products SET position = '{$other_product['position']}' WHERE id = '{$product['id']}'");
			mysql_query("UPDATE arbetsgivare_products SET position = '{$product['position']}' WHERE id = '{$other_product['id']}'");
			
			header("Location: /konto/arbetsgivare/foretag/redigera/".$company_id);
			exit;
		}
		else if(isset($_POST['product_move_down'])) {
			@$company_id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
			
			@$product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE id = '{$_POST['product_id']}'"));
			@$other_position = $product['position'] + 1;
			@$other_product = mysql_fetch_array(mysql_query("SELECT id,position FROM arbetsgivare_products WHERE company_id = '{$company_id}' AND position = '{$other_position}'"));


			mysql_query("UPDATE arbetsgivare_products SET position = '{$other_product['position']}' WHERE id = '{$product['id']}'");
			mysql_query("UPDATE arbetsgivare_products SET position = '{$product['position']}' WHERE id = '{$other_product['id']}'");
			
			header("Location: /konto/arbetsgivare/foretag/redigera/".$company_id);
			exit;
		}
		
		// check if there is an arbetsgivare_accounts row for the user
		@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_accounts WHERE account_id = '". account::user('id') ."'"));
		if($check['id'] <= 0) {
			// There is none, lets create one
			mysql_query("INSERT INTO arbetsgivare_accounts SET account_id = '". account::user('id') ."'");
		}
		
		if($GLOBALS['parser']->parse("{page:rurl index='4'/}") == "radera") {
			@$id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
			if($id > 0) {
				if($GLOBALS['parser']->parse("{page:rurl index='2'/}") == "foretag") {
					@$check = mysql_fetch_array(mysql_query("SELECT owner FROM arbetsgivare_companies WHERE id = '{$id}'"));
					if($check['owner'] == account::user('id')) {
						mysql_query("DELETE FROM arbetsgivare_companies WHERE id = '{$id}' AND owner = '". account::user('id') ."'");
					}
					$this->update_companies($GLOBALS['parser']->parse("{page:rurl index='3'/}"),"remove");
					header("Location: /konto/arbetsgivare/foretag");
					exit;
				}
				else if($GLOBALS['parser']->parse("{page:rurl index='2'/}") == "branschnyhet") {
					mysql_query("DELETE FROM arbetsgivare_branschnyheter WHERE id = '{$id}' AND account_id = '". account::user('id') ."'");
					header("Location: /konto/arbetsgivare/foretag");
					exit;
				}
			}
		}
		
		// Process a request
		if($GLOBALS['parser']->parse("{page:rurl index='3'/}") == "begaran") {
			@$check = mysql_fetch_array(mysql_query("SELECT owner FROM arbetsgivare_companies WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='4'/}") ."'"));
			
			if(isset($check['owner']) && $check['owner'] == account::user('id')) {
				if($GLOBALS['parser']->parse("{page:rurl index='5'/}") == "tillat") {
					mysql_query("UPDATE 
									arbetsgivare_requests 
								SET 
									status = 'processed',
									action = 'allowed' 
								WHERE 
									company_id = '". $GLOBALS['parser']->parse("{page:rurl index='4'/}") ."' 
								AND 
									account_id = '". $GLOBALS['parser']->parse("{page:rurl index='6'/}") ."'
								");
					$this->update_companies($GLOBALS['parser']->parse("{page:rurl index='4'/}"),"add",$GLOBALS['parser']->parse("{page:rurl index='6'/}"));
				}
				else if($GLOBALS['parser']->parse("{page:rurl index='5'/}") == "neka") {
					mysql_query("UPDATE 
									arbetsgivare_requests 
								SET 
									status = 'processed',
									action = 'denied' 
								WHERE 
									company_id = '". $GLOBALS['parser']->parse("{page:rurl index='4'/}") ."' 
								AND 
									account_id = '". $GLOBALS['parser']->parse("{page:rurl index='6'/}") ."'
								");
					$this->update_companies($GLOBALS['parser']->parse("{page:rurl index='4'/}"),"remove",$GLOBALS['parser']->parse("{page:rurl index='6'/}"));
				}
			}
		}
		
		if(isset($_POST['foretag_sok']) && !empty($_POST['foretag_sok'])) {
			$this->search = $_POST['foretag_sok'];
			@$this->sokning = mysql_query("SELECT * FROM arbetsgivare_companies WHERE name LIKE '%{$_POST['foretag_sok']}%' OR org_number LIKE '%{$_POST['foretag_sok']}%'");
			if(mysql_num_rows($this->sokning) > 0) {
				$this->total = mysql_num_rows($this->sokning);
			}
			else {
				// no search results found
				$this->failed['sok'] = true;
			}
			$this->searched = true;
		}
		

	}
	
	public function parser_product_position($tag, $attr) {
	
		if($attr['position'] == "not_first") {
			@$first = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_products WHERE company_id = '{$attr['company_id']}' ORDER BY position ASC LIMIT 0,1"));
			if($attr['id'] != $first['id']) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else if($attr['position'] == "not_last") {
			@$last = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_products WHERE company_id = '{$attr['company_id']}' ORDER BY position DESC LIMIT 0,1"));
			if($attr['id'] != $last['id']) {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}
	
	public function parser_products($tag, $attr) {
		$html = "";
		
		@$id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
		@$products = mysql_query("SELECT * FROM arbetsgivare_products WHERE company_id = '{$id}' ORDER BY position ASC");

		while($product = mysql_fetch_array($products)) {
			$this->item = $product;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_publicera_branschnyhet($tag, $attr) {
		@$id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
		@$check = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '{$id}'"));
		@$account_id = account::user("id");
		if($check['id'] > 0 && $account_id == $check['account_id']) {
			mysql_query("UPDATE arbetsgivare_branschnyheter SET status = 'waiting' WHERE id = '{$id}'");
		}
		header("Location: {$attr['success_redirect']}");
		exit;
	}
	
	public function parser_load_branschnyhet($tag, $attr) {
		@$branschnyhet = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '{$attr['id']}'"));
		@$company = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$branschnyhet['company_id']}'"));
		$this->item = $branschnyhet;
		$this->subitem = $company;
	}
	
	public function parser_access($tag, $attr) {
		@$redigera = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
		$company_id = (isset($attr['company_id']) && !empty($attr['company_id'])) ? $attr['company_id'] : $GLOBALS['parser']->parse("{page:rurl index='4'/}");
		
		if(empty($company_id) || $redigera == "redigera") {
			// Lets check all companies we have access to
			foreach($this->info['companies'] as $company) {
				if($attr['name'] == "branschguiden" && $attr['state'] == "true" && $company['access_branschguiden'] == "true") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				if($attr['name'] == "branschguiden" && $attr['state'] == "false" && $company['access_branschguiden'] == "false") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				if($attr['name'] == "platsbanken" && $attr['state'] == "true" && $company['access_platsbanken'] == "true") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				if($attr['name'] == "platsbanken" && $attr['state'] == "false" && $company['access_platsbanken'] == "false") {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
		}
		else {
			if($attr['name'] == "branschguiden" && $attr['state'] == "true" && $this->info['companies'][$company_id]['access_branschguiden'] == "true") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			if($attr['name'] == "branschguiden" && $attr['state'] == "false" && $this->info['companies'][$company_id]['access_branschguiden'] == "false") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			if($attr['name'] == "platsbanken" && $attr['state'] == "true" && $this->info['companies'][$company_id]['access_platsbanken'] == "true") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
			if($attr['name'] == "platsbanken" && $attr['state'] == "false" && $this->info['companies'][$company_id]['access_platsbanken'] == "false") {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}
	
	public function parser_status_comment($tag, $attr) {
		if(isset($this->info['branschnyheter'][$attr['index']]['status_comment']) && !empty($this->info['branschnyheter'][$attr['index']]['status_comment']) && $this->info['branschnyheter'][$attr['index']]['status'] == "declined") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_status($tag, $attr) {
		if($attr['status'] == $attr['equals']) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_branschnyhet_image($tag, $attr) {
		if($attr['id'] == "1" && !empty($this->info['branschnyheter'][$attr['index']]['image_big_1']) && $attr['state'] == "true") {
			$this->item['image'] = $this->info['branschnyheter'][$attr['index']]['image_big_1'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($attr['id'] == "1" && empty($this->info['branschnyheter'][$attr['index']]['image_big_1']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($attr['id'] == "2" && !empty($this->info['branschnyheter'][$attr['index']]['image_big_2']) && $attr['state'] == "true") {
			$this->item['image'] = $this->info['branschnyheter'][$attr['index']]['image_big_2'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($attr['id'] == "2" && empty($this->info['branschnyheter'][$attr['index']]['image_big_2']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_div_class($tag, $attr) {
		return $this->div_class;
	}
	
	public function parser_is_parent($tag, $attr) {
		if($this->is_parent && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->is_parent && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_in_category($tag, $attr) {
		if(empty($this->in_categories)) {
			@$check = mysql_fetch_array(mysql_query("SELECT categories FROM arbetsgivare_companies WHERE id = '{$attr['company_id']}'"));
			$cats = explode("]",str_replace("[","",$check['categories']));
			foreach($cats as $cat) {
				$this->in_categories[] = $cat;
			}
		}
		
		if(in_array($attr['id'],$this->in_categories) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!in_array($attr['id'],$this->in_categories) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_padding($tag, $attr) {
		return $attr['padding'] * $attr['pixels'];
	}
	
	public function calculate_padding($id) {
		@$category = mysql_fetch_array(mysql_query("SELECT parent FROM branschguiden_categories WHERE id = '{$id}'"));
		
		$count = 0;
		while(!$parent_found) {
			if($category['parent'] != 0) {
				$count++;
				@$category = mysql_fetch_array(mysql_query("SELECT parent FROM branschguiden_categories WHERE id = '{$category['parent']}'"));
			}
			else {
				$parent_found = true;
			}
		}
		
	return $count;
	}
	
	public function sub_categories($id) {
		@$childs = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$id}' ORDER BY name ASC");
		if(mysql_num_rows($childs) > 0) {
			while($child = mysql_fetch_array($childs)) {
				$child['padding'] = $this->calculate_padding($child['id']);
				$this->all_cats[] = $child;
				$this->sub_categories($child['id']);
			}
		}
	}
	
	public function parser_all_categories($tag, $attr) {
		$html = "";
		
		@$parents = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = 0 ORDER BY name ASC");
		
		while($parent = mysql_fetch_array($parents)) {
			$this->all_categories[] = $parent;
		}
		
		foreach($this->all_categories as $parent) {
			@$childs = mysql_query("SELECT * FROM branschguiden_categories WHERE parent = '{$parent['id']}' ORDER BY name ASC");
			$this->all_cats[] = $parent;
			while($child = mysql_fetch_array($childs)) {
				$child['padding'] = $this->calculate_padding($child['id']);
				$this->all_cats[] = $child;
				$this->sub_categories($child['id']);
			}
		}
		
		foreach($this->all_cats as $category) {
			$this->div_class = ($category['parent'] == 0) ? "all_cats_parent" : "all_cats_child";
			$this->is_parent = ($category['parent'] == 0) ? true : false;
			$this->item = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_org_number($tag, $attr) {
		if($attr['state'] == "empty" && $this->org_number_state == "empty") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($attr['state'] == "exists" && $this->org_number_state == "exists") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($attr['state'] == "create" && $this->org_number_state == "create") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_get_org_number($tag, $attr) {
		if(isset($_POST['companies_org_number']) && !empty($_POST['companies_org_number'])) {
			return $_POST['companies_org_number'];
		}
		else {
			return $this->org_number;
		}
	}
	
	public function parser_check_org_number($tag, $attr) {
		if(isset($_POST['org_number_check'])) {
			if(empty($_POST['org_number'])) {
				$this->org_number_state = "empty";
			}
			else {
				$orgnr = preg_replace('/[^0-9]/', '', $_POST['org_number']);
				$check = mysql_query("SELECT * FROM arbetsgivare_companies WHERE org_number = '{$orgnr}'");
				
				// If there is a company with this org number already
				// redirect the user to ask for permission
				if(mysql_num_rows($check) > 0) {
					$this->org_number_state = "exists";
					$this->item = mysql_fetch_array($check);
				} // If not, then redirect the user to create a new company
				else {
					$this->org_number_state = "create";
					$this->org_number = $orgnr;
				}
			}
		}
		else if(isset($_POST['companies_org_number']) && !empty($_POST['companies_org_number'])) {
			$this->org_number_state = "create";
		}
	}
	
	public function parser_waiting_requests($tag, $attr) {
		$html = "";
		if(!isset($attr['company_id'])) {
			$check = mysql_query("SELECT a.id FROM arbetsgivare_companies AS a 
									INNER JOIN arbetsgivare_requests AS b 
									ON b.company_id = a.id 
									WHERE a.owner = '". account::user('id') ."' AND b.status = 'waiting'");
						
			if(mysql_num_rows($check) > 0) {
				$this->total = mysql_num_rows($check);
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
		else {
			$check = mysql_query("SELECT id FROM arbetsgivare_requests  
									WHERE company_id = '{$attr['company_id']}' AND status = 'waiting'");
						
			if(mysql_num_rows($check) > 0) {
				$this->total = mysql_num_rows($check);
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}
	
	public function parser_get_company($tag, $attr) {
		if(empty($this->companies[$attr['id']])) {
			@$this->companies[$attr['id']] = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$attr['id']}'"));
		}
		return $this->companies[$attr['id']][$attr['get']];
	}
	
	public function parser_get_account($tag, $attr) {
		if(empty($this->accounts[$attr['id']])) {
			@$this->accounts[$attr['id']] = mysql_fetch_array(mysql_query("SELECT * FROM accounts WHERE id = '{$attr['id']}'"));
		}
		return $this->accounts[$attr['id']][$attr['get']];
	}
	
	public function parser_requests($tag, $attr) {
		$html = "";
		$requests = mysql_query("SELECT * FROM `arbetsgivare_requests` WHERE `company_id` = '{$attr['id']}' ORDER BY status DESC");
		if(mysql_num_rows($requests) > 0) {
			while($request = mysql_fetch_array($requests)) {
				$this->item = $request;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	return $html;
	}
	
	public function parser_owns_company($tag, $attr) {
		@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_companies WHERE id = '{$attr['id']}' AND owner = '". account::user('id') ."'"));
		if($check['id'] > 0 && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($check['id'] <= 0 && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_request_status($tag, $attr) {
		// First we check if the user has access to the company
		@$check = mysql_fetch_array(mysql_query("SELECT companies FROM arbetsgivare_accounts WHERE account_id = '{$attr['account_id']}'"));
		$companies = explode(",",$check['companies']);
		if(in_array($attr['company_id'],$companies)) {
			// We are allowed
			return $attr['allowed'];
		}
		else {
			// The user does not have access to the company, did he remove it, or did we deny him?
			@$check = mysql_fetch_array(mysql_query("SELECT `status`,`action` FROM arbetsgivare_requests WHERE account_id = '{$attr['account_id']}' AND company_id = '{$attr['company_id']}'"));
			if($check['status'] == "waiting") {
				return $attr['waiting'];
			}
			else {
				if($check['action'] == "denied") {
					return $attr['denied'];
				}
				else {
					return $attr['user_removed'];
				}
			}
		}
	}
	
	public function parser_waiting_request($tag, $attr) {
		@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_requests WHERE account_id = '". account::user('id') ."' AND company_id = '{$attr['id']}' AND status = 'waiting'"));
		if($check['id'] > 0 && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if($check['id'] <= 0 && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_send_request($tag, $attr) {
		@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_requests WHERE account_id = '". account::user('id') ."' AND company_id = '{$attr['id']}'"));
		if($check['id'] <= 0) {
			mysql_query("INSERT INTO arbetsgivare_requests SET account_id = '". account::user('id') ."', company_id = '{$attr['id']}', requested_at = '". time() ."'");
			@$this->item = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$attr['id']}'"));
			$this->success = true;
		}
	}
	
	public function parser_in_company($tag, $attr) {
		@$companies = explode(",",$this->info['account']['companies']);
		if(in_array($attr['id'],$companies) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!in_array($attr['id'],$companies) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_success($tag, $attr) {
		if($this->success && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(!$this->success && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_searched($tag, $attr) {
		if($this->searched) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_search($tag, $attr) {
		return $this->search;
	}
	
	public function parser_total($tag, $attr) {
		return $this->total;
	}
	
	public function parser_sok($tag, $attr) {
		$html = "";
		if(mysql_num_rows($this->sokning) > 0) {
			while($sok = mysql_fetch_array($this->sokning)) {
				$this->item = $sok;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
				
	return $html;
	}
	
	public function update_companies($id,$action,$account=0) {
		if($account > 0) {
			$get = mysql_fetch_array(mysql_query("SELECT companies FROM arbetsgivare_accounts WHERE account_id = '{$account}'"));
			if($action == "add") {
				$companies = explode(",",$get['companies']);
				if(!in_array($id,$companies)) {
					$companies[] = $id;
				}
				foreach($companies as $company) {
					if(!empty($company)) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '{$account}'");
			}
			else if($action == "remove") {
				$companies = explode(",",$get['companies']);
				foreach($companies as $company) {
					if($company != $id) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '{$account}'");
			}
		}
		else {
			if($action == "add") {
				$companies = explode(",",$this->info['account']['companies']);
				if(!in_array($id,$companies)) {
					$companies[] = $id;
				}
				foreach($companies as $company) {
					if(!empty($company)) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '". account::user('id') ."'");
			}
			else if($action == "remove") {
				$companies = explode(",",$this->info['account']['companies']);
				foreach($companies as $company) {
					if($company != $id) {
						$companies_new[] = $company;
					}
				}
				$new_company_ids = implode(",",$companies_new);
				mysql_query("UPDATE arbetsgivare_accounts SET companies = '{$new_company_ids}' WHERE account_id = '". account::user('id') ."'");
			}
		}
	}
	
	public function url_friendly($string) {
		$url = str_replace("ö","o",strtolower($string));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("ä","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function fix_files_array($arr) {
		$i = 0;
		foreach($arr as $key => $all) {
			foreach($all as $i => $val) {
				$new[$i][$key] = $val;   
			}   
		}
		return $new;
	}
	
	public function parser_update_branschnyhet($tag, $attr) {
	
		// update the personuppgifter
		if(isset($_POST['branschnyheter_submit']) || isset($_POST['branschnyheter_edit']) || isset($_POST['branschnyheter_preview'])) {
			
			$url_friendly = $this->url_friendly($_POST['branschnyheter_title']);
			@$id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
			
			// save the information to the database
			if(isset($_POST['branschnyheter_edit']) && !empty($_POST['branschnyheter_edit'])) {
				if($id > 0) {
					mysql_query("UPDATE arbetsgivare_branschnyheter SET 
									title = '{$_POST['branschnyheter_title']}', 
									breadcrumb = '{$_POST['branschnyheter_breadcrumb']}', 
									text = '{$_POST['branschnyheter_text']}',
									status = 'waiting', 
									url_friendly = '{$url_friendly}' 
									WHERE id = '{$id}'");
				}
			}
			else if(isset($_POST['branschnyheter_submit'])) {
				mysql_query("INSERT INTO arbetsgivare_branschnyheter SET 
								company_id = '{$_POST['branschnyheter_company']}', 
								account_id = '". account::user('id') ."', 
								added = '". time() ."', 
								title = '{$_POST['branschnyheter_title']}', 
								breadcrumb = '{$_POST['branschnyheter_breadcrumb']}', 
								text = '{$_POST['branschnyheter_text']}', 
								status = 'waiting', 
								url_friendly = '{$url_friendly}'");
								
				// Set the new company id
				$company_id = mysql_insert_id();
			}
			else if(isset($_POST['branschnyheter_preview'])) {
				if($id <= 0) {
					mysql_query("INSERT INTO arbetsgivare_branschnyheter SET 
									company_id = '{$_POST['branschnyheter_company']}', 
									account_id = '". account::user('id') ."', 
									added = '". time() ."', 
									title = '{$_POST['branschnyheter_title']}', 
									breadcrumb = '{$_POST['branschnyheter_breadcrumb']}', 
									text = '{$_POST['branschnyheter_text']}', 
									status = 'draft', 
									url_friendly = '{$url_friendly}'");
					$id = mysql_insert_id();
				}
				else {
					mysql_query("UPDATE arbetsgivare_branschnyheter SET 
									title = '{$_POST['branschnyheter_title']}', 
									breadcrumb = '{$_POST['branschnyheter_breadcrumb']}', 
									text = '{$_POST['branschnyheter_text']}',
									status = 'draft', 
									url_friendly = '{$url_friendly}' 
									WHERE id = '{$id}'");
				}
			}
			
			$id = (!empty($company_id)) ? $company_id : $id;

			if(isset($_FILES)) {
				@$files = $this->fix_files_array($_FILES['branschnyheter_bild']);
				
				// Delete any images if requested
				if(isset($_POST['branschnyheter_bild_radera_1'])) {
					unlink(INTERNAL_PATH ."/public/images/branschnyheter/{$this->info['branschnyheter'][$id]['image_big_1']}");
					unlink(INTERNAL_PATH ."/public/images/branschnyheter/{$this->info['branschnyheter'][$id]['image_thumb_1']}");
					mysql_query("UPDATE arbetsgivare_branschnyheter SET image_big_1 = '', image_thumb_1 = '' WHERE id = '{$id}'");
				}
				if(isset($_POST['branschnyheter_bild_radera_2'])) {
					unlink(INTERNAL_PATH ."/public/images/branschnyheter/{$this->info['branschnyheter'][$id]['image_big_2']}");
					mysql_query("UPDATE arbetsgivare_branschnyheter SET image_big_2 = '' WHERE id = '{$id}'");
				}
				
				$i = 1;
				foreach($files as $file) {
				
					@$allowedExts = array("jpg", "jpeg", "gif", "png");
					@$extension = end(explode(".", $file["name"]));
					
					if(in_array($extension, $allowedExts)) {
						$newName = "branschnyhet_".$id;
						
						if(file_exists(INTERNAL_PATH ."/public/images/branschnyheter/". $newName . $extension)) {
							unlink(INTERNAL_PATH ."/public/images/branschnyheter/". $newName . $extension);
						}
						
						$newFileBig = $newName ."_big_". $i .".". $extension;
						$newFileThumb = $newName ."_thumb_". $i .".". $extension;
						
						// Create the big image
						if(move_uploaded_file($file["tmp_name"], INTERNAL_PATH ."/public/images/branschnyheter/". $newFileBig)) {
							image::resize(INTERNAL_PATH ."/public/images/branschnyheter/". $newFileBig,
												INTERNAL_PATH ."/public/images/branschnyheter/". $newFileBig,
												471,
												360);
							
							// Create the thumb if it's the first image
							if($i == 1) {
								image::resize(INTERNAL_PATH ."/public/images/branschnyheter/". $newFileBig,
													INTERNAL_PATH ."/public/images/branschnyheter/". $newFileThumb,
													150,
													100);
								mysql_query("UPDATE arbetsgivare_branschnyheter SET image_big_1 = '{$newFileBig}', image_thumb_1 = '{$newFileThumb}' WHERE id = '{$id}'");
							}
							else {
								mysql_query("UPDATE arbetsgivare_branschnyheter SET image_big_2 = '{$newFileBig}' WHERE id = '{$id}'");
							}
						}
					}
					
				$i++;
				}
			}
			
			if(isset($_POST['branschnyheter_preview'])) {
				header("Location: /konto/arbetsgivare/branschnyhet/forhandsgranskning/{$id}");
				exit;
			}
			
			// now lets redirect if there is a redirect
			if(!empty($attr['success_redirect'])) {
				header("Location: {$attr['success_redirect']}");
				exit;
			}
		}
	}
	
	public function parser_update_info($tag, $attr) {
	
		// update the personuppgifter
		if(isset($_POST['companies_submit'])) {
			
			$url_friendly = $this->url_friendly($_POST['companies_name']);
			
			// Fix the categories
			$categories = "";
			foreach($_POST['companies_categories'] as $category) {
				$categories .= "[{$category}]";
			}
			
			// save the information to the database
			if(isset($_POST['companies_edit']) && !empty($_POST['companies_edit'])) {
				mysql_query("UPDATE arbetsgivare_companies SET 
								name = '{$_POST['companies_name']}', 
								description = '{$_POST['companies_description']}', 
								street = '{$_POST['companies_street']}', 
								city = '{$_POST['companies_city']}', 
								zip = '{$_POST['companies_zip']}', 
								website = '{$_POST['companies_website']}', 
								categories = '{$categories}', 
								contact_mail = '{$_POST['companies_contact_mail']}', 
								telephone = '{$_POST['companies_telephone']}',  
								url_friendly = '{$url_friendly}', 
								fax = '{$_POST['companies_fax']}' 
								WHERE id = '{$_POST['companies_edit']}' AND owner = '". account::user('id') ."'");
			}
			else {
				mysql_query("INSERT INTO arbetsgivare_companies SET 
								owner = '". account::user('id') ."', 
								name = '{$_POST['companies_name']}', 
								description = '{$_POST['companies_description']}', 
								org_number = '{$_POST['companies_org_number']}', 
								street = '{$_POST['companies_street']}', 
								city = '{$_POST['companies_city']}', 
								zip = '{$_POST['companies_zip']}', 
								website = '{$_POST['companies_website']}', 
								categories = '{$categories}', 
								contact_mail = '{$_POST['companies_contact_mail']}', 
								telephone = '{$_POST['companies_telephone']}', 
								url_friendly = '{$url_friendly}', 
								fax = '{$_POST['companies_fax']}'");
								
				// Then we update the arbetsgivare_accounts, and set the new company id
				$company_id = mysql_insert_id();
				$this->update_companies($company_id,"add");
			}
			
			$id = (!empty($company_id)) ? $company_id : $_POST['companies_edit'];
			
			if(isset($_POST['companies_bild_radera'])) {
				@$image = mysql_fetch_array(mysql_query("SELECT image_big, image_thumb FROM arbetsgivare_companies WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='4'/}") ."'"));
				unlink(INTERNAL_PATH ."/public/images/companies/". $image['image_big']);
				unlink(INTERNAL_PATH ."/public/images/companies/". $image['image_thumb']);
				mysql_query("UPDATE arbetsgivare_companies SET image_big = '', image_thumb = '' WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='4'/}") ."'");
			}
			
			// Check if we have an image to upload
			if(isset($_FILES["companies_bild"]["name"])) {
				
				@$allowedExts = array("jpg", "jpeg", "gif", "png");
				@$extension = end(explode(".", $_FILES["companies_bild"]["name"]));
				
				if(in_array($extension, $allowedExts)) {
					$newName = "company_".$id;
					
					if(file_exists(INTERNAL_PATH ."/public/images/companies/". $newName . $extension)) {
						unlink(INTERNAL_PATH ."/public/images/companies/". $newName . $extension);
					}
					
					$newFileBig = $newName ."_big.". $extension;
					$newFileThumb = $newName ."_thumb.". $extension;
					
					// Create the big image
					if(move_uploaded_file($_FILES["companies_bild"]["tmp_name"], INTERNAL_PATH ."/public/images/companies/". $newFileBig)) {
						image::resize(INTERNAL_PATH ."/public/images/companies/". $newFileBig,
											INTERNAL_PATH ."/public/images/companies/". $newFileBig,
											215,
											145);
											
						image::resize(INTERNAL_PATH ."/public/images/companies/". $newFileBig,
											INTERNAL_PATH ."/public/images/companies/". $newFileThumb,
											150,
											100);
					}
					mysql_query("UPDATE arbetsgivare_companies SET image_big = '{$newFileBig}', image_thumb = '{$newFileThumb}' WHERE id = '{$id}'");
				}
			}
			
			// now lets redirect if there is a redirect
			if(!empty($attr['success_redirect'])) {
				header("Location: {$attr['success_redirect']}");
				exit;
			}
		}
	}
	
	public function parser_companies_branschnyheter($tag, $attr) {
		$html = "";
		foreach($this->info['companies'] as $company) {
			if($company['access_branschguiden'] == "true") {
				$this->item = $company;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	return $html;
	}
	
	public function parser_image($tag, $attr) {
		return @$this->image;
	}
	
	public function parser_has_image($tag, $attr) {
		@$id = $GLOBALS['parser']->parse("{page:rurl index='4'/}");
		@$company = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$id}'"));
		if(!empty($company['image_big'])) {
			$this->image = $company['image_big'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_get_info($tag, $attr) {
		if($attr['get'] == "companies") {
			@$info = mysql_fetch_array(mysql_query("SELECT companies FROM arbetsgivare_accounts WHERE account_id = '". account::user('id') ."'"));
			if(isset($info['companies']) && !empty($info['companies'])) {
				$companies = explode(",",$info['companies']);
				foreach($companies as $id) {
					$company = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$id}'"));
					$this->info['companies'][$id] = $company;
				}
			}
		}
		else if($attr['get'] == "branschnyheter") {
			@$info = mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE account_id = '". account::user('id') ."'");
			if(mysql_num_rows($info) > 0) {
				while($branschnyhet = mysql_fetch_array($info)) {
					$this->info['branschnyheter'][$branschnyhet['id']] = $branschnyhet;
				}
			}
		}
		else if($attr['get'] == "account") {
			@$info = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_accounts WHERE account_id = '". account::user('id') ."'"));
			$this->info['account'] = $info;
		}
	}
	
	public function parser_branschnyheter($tag, $attr) {
		$html = "";
		if(isset($this->info['branschnyheter'])) {
			foreach($this->info['branschnyheter'] as $branschnyhet) {
				if(!empty($branschnyhet['id'])) {
					$this->item = $branschnyhet;
					$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
				}
			}
		}
	return $html;
	}
	
	public function parser_companies($tag, $attr) {
		$html = "";
		if(isset($this->info['companies'])) {
			foreach($this->info['companies'] as $company) {
				$this->item = $company;
				$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	return $html;
	}
	
	public function parser_subitem($tag, $attr) {
		return @$this->item[$attr['get']];
	}
	
	public function parser_item($tag, $attr) {
		return @$this->item[$attr['get']];
	}
	
	public function parser_check_companies($tag, $attr) {
		// Check if the user has created any company, or is linked to an existing one, to use for posting new jobs
		@$check = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_accounts WHERE account_id = '". account::user('id') ."'"));
		if($check['id'] <= 0) {
			if(isset($attr['redirect']) && !empty($attr['redirect'])) {
				header("Location: {$attr['redirect']}");
				exit;
			}
			else {
				return $GLOBALS['parser']->parse($tag['innerhtml']);
			}
		}
	}
	
	public function parser_info($tag, $attr) {
		// Posted values
		if(isset($attr['posted_first']) && !empty($attr['posted_first']) && isset($_POST["{$attr['get']}_{$attr['name']}"]) && !empty($_POST["{$attr['get']}_{$attr['name']}"])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $_POST["{$attr['get']}_{$attr['name']}"]) {
					return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
				}
				else {
					return "";
				}
			}
			else {
				return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
			}
		} // Stored values
		else if(isset($this->info[$attr['get']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['name']]) || 
		isset($attr['index']) && isset($this->info[$attr['get']][$attr['index']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['index']][$attr['name']])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $this->info[$attr['get']][$attr['name']] || isset($attr['index']) && $attr['value'] == $this->info[$attr['get']][$attr['index']][$attr['name']]) {
					if(isset($attr['index'])) {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
					}
					else {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
					}
				}
				else {
					return "";
				}
			}
			else {
				if(isset($attr['index'])) {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
				}
				else {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
				}
			}
		}
		else {
			return "";
		}
	}

}

?>