<?php

class parser_menu {

	public $items;
	public $item;
	public $sub_items;
	public $is_parent = false;
	public $is_active = false;

	function __construct() {
		@$menu = mysql_query("SELECT * FROM menu ORDER BY `order`");
		
		while($item = mysql_fetch_array($menu)) {
			$this->items[$item['id']] = $item;
			
			if($item['parent'] > 0) {
				$subs[] = $item['parent'];
				$this->sub_items[$item['parent']][] = $item;
			}
		}
		
		foreach($subs as $sub) {
			$this->items[$sub]['has_sub'] = 1;
		}
	}
	public function parser_items($tag, $attr) {
		$html = "";
		
		@$uri = explode("/",mysql_real_escape_string($_SERVER['REQUEST_URI']));
		
		foreach($this->items as $item) {
			$this->item = $item;
			@$this->is_parent = ($this->item['has_sub'] >= 1) ? true : false;
			$this->is_active = ("/{$uri[1]}" == $this->item['url']) ? true : false;
			$html .= ($this->item['parent'] == 0) ? $GLOBALS['parser']->parse($tag['innerhtml']) : "";
		}
	
	return $html;
	}
	
	public function parser_sub_items($tag, $attr) {
		$html = "";
		
		foreach($this->sub_items[$this->item['id']] as $item) {
			$this->item = $item;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_has_sub($tag, $attr) {
		return @($this->item['has_sub'] == 1) ? $GLOBALS['parser']->parse($tag['innerhtml']) : "" ;
	}
	
	public function parser_is_active($tag, $attr) {
		return ($this->is_active) ? $attr['print'] : "";
	}
	
	public function parser_is_parent($tag, $attr) {
		return ($this->is_parent) ? $attr['print'] : "";
	}
	
	public function parser_url($tag, $attr) {
		return $GLOBALS['parser']->parse($this->item['url']);
	}
	
	public function parser_name($tag, $attr) {
		return $GLOBALS['parser']->parse("{translate:get id='{$this->item['translate_id']}'/}");
	}

}

?>