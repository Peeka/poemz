<?php

class parser_if {

	function __construct() {
	
	}
	
	public function parser_exists($tag, $attr) {
		if(strstr($attr['string'],$attr['check'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_not_exists($tag, $attr) {
		if(!strstr($attr['string'],$attr['check'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_empty($tag, $attr) {
		if(empty($attr['string'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_not_empty($tag, $attr) {
		if(!empty($attr['string'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			if(!empty($attr['else'])) {
				return $GLOBALS['parser']->parse($attr['else']);
			}
		}
	}
	
	public function parser_not_equals($tag, $attr) {
		if($attr['arg1'] != $attr['arg2']) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_equals($tag, $attr) {
		if($attr['arg1'] == $attr['arg2']) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

}

?>