<?php

class parser_kontakt {

	private $info = array();
	private $item = array();
	private $subitem = array();
	private $message = "";

	function __construct() {
		// admin requests
		@$rurl = $GLOBALS['page']->get('rurl');
		if($rurl[3] == "accept") {
			mysql_query("UPDATE contact_annons SET status = 'accepted' WHERE id = '{$rurl[4]}'");
		}
		else if($rurl[3] == "decline") {
			mysql_query("UPDATE contact_annons SET status = 'declined' WHERE id = '{$rurl[4]}'");
		}
	}
	
	public function parser_requests($tag, $attr) {
		$html = "";
		@$status = (isset($attr['status']) && !empty($attr['status'])) ? " WHERE status = '{$attr['status']}'" : "";
		@$requests = mysql_query("SELECT * FROM contact_annons{$status} ORDER BY `added` DESC");
		while($request = mysql_fetch_array($requests)) {
			$this->item = $request;
			$this->item['sent'] = date("H:i, d-m-y", $request['added']);
			if($request['type'] == "branschguiden") {
				$this->item['type'] = "Branschguiden";
			}
			else if($request['type'] == "annons") {
				$this->item['type'] = "Annons";
			}
			else if($request['type'] == "platsbanken") {
				$this->item['type'] = "Platsbanken";
			}
			else if($request['type'] == "kop_salj") {
				$this->item['type'] = "Köp & Säljmarknad för leverantörer och mäklare";
			}
			else if($request['type'] == "ovrigt") {
				$this->item['type'] = "Övrigt";
			}
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_check($tag, $attr) {
		if(isset($_POST['contact_submit'])) {
		/*	mysql_query("INSERT INTO contact_annons SET 
							name = '{$_POST['contact_name']}',
							email = '{$_POST['contact_email']}',
							company_name = '{$_POST['contact_company_name']}',
							telephone = '{$_POST['contact_telephone']}',
							message = '{$_POST['contact_message']}',
							type = '{$_POST['contact_type']}', 
							added = '". time() ."', 
							ip = '". $_SERVER['REMOTE_ADDR'] ."'
							");
		*/
		
			$message = '<b>Skickat:</b> '. date("H:i m d", time()) .'<br />
						<b>IP:</b> '. $_SERVER['REMOTE_ADDR'] .'<br />
						<b>Typ:</b> '. $_POST['contact_type'] .'<br />
						<b>Namn:</b> '. $_POST['contact_name'] .'<br />
						<b>Email:</b> '. $_POST['contact_email'] .'<br />
						<b>Företags Namn:</b> '. $_POST['contact_company_name'] .'<br />
						<b>Telefon:</b> '. $_POST['contact_telephone'] .'<br />
						<b>Meddelande:</b><br />
						'. nl2br($_POST['contact_message']);
							
			//send email to info@hrtorget.se
			/*
			// get the mail template, which is ID 2
				@$mail_template = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = 2"));
				$tags['link'] = "http://www.mybutterfly.se/konto/nytt-losenord/$new_password_hash";
				$template['mail_title'] = $mail_template['mail_title'];
				$template['mail_text'] = $mail_template['mail_text'];
				
				// replace the tags
				// [link] = the link to the new password url
				$mail = $GLOBALS['mail']->tags($tags,$template);
			*/
			
			$GLOBALS['mail']->send('annonsera@hrtorget.se', 'Annonseringsfrågor + '. ucfirst($_POST['contact_type']), $message);
			
			$this->message = "Ditt meddelande har nu skickats.";
		}
	}
	
	public function parser_message($tag, $attr) {
		if(!empty($this->message) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($this->message) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($attr['state'])) {
			return $this->message;
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

}

?>