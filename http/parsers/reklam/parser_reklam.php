<?php

class parser_reklam {

	private $info = array();
	private $item = array();
	private $subitem = array();
	private $image = "";

	function __construct() {
		@$id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
		if($id > 0) {
			@$this->item = mysql_fetch_array(mysql_query("SELECT * FROM reklam WHERE id = '{$id}'"));
		}
		
		// ADMIN CALENDAR
		
		@$radera = $GLOBALS['parser']->parse('{page:rurl index="3"/}');
		
		if($radera == "radera") {
			@$item = mysql_fetch_array(mysql_query("SELECT flash,image FROM reklam WHERE id = '{$id}'"));
			if(file_exists(INTERNAL_PATH ."/public/images/advertisement/". $item['flash'])) {
				unlink(INTERNAL_PATH ."/public/images/advertisement/". $item['flash']);
			}
			if(file_exists(INTERNAL_PATH ."/public/images/advertisement/". $item['image'])) {
				unlink(INTERNAL_PATH ."/public/images/advertisement/". $item['image']);
			}
			mysql_query("DELETE FROM reklam WHERE id = '{$id}'");
		}
		
		if(isset($_POST['reklam_add'])) {
		
			$start_date = strtotime($_POST['reklam_start_date']);
			$end_date = strtotime($_POST['reklam_end_date']);
		
			mysql_query("INSERT INTO reklam SET 
							name = '{$_POST['reklam_name']}', 
							url = '{$_POST['reklam_url']}', 
							start_date = '{$start_date}', 
							end_date = '{$end_date}', 
							type = '{$_POST['reklam_type']}'");
			@$id = mysql_insert_id();
			
			
			// upload the flash file
			@$allowedExts = array("swf");
			@$extension = end(explode(".", $_FILES["reklam_flash"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_{$_POST['reklam_type']}_flash_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the flash
				if(move_uploaded_file($_FILES["reklam_flash"]["tmp_name"], INTERNAL_PATH ."/public/images/advertisement/". $newFileBig)) {
					mysql_query("UPDATE reklam SET flash = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			// upload the image
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["reklam_image"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_{$_POST['reklam_type']}_image_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the image
				if(move_uploaded_file($_FILES["reklam_image"]["tmp_name"], INTERNAL_PATH ."/public/images/advertisement/". $newFileBig)) {
					if($_POST['reklam_type'] == "big") {
						image::resize(INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											950,
											120);
					}
					else if($_POST['reklam_type'] == "small") {
						image::resize(INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											190,
											285);
					}
					else if($_POST['reklam_type'] == "startpage") {
						image::resize(INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											511,
											120);
					}
					mysql_query("UPDATE reklam SET image = '{$newFileBig}' WHERE id = '{$id}'");
				}
			}
			
			header("Location: /admin/reklam/{$_POST['reklam_type']}");
			exit;
		}
		else if(isset($_POST['reklam_edit'])) {
			@$id = $GLOBALS['parser']->parse('{page:rurl index="4"/}');
		
			$start_date = strtotime($_POST['reklam_start_date']);
			$end_date = strtotime($_POST['reklam_end_date']);
							
			mysql_query("UPDATE reklam SET 
							name = '{$_POST['reklam_name']}', 
							url = '{$_POST['reklam_url']}', 
							start_date = '{$start_date}', 
							end_date = '{$end_date}' 
							WHERE id = '{$id}'") or die(mysql_error());
			
			// upload the flash file
			@$allowedExts = array("swf");
			@$extension = end(explode(".", $_FILES["reklam_flash"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_{$_POST['reklam_type']}_flash_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the flash
				if(move_uploaded_file($_FILES["reklam_flash"]["tmp_name"], INTERNAL_PATH ."/public/images/advertisement/". $newFileBig)) {
					
				}
				mysql_query("UPDATE reklam SET flash = '{$newFileBig}' WHERE id = '{$id}'");
			}
			
			// upload the image
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $_FILES["reklam_image"]["name"]));
			
			if(in_array($extension, $allowedExts)) {
				$newName = "ad_{$_POST['reklam_type']}_image_".$id;
				
				if(file_exists(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension)) {
					unlink(INTERNAL_PATH ."/public/images/advertisement/". $newName . $extension);
				}
				
				$newFileBig = $newName .".". $extension;
				
				// Create the image
				if(move_uploaded_file($_FILES["kalender_image"]["tmp_name"], INTERNAL_PATH ."/public/images/advertisement/". $newFileBig)) {
					if($_POST['reklam_type'] == "big") {
						image::resize(INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											950,
											120);
					}
					else if($_POST['reklam_type'] == "small") {
						image::resize(INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											190,
											285);
					}
					else if($_POST['reklam_type'] == "startpage") {
						image::resize(INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											INTERNAL_PATH ."/public/images/advertisement/". $newFileBig,
											511,
											120);
					}
				}
				mysql_query("UPDATE reklam SET image = '{$newFileBig}' WHERE id = '{$id}'");
			}
			
			header("Location: /admin/reklam/{$_POST['reklam_type']}");
			exit;
		}
	}
	
	public function parser_amount_of_smalls($tag, $attr) {
		@$ads = mysql_fetch_array(mysql_query("SELECT count(id) AS amount FROM reklam WHERE type = 'small'"));
		return $ads['amount'];
	}
	
	public function parser_startpage($tag, $attr) {
		$html = "";
		@$ads = mysql_query("SELECT * FROM reklam WHERE type = 'startpage' ORDER BY rand()");
		$i = 1;
		while($ad = mysql_fetch_array($ads)) {
			$this->item = "";
			$this->item = $ad;
			$this->item['number'] = $i;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		$i++;
		}
	return $html;
	}
	
	public function parser_small_banners($tag, $attr) {
		$html = "";
		@$ads = mysql_query("SELECT * FROM reklam WHERE type = 'small' ORDER BY rand()");
		$i = 1;
		while($ad = mysql_fetch_array($ads)) {
			$this->item = "";
			$this->item = $ad;
			$this->item['number'] = $i;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		$i++;
		}
	return $html;
	}
	
	public function parser_big_banners($tag, $attr) {
		$html = "";
		@$ads = mysql_query("SELECT * FROM reklam WHERE type = 'big' ORDER BY name ASC LIMIT 0,1");
		while($ad = mysql_fetch_array($ads)) {
			$this->item = "";
			$this->item = $ad;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_list($tag, $attr) {
		$html = "";
		@$ads = mysql_query("SELECT * FROM reklam WHERE type = '{$attr['type']}' ORDER BY name ASC");
		while($ad = mysql_fetch_array($ads)) {
			$this->item = "";
			$this->item = $ad;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_has_image($tag, $attr) {
		if(isset($this->item['image']) && !empty($this->item['image']) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($this->item['image']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_has_flash($tag, $attr) {
		if(isset($this->item['flash']) && !empty($this->item['flash']) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($this->item['flash']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if($attr['get'] == "start_date" || $attr['get'] == "end_date") {
				return date("m/d/Y", $this->item[$attr['get']]);
			}
			else if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

}

?>