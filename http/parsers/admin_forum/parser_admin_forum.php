<?php

class parser_admin_forum {

  	public $item = array();
	public $search_parameter = "";

    function __construct() {
        @$rurl = $GLOBALS['page']->get('rurl');

        if(isset($rurl[2]) && $rurl[2] == "reject") {
            mysql_query("UPDATE forum SET accepted='0' WHERE id = '" . $rurl[3] . "'");
		} else if(isset($rurl[2]) && $rurl[2] == "accept") {
            mysql_query("UPDATE forum SET accepted='1' WHERE id = '" . $rurl[3] . "'");

		} else if(isset($rurl[2]) && $rurl[2] == "delete")  {
			mysql_query("UPDATE forum SET active='0' WHERE id = '" . $rurl[3] . "'");
		}
	}

    public function parser_list($tag, $attr) {
		$html = ''; 

        if (isset($attr['type'])) {

			$sql = "SELECT f.id,f.account_id,f.title,a.first_name,a.last_name,f.text,f.created_on,count(f2.id) as replies
					FROM forum AS f
					INNER JOIN accounts a ON (a.id = f.account_id)
					LEFT JOIN forum f2 ON (f.id = f2.parent_id)
					WHERE f.accepted = '" . $attr['type'] . "' and f.active='1'";

			//search filter
			if(isset($_POST['search_forum']) && !empty($_POST['search_forum'])) {
				$sql .= " AND ( ( concat(a.first_name ,' ', a.last_name) LIKE '%{$_POST['search_forum']}%') OR ( f.title LIKE '%{$_POST['search_forum']}%')) ";

				$this->search_parameter = $_POST['search_forum'];
			}

			$sql .= "GROUP BY f.id
                     ORDER BY f.created_on DESC";

            $forum_list_query = mysql_query($sql);

    		while($forum_list = mysql_fetch_array($forum_list_query)) {

    			$this->item = $forum_list;
    			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
    		}
        }
    	return $html;
	}

    public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_get_search_parameter($tag, $attr) {
		return $this->search_parameter;
	}
}
?>