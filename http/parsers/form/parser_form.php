<?php

class parser_form {

	function __construct() {
	
	}
	
	public function parser_checked($tag, $attr) {
		if(!empty($_POST[$attr['name']]) && $_POST[$attr['name']] == $attr['this']) {
			return $attr['return'];
		}
		else {
			return "";
		}
	}
	
	public function parser_selected($tag, $attr) {
		if(!empty($_POST[$attr['name']]) && $_POST[$attr['name']] == $attr['this']) {
			return $attr['return'];
		}
		else {
			return "";
		}
	}
	
	public function parser_posted($tag, $attr) {
		if(!empty($_POST[$attr['name']])) {
			return $_POST[$attr['name']];
		}
		else {
			return "";
		}
	}

}

?>