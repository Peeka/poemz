<?php

class parser_admin {

	private $item = array();
	private $subitem = array();

	function __construct() {
	
	}
	
	public function parser_methods($tag, $attr) {
		$html = "";
		@$methods = mysql_query("SELECT * FROM admin_taggar_methods WHERE parent_tag = '{$attr['parent']}'");
		while($method = mysql_fetch_array($methods)) {
			$this->subitem = $method;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_tagg($tag, $attr) {
		$html = "";
		@$taggar = mysql_query("SELECT * FROM admin_taggar WHERE id = '{$attr['id']}'");
		while($tagg = mysql_fetch_array($taggar)) {
			$this->item = $tagg;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_taggar($tag, $attr) {
		$html = "";
		@$taggar = mysql_query("SELECT * FROM admin_taggar ORDER BY `name` DESC");
		while($tagg = mysql_fetch_array($taggar)) {
			$this->item = $tagg;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->subitem[$attr['get']]) > $attr['cut']) {
				return substr($this->subitem[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->subitem[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

}

?>