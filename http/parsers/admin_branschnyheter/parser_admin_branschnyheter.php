<?php

class parser_admin_branschnyheter {

	private $item = array();
	private $subitem = array();
	private $rurl = array();
	private $category = "";
	private $message = "";
	private $has_images=false;
	private $has_movies=false;
    private $loaded_img=0;
    private $loaded_movie=0;
	private $editing_news = false;
	

	function __construct() {
		@$this->rurl = $GLOBALS['page']->get('rurl');
	
		if(!empty($this->rurl[3])) {
			$this->item = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '". $this->rurl[3] ."'"));
		#	$this->image_count = mysql_fetch_array(mysql_query("SELECT count(branschnyheter_id) as image_count FROM arbetsgivare_branschnyheter_images WHERE active='1' and branschnyheter_id='". $this->rurl[3] ."';"));
			$this->movie_count = mysql_fetch_array(mysql_query("SELECT count(branschnyheter_id) as movie_count FROM arbetsgivare_branschnyheter_movies WHERE active='1' and branschnyheter_id='". $this->rurl[3] ."';"));
			$this->editing_news = true;
		}
		
		if(isset($_GET['delete_image'])) {
			@$image = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter_images_2 WHERE branschnyheter_id = '{$_GET['branschnyheter_id']}'"));
			if(file_exists(INTERNAL_PATH."/public/images/branschnyheter/". $image[$_GET['delete_image']])) {
				unlink(INTERNAL_PATH."/public/images/branschnyheter/". $image[$_GET['delete_image']]);
				mysql_query("UPDATE arbetsgivare_branschnyheter_images_2 SET `{$_GET['delete_image']}` = '' WHERE branschnyheter_id = '{$_GET['branschnyheter_id']}'");
			}
		}
	}
	
	public function parser_check_delete($tag, $attr) {
		if($this->rurl[2] == "delete") {
			@$nyhet = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '". $this->rurl[3] ."'"));
			
			if(!empty($nyhet['image_big_1'])) {
				unlink(INTERNAL_PATH."/public/images/branschnyheter/{$nyhet['image_big_1']}");
			}
			if(!empty($nyhet['image_thumb_1'])) {
				unlink(INTERNAL_PATH."/public/images/branschnyheter/{$nyhet['image_thumb_1']}");
			}
			if(!empty($nyhet['image_big_2'])) {
				unlink(INTERNAL_PATH."/public/images/branschnyheter/{$nyhet['image_big_2']}");
			}
			
			mysql_query("DELETE FROM arbetsgivare_branschnyheter WHERE id = '". $this->rurl[3] ."'");
			mysql_query("DELETE FROM arbetsgivare_branschnyheter_images_2 WHERE branschnyheter_id = '". $this->rurl[3] ."'");
			
		}
		header("Location: /admin/branschnyheter/branschnyheter");
	}
	
	public function parser_check_search($tag, $attr) {
		$html = "";
		@$results = mysql_query("SELECT id,title FROM arbetsgivare_branschnyheter WHERE title LIKE '%{$_POST['branschnyheter_search']}%' ORDER BY added DESC");
		
		while($result = mysql_fetch_array($results)) {
			$this->item = $result;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}

	return $html;
	}

	public function parser_check_edit($tag, $attr) {
	
		if(isset($_POST['branschnyheter_edit'])) {
			$id = $this->rurl[3];

            if (isset($_POST['branschnyheter_show_company'])) $show_company_info = "yes";
            else $show_company_info = "no";


         //   var_dump($_POST);
			mysql_query("UPDATE arbetsgivare_branschnyheter SET
								company_id = '{$_POST['branschnyheter_company']}',
								account_id = '0',
								title = '{$_POST['branschnyheter_title']}',
								breadcrumb = '{$_POST['branschnyheter_breadcrumb']}',
								text_1 = '{$_POST['branschnyheter_text1']}',
								text_2 = '{$_POST['branschnyheter_text2']}',
								status = 'accepted',
								url_friendly = '{$url_friendly}',
                                show_company_info = '". $show_company_info ."' 
								WHERE id = '{$id}'");

            $this->addMiniatureImage($_FILES,$id);
            $this->addImageBig($_FILES,$id);
            $this->addImageBig2($_FILES,$id);

            if (isset($_POST['branschnyheter_movie_link']) && ($_POST['branschnyheter_movie_link'] != "")) {
               $this->updateMovies($id);

               $this->addMovie($_POST['branschnyheter_movie_link'],$_POST['branschnyheter_movie_text'],$_POST['branschnyheter_movie_location'],$id);
            }

			$this->message = "Branschnyheten har redigerats.";
			//$this->item = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '$id'"));

			header("Location: /admin/branschnyheter/edit/$id");
		}
	}

	public function parser_check_add($tag, $attr) {

		if(isset($_POST['branschnyheter_submit']) || isset($_POST['branschnyheter_publish'])) {

            if (isset($_POST['branschnyheter_submit'])) {
                 $status = "saved";
            } else if (isset($_POST['branschnyheter_publish'])) {
                 $status = "published";
            }

            if (isset($_POST['branschnyheter_show_company'])) $show_company_info = "yes";
            else $show_company_info = "no";

			mysql_query("INSERT INTO arbetsgivare_branschnyheter SET
								company_id = '{$_POST['branschnyheter_company']}',
								account_id = '0',
								added = '". time() ."',
								title = '{$_POST['branschnyheter_title']}',
								breadcrumb = '{$_POST['branschnyheter_breadcrumb']}',
								text_1 = '{$_POST['branschnyheter_text1']}',
								text_2 = '{$_POST['branschnyheter_text2']}',
								status = '". $status ."',
								url_friendly = '{$url_friendly}',
                                show_company_info = '". $show_company_info ."'");

            //get the inserted news id
            $id = mysql_insert_id();
			
			mysql_query("INSERT INTO arbetsgivare_branschnyheter_images_2 SET
								branschnyheter_id = '{$id}',
								added = '". time() ."', 
								active = 1
								");

            $this->addMiniatureImage($_FILES,$id);
            $this->addImageBig($_FILES,$id);
            $this->addImageBig2($_FILES,$id);

            if (isset($_POST['branschnyheter_movie_link']) && ($_POST['branschnyheter_movie_link'] != "")) {
               $this->addMovie($_POST['branschnyheter_movie_link'],$_POST['branschnyheter_movie_text'],$_POST['branschnyheter_movie_location'],$id);
            }

			$this->message = "Branschnyheten har lagts till.";

			header("Location: /admin/branschnyheter/edit/$id");
		}
	}

	public function parser_message($tag, $attr) {
		if(isset($attr['check'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $this->message;
		}
	}

	public function fix_files_array($arr) {
		$i = 0;
		foreach($arr as $key => $all) {
			foreach($all as $i => $val) {
				$new[$i][$key] = $val;   
			}   
		}
		return $new;
	}
	
	public function url_friendly($string) {
		$url = str_replace("ö","o",strtolower($string));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("ä","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->subitem[$attr['get']]) > $attr['cut']) {
				return substr($this->subitem[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->subitem[$attr['get']];
			}
		}
		else {
			return "";
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

    public function parser_get_show_company_info($tag, $attr)
    {
        if (isset($this->item['show_company_info']) && $this->item['show_company_info'] == "yes") {
            return "checked=\"checked\"";
        } else return "";

    }
	
	public function parser_check_article_exists($tag, $attr)
	{
		if (isset($this->item['id'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

    public function parser_bild($tag, $attr) {
		@$check = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter_images_2 
													WHERE branschnyheter_id = '{$attr['branschnyheter_id']}'"));
		return $check[$attr['type']];
	}

    public function parser_has_image($tag, $attr) {
		@$check = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter_images_2 
													WHERE branschnyheter_id = '{$attr['branschnyheter_id']}'"));

		if( !empty($check[$attr['type']]) && $attr['state'] == "true" ) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if( empty($check[$attr['type']]) && $attr['state'] == "false" ) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

    public function parser_has_image2($tag, $attr) {
        if(((int)$this->image_count['image_count'] != 0) && $attr['state'] == "true" ) {
            if ($this->loaded_img == 0) $this->loaded_img = $this->subitem['rnum'];

       		return $GLOBALS['parser']->parse($tag['innerhtml']);
		} else if ( $attr['state'] == "false" && ((int)$this->image_count['image_count'] == 0)) {
           return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	private function addImage($files,$news_id,$image_titles)
    {
		
        @$allowedExts = array("jpg", "jpeg", "gif", "png");
        for($i=0;$i<sizeOf($files["branschnyheter_bild"]["name"]);$i++){
        @$extension = end(explode(".", $files["branschnyheter_bild"]["name"][$i]));
		
            if(in_array($extension, $allowedExts)) {
               //get next incremental number
              $next_rnum = mysql_query("SELECT (MAX(rnum)+1) as next_rnum FROM arbetsgivare_branschnyheter_images WHERE active='1' and branschnyheter_id='{$news_id}';");
			  
              $next_rnum_value = mysql_fetch_array($next_rnum);

			  if ($next_rnum_value['next_rnum'] == NULL) $next_rnum_value['next_rnum'] = 1;

              $newNameBig = "branschnyheter_".$news_id."_".$next_rnum_value['next_rnum']."_big";
              $newNameThumb = "branschnyheter_".$news_id."_".$next_rnum_value['next_rnum']."_thumb";


              if(file_exists(INTERNAL_PATH."/public/images/branschnyheter/". $newName . $extension)) {
                    unlink(INTERNAL_PATH."/public/images/branschnyheter/". $newName . $extension);
              }

              $newFileBig = $newNameBig .".". $extension;
              $newFileThumb = $newNameThumb .".". $extension;

              // Create the big image
              if(move_uploaded_file($files["branschnyheter_bild"]["tmp_name"][$i], INTERNAL_PATH."/public/images/branschnyheter/". $newFileBig)) {

				image::resize(INTERNAL_PATH."/public/images/branschnyheter/". $newFileBig,
									INTERNAL_PATH."/public/images/branschnyheter/". $newFileBig,
									471,
									360);

				// Create the thumb
				image::resize(INTERNAL_PATH."/public/images/branschnyheter/". $newFileBig,
							INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb,
							150,
							100);

				mysql_query("INSERT INTO arbetsgivare_branschnyheter_images (branschnyheter_id,title,image_big,image_thumb,added,active) VALUES ('".$news_id."','".$image_titles[$i]."','".$newFileBig."','".$newFileThumb."','". time() ."','1')");
              }
            }
        }
    }

	public function parser_get_news_images($tag, $attr){

        if(isset($attr['id'])){
            $limit = (isset($attr['limit'])) ? "LIMIT ".$attr['limit'] : '';
            $min   = (isset($attr['min'])) ? "rnum > ".$attr['min'] : '1';
            $loaded  = ($this->loaded_img != 0) ? "rnum <> ".$this->loaded_img : '1';

            $images = mysql_query("SELECT * FROM arbetsgivare_branschnyheter_images WHERE branschnyheter_id='{$attr['id']}' and {$min} and {$loaded} and active='1' ORDER BY rnum {$limit}");
			
            $counter=2;
            while($image = mysql_fetch_array($images)) {
    		    $this->has_images = true;
                $image['rownumber']=$counter;
                $counter++;
    			$this->subitem = $image;
    			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
    		}
            return $html;
        }
    }

    private function addImageBig2($files,$news_id){

		if(isset($files["branschnyheter_image_big_2"])) {
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $files["branschnyheter_image_big_2"]["name"]));
			
			if(in_array($extension, $allowedExts)) {

			  $newNameThumb = "branschnyheter_".$news_id."_image_big_2";

			  if(file_exists(INTERNAL_PATH."/public/images/branschnyheter/". $newNameThumb . $extension)) {
					unlink(INTERNAL_PATH."/public/images/branschnyheter/". $newNameThumb . $extension);
			  }

			  $newFileThumb = $newNameThumb .".". $extension;

			  // Create the big image
			  if(move_uploaded_file($files["branschnyheter_image_big_2"]["tmp_name"], INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb)) {

				  // Create the thumb
				  image::resize(INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb,
							INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb,
							471,
							360);
							
				  mysql_query("UPDATE arbetsgivare_branschnyheter_images_2 SET image_big_2 = '".$newFileThumb."' WHERE branschnyheter_id = '{$news_id}'");
			  }
			}
		}
    }

    private function addImageBig($files,$news_id){

		if(isset($files["branschnyheter_image_big"])) {
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $files["branschnyheter_image_big"]["name"]));
			
			if(in_array($extension, $allowedExts)) {

			  $newNameThumb = "branschnyheter_".$news_id."_image_big";

			  if(file_exists(INTERNAL_PATH."/public/images/branschnyheter/". $newNameThumb . $extension)) {
					unlink(INTERNAL_PATH."/public/images/branschnyheter/". $newNameThumb . $extension);
			  }

			  $newFileThumb = $newNameThumb .".". $extension;

			  // Create the big image
			  if(move_uploaded_file($files["branschnyheter_image_big"]["tmp_name"], INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb)) {

				  // Create the thumb
				  image::resize(INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb,
							INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb,
							471,
							360);
							
				  mysql_query("UPDATE arbetsgivare_branschnyheter_images_2 SET image_big = '".$newFileThumb."' WHERE branschnyheter_id = '{$news_id}'");
			  }
			}
		}
    }

    private function addMiniatureImage($files,$news_id){

		if(isset($files["branschnyheter_image_thumb"])) {
			@$allowedExts = array("jpg", "jpeg", "gif", "png");
			@$extension = end(explode(".", $files["branschnyheter_image_thumb"]["name"]));
			
			if(in_array($extension, $allowedExts)) {

			  $newNameThumb = "branschnyheter_".$news_id."_thumb";

			  if(file_exists(INTERNAL_PATH."/public/images/branschnyheter/". $newNameThumb . $extension)) {
					unlink(INTERNAL_PATH."/public/images/branschnyheter/". $newNameThumb . $extension);
			  }

			  $newFileThumb = $newNameThumb .".". $extension;

			  // Create the big image
			  if(move_uploaded_file($files["branschnyheter_image_thumb"]["tmp_name"], INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb)) {

				  // Create the thumb
				  image::resize(INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb,
							INTERNAL_PATH."/public/images/branschnyheter/". $newFileThumb,
							150,
							100);
							
				  mysql_query("UPDATE arbetsgivare_branschnyheter_images_2 SET image_thumb = '".$newFileThumb."' WHERE branschnyheter_id = '{$news_id}'");
			  }
			}
		}
    }
	
	public function parser_check_news_edit($tags, $attr) {
		
		if (isset($this->editing_news) && $this->editing_news) {
			return "edit/".$this->rurl[3];
		} else return "add";
	
	}
	
	private function updateImage($rnum,$branschnyheter_id)
    {
		$nyhet = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter_images WHERE branschnyheter_id = '". $branschnyheter_id ."' and rnum='". $rnum ."'"));
		if ((!empty($nyhet['image_thumb'])) && (!empty($nyhet['image_big']))) {
		
	
			unlink(INTERNAL_PATH."/public/images/branschnyheter/{$nyhet['image_thumb']}");
			unlink(INTERNAL_PATH."/public/images/branschnyheter/{$nyhet['image_big']}");
			
			mysql_query("UPDATE arbetsgivare_branschnyheter_images
						  SET active = '0'
						  WHERE branschnyheter_id='".$branschnyheter_id."' and rnum='".$rnum."'");
		}
    }

   private function updateMiniatureImage($branschnyheter_id)
    {
		@$nyhet = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter WHERE id = '". $branschnyheter_id ."'"));
		if ((!empty($nyhet['image_mini_big'])) && (!empty($nyhet['image_mini_thumb']))) {

			unlink(INTERNAL_PATH."/public/images/branschnyheter/{$nyhet['image_mini_big']}");
			unlink(INTERNAL_PATH."/public/images/branschnyheter/{$nyhet['image_mini_thumb']}");

			mysql_query("UPDATE arbetsgivare_branschnyheter
						  SET image_mini_big = '', image_mini_thumb = ''
						  WHERE id='".$branschnyheter_id."'");
		}
    }

   private function updateMovies($branschnyheter_id)
   {
        mysql_query("UPDATE arbetsgivare_branschnyheter_movies SET active='0' WHERE branschnyheter_id = '". $branschnyheter_id ."'");
   }

   private function addMovie($movies_link,$movies_text,$movies_location,$branschnyheter_id)
   {
        for($i=0;$i<sizeOf($movies_link);$i++){
          @$movie = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_branschnyheter_movies
                                                  WHERE link='".$movies_link[$i]."' and title='".$movies_text[$i]."' and location='".$movies_location[$i]."'"));

          if (!empty($movie) && ($movie['active'] == "0")){
             mysql_query("UPDATE arbetsgivare_branschnyheter_movies SET active='1' WHERE rnum='".$movie['rnum']."'");
          } else {
           mysql_query("INSERT INTO arbetsgivare_branschnyheter_movies (branschnyheter_id,link,title,location,added,active)
                       VALUES ('".$branschnyheter_id."','".$movies_link[$i]."','".$movies_text[$i]."','".$movies_location[$i]."','".time()."','1')");
          }

        }
   }

   	public function parser_get_news_movies($tag, $attr){

        $this->subitem = null;
        if(isset($attr['id'])){
            $limit = (isset($attr['limit'])) ? "LIMIT ".$attr['limit'] : '';
            $min   = (isset($attr['min'])) ? "rnum > ".$attr['min'] : '1';
            $loaded  = ($this->loaded_movie != 0) ? "rnum <> ".$this->loaded_movie : '1';

            $movies = mysql_query("SELECT * FROM arbetsgivare_branschnyheter_movies WHERE branschnyheter_id='{$attr['id']}' and {$min} and {$loaded} and active='1' ORDER BY rnum {$limit}");

            $counter=2;
            while($movie = mysql_fetch_array($movies)) {
    		    $this->has_movies = true;
                $movie['rownumber']=$counter;
                $counter++;
    			$this->subitem = $movie;
                if ($this->loaded_movie == 0) $this->loaded_movie = $this->subitem['rnum'];
    			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
    		}
            return $html;
        }
    }

}

?>