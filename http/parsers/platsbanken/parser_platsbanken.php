<?php

class parser_platsbanken {

	private $item = array();
	private $subitem = array();
	private $image = array();
	private $where = "";

	function __construct() {
		$redirect = false;
		$reg = "_all";
		$cat = "_all";
		$this->parser_get_info("",array("get"=>"regions"));
		$this->parser_get_info("",array("get"=>"categories"));
		
		// Convert the post to our url friendly url instead
		$rurl = $GLOBALS['page']->get('rurl');
		$redirect = false;
		$reg = "_all";
		$cat = "_all";
		$page = isset($_POST['pager_button']) ? $_POST['pager_button'] : 1 ;
		
		// CHECK POST
		// REGION
		if(isset($_POST['platsbanken_regions']) && !empty($_POST['platsbanken_regions']) && $_POST['platsbanken_regions'] != '_all') {
			$reg = mysql_fetch_array(mysql_query("SELECT url_friendly FROM regions WHERE id = '{$_POST['platsbanken_regions']}'"));
			$reg = $reg['url_friendly'];
			$redirect = true;
		}
		// CATEGORY
		if(isset($_POST['platsbanken_categories']) && !empty($_POST['platsbanken_categories']) && $_POST['platsbanken_categories'] != '_all') {
			$cat = mysql_fetch_array(mysql_query("SELECT url_friendly FROM arbetsgivare_job_categories WHERE id = '{$_POST['platsbanken_categories']}'"));
			$cat = $cat['url_friendly'];
			$redirect = true;
		}
		
		// CHECK GET
		// REGION
		if(isset($rurl[3]) && !empty($rurl[3]) && $rurl[3] != '_all') {
			$reg = $rurl[3];
		}
		// CATEGORY
		if(isset($rurl[2]) && !empty($rurl[2]) && $rurl[2] != '_all') {
			$cat = $rurl[2];
		}
		
		// Check if we want to load a category
		$name = $GLOBALS['page']->index(2);
		
		if(isset($name) && !empty($name) && $name != "_all") {
			$name = $this->url_friendly($name);
			$cat1 = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_job_categories WHERE url_friendly = '{$name}'"));
			if($cat1['id'] > 0) {
				$this->sql_category = " category = '{$cat1['id']}' AND ";
			}
		}
		else {
			$this->sql_category = (isset($_POST['platsbanken_categories']) && !empty($_POST['platsbanken_categories']) && $_POST['platsbanken_categories'] != "_all") ? " category = '{$_POST['platsbanken_categories']}' AND " : "";
		}
		
		// Check if we want to load a region
		$name = $GLOBALS['page']->index(3);
		
		if(isset($name) && !empty($name) && $name != "_all") {
			$name = $this->url_friendly($name);
			$reg1 = mysql_fetch_array(mysql_query("SELECT id FROM regions WHERE url_friendly = '{$name}'"));
			if($reg1['id'] > 0) {
				$this->sql_region = " AND region = '{$reg1['id']}'";
			}
		}
		else {
			$this->sql_region = (isset($_POST['regions']) && !empty($_POST['regions']) && $_POST['regions'] != "_all") ? " AND region = '{$_POST['regions']}'" : "";
		}

		if(isset($_POST['pager_change']) || isset($_POST['platsbanken_submit'])) {
			header("Location: /platsbanken/arbetssokande/{$cat}/{$reg}/{$page}");
			exit;
		}

		if($rurl[0] == "platsbanken" && empty($rurl[2])) {
			header("Location: /platsbanken/arbetssokande/_all/_all/1");
			exit;
		}
	}
	
	public function parser_selected_region($tag, $attr) {
		$reg = $GLOBALS['page']->index(3);
		$reg = mysql_fetch_array(mysql_query("SELECT id FROM regions WHERE url_friendly = '{$reg}'"));
		if(intval($attr['id']) == intval($reg['id'])) {
			return $GLOBALS['parser']->parse($attr['return']);
		}
	}
	
	public function parser_selected_category($tag, $attr) {
		$cat = $GLOBALS['page']->index(2);
		$cat = mysql_fetch_array(mysql_query("SELECT id FROM arbetsgivare_job_categories WHERE url_friendly = '{$cat}'"));
		if(intval($attr['id']) == intval($cat['id'])) {
			return $GLOBALS['parser']->parse($attr['return']);
		}
	}
	
	public function parser_profile_image($tag, $attr) {
		// check if our image status is 3 (complete), otherwise load the default user_no_image.jpg
		$id = (isset($attr['account_id']) && !empty($attr['account_id'])) ? $attr['account_id'] : $this->item['account_id'];
		$bild = mysql_fetch_array(mysql_query("SELECT * FROM arbetssokande_bild WHERE account_id = '{$id}'"));
		
		if($bild['step'] == 3) {
			return "user_{$id}.jpg";
		}
		else {
			return "user_no_image.jpg";
		}
	}
	
	public function parser_arbetssokande($tag, $attr) {
		$html = "";
		
		$info = mysql_query("SELECT * FROM arbetssokande_profil ORDER BY added DESC");
		
		while($arbetssokande = mysql_fetch_array($info)) {
			$this->item = $arbetssokande;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_load_jobb_annons($tag, $attr) {
		$job = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE id = '{$attr['id']}'"));
		$company = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$job['company_id']}'"));
		$this->item = $job;
		$this->subitem = $company;
	}
	
	public function parser_get_category($tag, $attr) {
		$category = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_job_categories WHERE id = '{$attr['id']}'"));
		return (isset($category[$attr['get']]) && !empty($category[$attr['get']])) ? $category[$attr['get']] : "Ingen kategori";
	}
	
	public function parser_get_region($tag, $attr) {
		$region = mysql_fetch_array(mysql_query("SELECT * FROM regions WHERE id = '{$attr['id']}'"));
		return (isset($region[$attr['get']]) && !empty($region[$attr['get']])) ? $region[$attr['get']] : "Ingen region";
	}
	
	public function parser_total_items($tag, $attr) {
		$all = mysql_fetch_array(mysql_query("SELECT COUNT(id) AS count FROM arbetsgivare_jobb_annonser{$this->where}"));
		return $all['count'];
	}
	
	public function parser_jobbannonser($tag, $attr) {
		$html = "";
		
		// Lets create the pager limit
	//	$limit = (isset($attr['limit'])) ? $attr['limit'] : $GLOBALS['pager']->limit();
		$limit = (isset($attr['limit'])) ? $attr['limit'] : 1000000;
		
		if(isset($attr['show_all'])) {
			if(isset($attr['exclude'])) {
				$info = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE active = '1' AND id != '{$attr['exclude']}' ORDER BY added DESC LIMIT 1000000");
			}
			else {
				$info = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE active = '1' ORDER BY added DESC LIMIT 1000000");
			}
		}
		else {
			if(isset($attr['exclude'])) {
				$info = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE{$this->sql_category} active = '1'{$this->sql_region} AND id != '{$attr['exclude']}' ORDER BY added DESC LIMIT {$limit}");
			}
			else {
				$info = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE{$this->sql_category} active = '1'{$this->sql_region} ORDER BY added DESC LIMIT {$limit}");
			}
		}
		
		while($jobbannons = mysql_fetch_array($info)) {
			$this->item = $jobbannons;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_get_image($tag, $attr) {
		$html = "";
		$limit_start = (isset($attr['index'])) ? $attr['index'] : 0;
		@$images = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser_images WHERE jobb_annonser_id = '{$this->item['id']}' AND active = '1' ORDER BY rnum ASC LIMIT {$limit_start},1");
		
		while($image = mysql_fetch_array($images)) {
			$this->subitem = $image;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
			
		if(empty($html)) {
			$html .= '<img style="text-align:center;" width="150" title="ikon" src="/public/images/jobbannonser/'. $this->item['image_big'] .'" border="0" />';
		}
		else {
			$html .= '<img style="text-align:center;" width="150" title="ikon" src="/public/images/jobbannonser/'. $this->item['image_big'] .'" border="0" />';
		}
	return $html;
	}
	
	public function parser_categories($tag, $attr) {
		$html = "";
		foreach($this->info['categories'] as $category) {
			$this->item = $category;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_regions($tag, $attr) {
		$html = "";
		foreach($this->info['regions'] as $region) {
			$this->item = $region;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	return $html;
	}
	
	public function parser_get_info($tag, $attr) {
		if($attr['get'] == "regions") {
			@$info = mysql_query("SELECT * FROM regions ORDER BY type DESC, name ASC");
			while($region = mysql_fetch_array($info)) {
				$this->info['regions'][$region['id']] = $region;
			}
		}
		if($attr['get'] == "categories") {
			@$info = mysql_query("SELECT * FROM arbetsgivare_job_categories ORDER BY name ASC");
			while($category = mysql_fetch_array($info)) {
				$this->info['categories'][$category['id']] = $category;
			}
		}
	}
	
	public function parser_get_company($tag, $attr) {
		@$info = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE id = '{$attr['company_id']}'"));
		// Fix links
		if(isset($attr['link'])) {
			if(!strstr($info[$attr['get']],"http://")) {
				$info[$attr['get']] = "http://{$info[$attr['get']]}";
			}
		}
		return $info[$attr['get']];
	}
	
	public function parser_item($tag, $attr) {	
		$get = "";
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Cut the string
			if(isset($attr['cut']) && !empty($attr['cut'])) {
				if(isset($attr['add'])) {
					return $GLOBALS['text']->cut($get, intval($attr['cut'])).$attr['add'];
				}
				else {
					return $GLOBALS['text']->cut($get, intval($attr['cut']));
				}
			}
		}
		return $get;
	}
	
	public function parser_subitem($tag, $attr) {
		$get = "";
		if(!empty($this->subitem[$attr['get']])) {
			$get = strip_tags($this->subitem[$attr['get']]);
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			else {
				$get = $this->subitem[$attr['get']];
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				return $GLOBALS['text']->cut($get, intval($attr['cut'])).$attr['add'];
			}
			else {
				return $get;
			}
		}
		else {
			return "";
		}
	}
	
	public function url_friendly($string) {
		$url = str_replace("ö","o",strtolower($string));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = str_replace("ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}
	
	public function parser_info($tag, $attr) {
		// Posted values
		if(isset($attr['posted_first']) && !empty($attr['posted_first']) && isset($_POST["{$attr['get']}_{$attr['name']}"]) && !empty($_POST["{$attr['get']}_{$attr['name']}"])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $_POST["{$attr['get']}_{$attr['name']}"]) {
					return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
				}
				else {
					return "";
				}
			}
			else {
				return (isset($attr['return'])) ? $attr['return'] : $_POST["{$attr['get']}_{$attr['name']}"];
			}
		} // Stored values
		else if(isset($this->info[$attr['get']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['name']]) || 
		isset($attr['index']) && isset($this->info[$attr['get']][$attr['index']][$attr['name']]) && !empty($this->info[$attr['get']][$attr['index']][$attr['name']])) {
			if(isset($attr['value'])) {
				if($attr['value'] == $this->info[$attr['get']][$attr['name']] || isset($attr['index']) && $attr['value'] == $this->info[$attr['get']][$attr['index']][$attr['name']]) {
					if(isset($attr['index'])) {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
					}
					else {
						return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
					}
				}
				else {
					return "";
				}
			}
			else {
				if(isset($attr['index'])) {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['index']][$attr['name']];
				}
				else {
					return (isset($attr['return'])) ? $attr['return'] : $this->info[$attr['get']][$attr['name']];
				}
			}
		}
		else {
			return "";
		}
	}

}

?>