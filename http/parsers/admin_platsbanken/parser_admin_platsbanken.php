<?php

class parser_admin_platsbanken {

    private $has_images=false;
    private $loaded_img=0;
    private $loaded_contact=0;
    private $hasContacts = false;

    function __construct() {
        @$id = $GLOBALS['parser']->parse("{page:rurl index='3'/}");
        @$action = $GLOBALS['parser']->parse("{page:rurl index='2'/}");

		if (($id > 0) && ($action != 'radera')) {
			      $this->item = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE id = '{$id}' and active='1'"));
            $this->maincontact = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_jobb_annonser_contacts WHERE jobb_annonser_id = '{$id}' and active='1' ORDER BY rnum ASC LIMIT 1"));
            $this->image_count = mysql_fetch_array(mysql_query("SELECT count(jobb_annonser_id) as image_count FROM arbetsgivare_jobb_annonser_images WHERE active='1' and jobb_annonser_id='{$id}';"));
		}

        if (($action == 'radera') && ((int)$id > 0)) {
            mysql_query("UPDATE arbetsgivare_jobb_annonser SET active='0' WHERE id='{$id}';");
        }

        if (isset($action) && ($action=="email")){
            if (isset($this->item['contact_mail']) && ($this->item['contact_mail'] != '')){
              if (filter_var($this->item['contact_mail'], FILTER_VALIDATE_EMAIL)){
                  @$email_type = $GLOBALS['parser']->parse("{page:rurl index='4'/}");

                  if ($email_type == "annons_website"){
                      $this->sendEmail($id,$this->item['contact_mail'],6);
                  } else if ($email_type == "annons_facebook"){
                      $this->sendEmail($id,$this->item['contact_mail'],7);
                  }
              }else{
                 $this->message = "E-post är inte giltig för annons <b>".$this->item['title']."</b>";
              }

            }
            header("Location: /admin/platsbanken/alla-platsannonser");
			exit;
        }

        if (isset($_POST['platsannons_publish']) && ($_POST['job_edit'] == "")){

            @$last_day = strtotime($_POST['platsannons_date']);
			$last_day = "$last_day";
            @$job_category = (isset($_POST['platsannons_category']) && ($_POST['platsannons_category'] != '_empty') ) ? $_POST['platsannons_category'] : '';

            mysql_query("INSERT INTO arbetsgivare_jobb_annonser
                        SET company_id = '{$_POST['platsannons_company']}',
                            account_id = '". account::user('id') ."',
                            category_id = '". $job_category ."',
                            title = '{$_POST['platsannons_title']}',
                            intro = '{$_POST['platsannons_intro']}',
                            description = '{$_POST['platsannons_body']}',
                            company_name = '{$_POST['platsannons_company_name']}',
                            org_number = '{$_POST['platsannons_company_vat_no']}',
                            street = '{$_POST['platsannons_company_street']}',
                            zip = '{$_POST['platsannons_company_postno']}',
                            city = '{$_POST['platsannons_company_city']}',
                            state = '{$_POST['platsannons_company_state']}',
                            contact_mail = '{$_POST['platsannons_company_email']}',
                            website = '{$_POST['platsannons_company_website']}',
                            telephone = '{$_POST['platsannons_company_telephone']}',
                            last_day = '{$last_day}',
                            last_day_comment = '{$_POST['platsannons_date_comment']}',
                            added = '". time() ."',
                            extern_website = '{$_POST['platsannons_contact']}',
                            status = 'waiting'");

            $jobs_annonser_id = mysql_insert_id();

            if (isset($_POST['platsannons_contact_name'])) {
                $this->createContacts($_POST['platsannons_contact_name'],$_POST['platsannons_contact_phone'],$_POST['platsannons_contact_email'],$jobs_annonser_id);
            }

            //add logo
            if (isset($_FILES['platsannons_logo'])){
                $this->addLogo($_FILES,$jobs_annonser_id);
            }

            //add image
            if (isset($_FILES['platsannons_image'])){
                $this->addImage($_FILES,$jobs_annonser_id);
            }

            header("Location: /admin/platsbanken/redigera/".$jobs_annonser_id);
			exit;


        } else if (isset($_POST['platsannons_publish']) && ($_POST['job_edit'] != "")){
		
		//	exit($_POST['platsannons_date']);

			$last_day = strtotime( $_POST['platsannons_date'] );
			$last_day = "$last_day";
			
            @$job_category = (isset($_POST['platsannons_category']) && ($_POST['platsannons_category'] != '_empty') ) ? $_POST['platsannons_category'] : '';

            mysql_query("UPDATE arbetsgivare_jobb_annonser
                        SET company_id = '{$_POST['platsannons_company']}',
                            account_id = '". account::user('id') ."',
                            category_id = '". $job_category ."',
                            title = '{$_POST['platsannons_title']}',
                            intro = '{$_POST['platsannons_intro']}',
                            description = '{$_POST['platsannons_body']}',
                            company_name = '{$_POST['platsannons_company_name']}',
                            org_number = '{$_POST['platsannons_company_vat_no']}',
                            street = '{$_POST['platsannons_company_street']}',
                            zip = '{$_POST['platsannons_company_postno']}',
                            city = '{$_POST['platsannons_company_city']}',
                            state = '{$_POST['platsannons_company_state']}',
                            contact_mail = '{$_POST['platsannons_company_email']}',
                            website = '{$_POST['platsannons_company_website']}',
                            telephone = '{$_POST['platsannons_company_telephone']}',
                            last_day = '{$last_day}',
                            last_day_comment = '{$_POST['platsannons_date_comment']}',
                            extern_website = '{$_POST['platsannons_contact']}'
                            WHERE id='{$_POST['job_edit']}'");

            if (isset($_POST['platsannons_contact_name'])) {
                $this->updateContacts($_POST['job_edit']);
                $this->createContacts($_POST['platsannons_contact_name'],$_POST['platsannons_contact_phone'],$_POST['platsannons_contact_email'],$_POST['job_edit']);
            }

            if (isset($_POST['jobb_annons_image_radera'])){
                $this->updateImage($_POST['jobb_annons_image_radera'],$_POST['job_edit']);
            }

            if (isset($_POST['jobb_annons_logo_radera'])){
                $this->updateLogo($_POST['job_edit']);
            }
            //add logo
            if (isset($_FILES['platsannons_logo'])){
                $this->addLogo($_FILES,$_POST['job_edit']);
            }

            //add image
            if (isset($_FILES['platsannons_image'])){
                $this->addImage($_FILES,$_POST['job_edit']);
            }
            header("Location: /admin/platsbanken/redigera/".$_POST['job_edit']);
			exit;
        }
    }

    private function createContacts($contacts_name, $contacts_telephone, $contacts_email, $jobb_annonser_id)
    {
        for($i=0;$i<sizeOf($contacts_name);$i++){
            mysql_query("INSERT INTO arbetsgivare_jobb_annonser_contacts
                        SET jobb_annonser_id = '". $jobb_annonser_id ."',
        					name = '". $contacts_name[$i] ."',
        					telephone = '". $contacts_telephone[$i] ."',
        					contact_email = '". $contacts_email[$i] ."',
                            active = '1'");
        }

    }

    private function updateContacts($jobb_annonser_id)
    {
        mysql_query("UPDATE arbetsgivare_jobb_annonser_contacts
                      SET active = '0'
                      WHERE jobb_annonser_id='".$jobb_annonser_id."'");
    }

    private function updateImage($rnum,$jobb_annonser_id)
    {
        mysql_query("UPDATE arbetsgivare_jobb_annonser_images
                      SET active = '0'
                      WHERE jobb_annonser_id='".$jobb_annonser_id."' and rnum='".$rnum."'");
    }

    private function updateLogo($jobb_annonser_id)
    {
        mysql_query("UPDATE arbetsgivare_jobb_annonser
                      SET logo = ''
                      WHERE id='".$jobb_annonser_id."'");
    }

    private function addLogo($files,$jobb_annonser_id)
    {
      @$allowedExts = array("jpg", "jpeg", "gif", "png");
      @$extension = end(explode(".", $files["platsannons_logo"]["name"]));

      if(in_array($extension, $allowedExts)) {
        $newName = "jobbannons_".$jobb_annonser_id."_thumb";

        if(file_exists(INTERNAL_PATH."/public/images/jobbannonser/". $newName . $extension)) {
              unlink(INTERNAL_PATH."/public/images/jobbannonser/". $newName . $extension);
        }

        $newFileThumb = $newName .".". $extension;

        // Create the big image
        if(move_uploaded_file($files["platsannons_logo"]["tmp_name"], INTERNAL_PATH."/public/images/jobbannonser/". $newFileThumb)) {
            image::resize(INTERNAL_PATH."/public/images/jobbannonser/". $newFileThumb,
                          INTERNAL_PATH."/public/images/jobbannonser/". $newFileThumb,
                          170,
                          200);

            mysql_query("UPDATE arbetsgivare_jobb_annonser SET logo = '{$newFileThumb}', image_thumb = '{$newFileThumb}' WHERE id = '{$jobb_annonser_id}'");
        }
      }

    }

    private function removeImage()
    {
      if(isset($_POST['companies_bild_radera'])) {
				@$image = mysql_fetch_array(mysql_query("SELECT image_big, image_thumb FROM arbetsgivare_companies WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='3'/}") ."'"));
				unlink(INTERNAL_PATH."/public/images/companies/". $image['image_big']);
				unlink(INTERNAL_PATH."/public/images/companies/". $image['image_thumb']);
				mysql_query("UPDATE arbetsgivare_companies SET image_big = '', image_thumb = '' WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='3'/}") ."'");
			}
    }

    private function removeLogo()
    {
      if(isset($_POST['companies_bild_radera'])) {
				@$image = mysql_fetch_array(mysql_query("SELECT image_big, image_thumb FROM arbetsgivare_companies WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='3'/}") ."'"));
				unlink(INTERNAL_PATH."/public/images/companies/". $image['image_big']);
				unlink(INTERNAL_PATH."/public/images/companies/". $image['image_thumb']);
				mysql_query("UPDATE arbetsgivare_companies SET image_big = '', image_thumb = '' WHERE id = '". $GLOBALS['parser']->parse("{page:rurl index='3'/}") ."'");
			}
    }

    private function addImage($files,$jobb_annonser_id)
    {
        @$allowedExts = array("jpg", "jpeg", "gif", "png");
        for($i=0;$i<sizeOf($files["platsannons_image"]["name"]);$i++){
        @$extension = end(explode(".", $files["platsannons_image"]["name"][$i]));


            if(in_array($extension, $allowedExts)) {
               //get next incremental number
              $next_rnum = mysql_query("SELECT (MAX(rnum)+1) as next_rnum FROM arbetsgivare_jobb_annonser_images WHERE active='1' and jobb_annonser_id='{$jobb_annonser_id}';");

              $next_rnum_value = mysql_fetch_array($next_rnum);

              $newName = "jobbannons_".$jobb_annonser_id."_".$next_rnum_value['next_rnum']."_big";


              if(file_exists(INTERNAL_PATH."/public/images/jobbannonser/". $newName . $extension)) {
                    unlink(INTERNAL_PATH."/public/images/jobbannonser/". $newName . $extension);
              }

              $newFileBig = $newName .".". $extension;

              // Create the big image
              if(move_uploaded_file($files["platsannons_image"]["tmp_name"][$i], INTERNAL_PATH."/public/images/jobbannonser/". $newFileBig)) {
                  image::resize(INTERNAL_PATH."/public/images/jobbannonser/". $newFileBig,
                                INTERNAL_PATH."/public/images/jobbannonser/". $newFileBig,
                                523,
                                500);


                  mysql_query("INSERT INTO arbetsgivare_jobb_annonser_images (jobb_annonser_id,image,active) VALUES ('".$jobb_annonser_id."','".$newFileBig."','1')");
                  mysql_query("UPDATE arbetsgivare_jobb_annonser SET 
									image_big = '{$newFileBig}' 
								WHERE id = '{$jobb_annonser_id}'
								");
              }
            }
        }
    }

    public function parser_jobbannonser($tag, $attr) {
		$html = "";

		// Lets create the pager limit
	//	$limit = (isset($attr['limit'])) ? $attr['limit'] : $GLOBALS['pager']->limit();
		$limit = 1000000;
		@$info = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser WHERE active='1' ORDER BY added DESC LIMIT {$limit}");
		while($jobbannons = mysql_fetch_array($info)) {
			$this->item = $jobbannons;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}

	    return $html;
	}

    public function parser_message($tag, $attr) {
		if(isset($attr['check'])) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return $this->message;
		}
	}

    public function url_friendly($string) {
		$url = str_replace("ö","o",strtolower($string));
		$url = str_replace("Ö","o",$url);
		$url = str_replace("å","a",$url);
		$url = str_replace("Å","a",$url);
		$url = str_replace("ä","a",$url);
		$url = str_replace("Ä","a",$url);
		$url = preg_replace('/[^a-zA-Z0-9- ]/','',$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
	return $url;
	}

	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {

            if ( ($this->loaded_contact == 0) && (isset($attr['type'])) && ($attr['type'] == "contact")) {
              $this->loaded_contact = $this->subitem['rnum'];
              }

			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->subitem[$attr['get']]) > $attr['cut']) {
				return substr($this->subitem[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->subitem[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

    public function parser_contact_main($tag, $attr) {
      var_dump($this->contact_main);
		if(!empty($this->contact_main[$attr['get']])) {

			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->contact_main[$attr['get']]) > $attr['cut']) {
				return substr($this->contact_main[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->contact_main[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

    public function parser_maincontact($tag, $attr) {
		if(!empty($this->maincontact[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->maincontact[$attr['get']]) > $attr['cut']) {
				return substr($this->maincontact[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->maincontact[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

    public function parser_has_image($tag, $attr) {
        if(((int)$this->image_count['image_count'] != 0) && $attr['state'] == "true" ) {
            if ($this->loaded_img == 0) $this->loaded_img = $this->subitem['rnum'];

       		return $GLOBALS['parser']->parse($tag['innerhtml']);
		} else if ( $attr['state'] == "false" && ((int)$this->image_count['image_count'] == 0)) {
           return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

    public function parser_has_contacts($tag, $attr) {
		if( $this->hasContacts && $attr['state'] == "true" ) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if( !$this->hasContacts && $attr['state'] == "false" ) {
           return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

    public function parser_has_logo($tag, $attr) {
		if(!empty($this->item['logo']) && $attr['state'] == "true" ) {
			$this->image = $this->item['logo'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if ( $attr['state'] == "false" && empty($this->item['logo'])) {
           return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}

    public function parser_get_images($tag, $attr){

        if(isset($attr['id'])){
            $limit = (isset($attr['limit'])) ? "LIMIT ".$attr['limit'] : '';
            $min   = (isset($attr['min'])) ? "rnum > ".$attr['min'] : '1';
            $loaded  = ($this->loaded_img != 0) ? "rnum <> ".$this->loaded_img : '1';

            $images = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser_images WHERE jobb_annonser_id='{$attr['id']}' and {$min} and {$loaded} and active='1' ORDER BY rnum {$limit}");

            $counter=2;
            while($image = mysql_fetch_array($images)) {
    		    $this->has_images = true;
                $image['rownumber']=$counter;
                $counter++;
    			$this->subitem = $image;
    			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
    		}
            return $html;
        }
    }

    public function parser_set_dropdown($tag, $attr){
        if (isset($attr['set']) && isset($attr['current'])){
            if ($attr['set'] == $attr['current']) return "selected='selected'";
        }

        return "";
    }

    public function parser_get_companies($tag, $attr){
        $html = "";

		@$companies = mysql_query("SELECT * FROM arbetsgivare_companies ORDER BY date_created DESC");

		while($company = mysql_fetch_array($companies)) {
			$this->subitem = $company;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}

	    return $html;

    }

    public function parser_get_date($tag, $attr){

        if (isset($this->item['last_day'])){
          return $this->item['last_day'];
        }

        return "";
    }


    public function parser_get_contacts($tag, $attr){

        if(isset($attr['id'])){
            $limit = (isset($attr['limit'])) ? "LIMIT ".$attr['limit'] : '';
            $loaded  = ($this->maincontact['rnum'] != 0) ? "rnum <> ".$this->maincontact['rnum'] : '1';

            $contacts = mysql_query("SELECT * FROM arbetsgivare_jobb_annonser_contacts WHERE jobb_annonser_id='{$attr['id']}' and {$loaded} and active='1' ORDER BY rnum ASC {$limit}");
            $counter=2;
            while($contact = mysql_fetch_array($contacts)) {
				$this->hasContacts = true;
                $contact['rownumber']=$counter;
                $counter++;
    			$this->subitem = $contact;
    			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
    		}
            return $html;
        }
    }

    public function parser_check_contacts($tag, $attr)
    {
        if ($this->loaded_contact == 0){
            return $GLOBALS['parser']->parse($tag['innerhtml']);
        }
    }

    public function sendEmail($id,$sender,$template)
    {
        // mail the user
        @$mail_template = mysql_fetch_array(mysql_query("SELECT * FROM mail_templates WHERE id = ".$template));

        $tags['annons_link'] = $_SERVER['SERVER_NAME'].'/konto/mina-annonser/jobb-annons/redigera/'.$id;
        $template_content['mail_title'] = $mail_template['mail_title'];
        $template_content['mail_text'] = $mail_template['mail_text'];

        // replace the tags
        $mail = $GLOBALS['mail']->tags($tags,$template_content);

        $GLOBALS['mail']->send($sender,$mail['mail_title'],$mail['mail_text']);

        $this->message = "Ëmail sent";
    }
}

?>