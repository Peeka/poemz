<?php

class parser_banners {

	private $item = array();
	private $subitem = array();
	private $rurl = array();
	private $shownOnce = array();
	private $categoryId = 0;
	private $bannersTotal = 0;
	

	function __construct() {
		@$this->rurl = $GLOBALS['page']->get("rurl");
	}
	
	public function parser_banners($tag, $attr) {
		$html = '';
		$now = time();
		$plusOneDay = 60*60*24;
		$this->bannersTotal = 0;
		
		// Get all the banners for this size profile and category
		$random = (isset($attr['random'])) ? " ORDER BY rand()" : " ORDER BY rand()";
		$limit = (isset($attr['limit'])) ? " LIMIT 0,{$attr['limit']}" : "";
		
		// RIGHT MARGIN BANNERS
		if($attr['size_profile'] == 1) {
			// Lets figure out what page we want to load banners for
				$found = false;
				$i = 0;
				$urls[] = '/';
				
				while($i <= count($this->rurl)) {
					if(!empty($this->rurl[$i])) {
						$temp = "";
						for($x = 0; $x <= $i; $x++) {
							if(!empty($this->rurl[$x])) {
								$temp[] = $this->rurl[$x];
							}
						}
						$urls[] = "/". implode("/",$temp);
					}
				$i++;
				}
				@$urls = array_reverse($urls);
				// Now lets see if we can find a parent page
				if($urls[4] == '/till-salu') {
					$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '/'"));
					if($check['id'] > 0) {
						$categoryId = $check['id'];
					}
					else {
						$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = ''"));
						if($check['id'] > 0) {
							$categoryId = $check['id'];
						}
					}
				}
				else if(!empty($urls)) {
					foreach($urls as $url) {
						// See if there is a page with the calculated url
						$url = $this->fixUrl($url);
					//	exit("SELECT * FROM banners_pages WHERE url = '{$url}'");
						$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '{$url}'"));
						if($check['id'] > 0) {
							$categoryId = $check['id'];
							break;
						}
					}
				}
				else {
					$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '/'"));
					if($check['id'] > 0) {
						$categoryId = $check['id'];
					}
					else {
						$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = ''"));
						if($check['id'] > 0) {
							$categoryId = $check['id'];
						}
					}
				}
			
				$banners = mysql_query("SELECT 
						a.*,
						b.flash AS flash,
						b.fallback AS fallback,
						c.* 
					FROM banners_active AS a 
					INNER JOIN banners AS b 
						ON b.id = a.banner_id 
					INNER JOIN banners_size_profiles AS c 
						ON c.id = b.size_profile 
					WHERE b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND '{$now}' < (a.end_date+{$plusOneDay})
					OR
					b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND a.end_date_ongoing = 'true' 
						$random$limit
					");
					
				if(isset($attr['check']) && mysql_num_rows($banners) > 0) {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else {
				
					while($banner = mysql_fetch_array($banners)) {
						$this->bannersTotal++;
						$this->item = $banner;
						$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
					}

				}
		} // BRANSCHGUIDEN BANNERS
		else if($attr['size_profile'] == 2) {
			if($this->rurl[0] == "branschguiden") {
				if($this->rurl[1] == "leverantor") {
					@$company = mysql_fetch_array(mysql_query("SELECT * FROM arbetsgivare_companies WHERE url_friendly = '{$this->rurl[2]}'"));
						
					if(!empty($company['banner_flash']) || !empty($company['banner_fallback'])) {
						$banners[0]['id'] = $company['id'];
						$banners[0]['flash'] = $company['banner_flash'];
						$banners[0]['fallback'] = $company['banner_fallback'];
						$banners[0]['url'] = $company['banner_url'];
						$banners[0]['width'] = 980;
						$banners[0]['height'] = 120;
					}
						
					foreach($banners AS $banner) {
						$this->bannersTotal++;
						$this->item = $banner;
						$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
					}
				}
				else {
					$categoryUrl = mysql_real_escape_string($this->rurl[2]);
					$category = mysql_fetch_array(mysql_query("SELECT id FROM branschguiden_categories WHERE current_url = '{$categoryUrl}'"));
					$banners = mysql_query("SELECT 
							a.*,
							b.flash AS flash,
							b.fallback AS fallback,
							c.* 
						FROM banners_active AS a 
						INNER JOIN banners AS b 
							ON b.id = a.banner_id 
						INNER JOIN banners_size_profiles AS c 
							ON c.id = b.size_profile 
						WHERE b.size_profile = '{$attr['size_profile']}' 
						AND a.category_id = '{$category['id']}' 
						AND a.type = 'branschguiden' 
						AND '{$now}' >= a.start_date 
						AND '{$now}' < (a.end_date+{$plusOneDay})
						OR
						b.size_profile = '{$attr['size_profile']}' 
						AND a.category_id = '{$category['id']}' 
						AND '{$now}' >= a.start_date 
						AND a.end_date_ongoing = 'true' 
						AND a.type = 'branschguiden' 
							$random$limit
						");
						
					if(isset($attr['check']) && mysql_num_rows($banners) > 0) {
						return $GLOBALS['parser']->parse($tag['innerhtml']);
					}
					else {
						// SHOW BRANSCHGUIDEN CATEGORIES
						if($this->rurl[0] == "branschguiden" && $this->rurl[1] == "category") {
							while($banner = mysql_fetch_array($banners)) {
								$this->bannersTotal++;
								$this->item = $banner;
								$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
							}
						}
					}
				}
			}
			else {
				// Lets figure out what page we want to load banners for
				$found = false;
				$i = 0;
				$urls[] = '/';
				
				while($i <= count($this->rurl)) {
					if(!empty($this->rurl[$i])) {
						$temp = "";
						for($x = 0; $x <= $i; $x++) {
							if(!empty($this->rurl[$x])) {
								$temp[] = $this->rurl[$x];
							}
						}
						$urls[] = "/". implode("/",$temp);
					}
				$i++;
				}
				@$urls = array_reverse($urls);
				
				// Now lets see if we can find a parent page
				if(!empty($urls)) {
					foreach($urls as $url) {
						// See if there is a page with the calculated url
						$url = $this->fixUrl($url);
						$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '{$url}'"));
						if($check['id'] > 0) {
							$categoryId = $check['id'];
							break;
						}
					}
				}
				else {
					$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '/'"));
					if($check['id'] > 0) {
						$categoryId = $check['id'];
					}
					else {
						$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = ''"));
						if($check['id'] > 0) {
							$categoryId = $check['id'];
						}
					}
				}
			
				$banners = mysql_query("SELECT 
						a.*, 
						b.flash AS flash, 
						b.fallback AS fallback, 
						c.*
					FROM banners_active AS a 
					INNER JOIN banners AS b 
						ON b.id = a.banner_id 
					INNER JOIN banners_size_profiles AS c 
						ON c.id = b.size_profile 
					WHERE b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND '{$now}' < (a.end_date+{$plusOneDay}) 
					AND c.id = b.size_profile 
					OR 
					b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND a.end_date_ongoing = 'true' 
						$random$limit
					");
					
				if(isset($attr['check']) && mysql_num_rows($banners) > 0) {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else {
					while($banner = mysql_fetch_array($banners)) {
						$this->bannersTotal++;
						$this->item = $banner;
						$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
					}
				}
			}
		}
		else // RIGHT BIG BANNERS
		if($attr['size_profile'] == 3) {
			// Lets figure out what page we want to load banners for
				$found = false;
				$i = 0;
				$urls[] = '/';
				
				while($i <= count($this->rurl)) {
					if(!empty($this->rurl[$i])) {
						$temp = "";
						for($x = 0; $x <= $i; $x++) {
							if(!empty($this->rurl[$x])) {
								$temp[] = $this->rurl[$x];
							}
						}
						$urls[] = "/". implode("/",$temp);
					}
				$i++;
				}
				@$urls = array_reverse($urls);
				
				// Now lets see if we can find a parent page
				if(!empty($urls)) {
					foreach($urls as $url) {
						// See if there is a page with the calculated url
						$url = $this->fixUrl($url);
						$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '{$url}'"));
						if($check['id'] > 0) {
							$categoryId = $check['id'];
							break;
						}
					}
				}
				else {
					$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '/'"));
					if($check['id'] > 0) {
						$categoryId = $check['id'];
					}
					else {
						$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = ''"));
						if($check['id'] > 0) {
							$categoryId = $check['id'];
						}
					}
				}
			
				$banners = mysql_query("SELECT 
						a.*,
						b.flash AS flash,
						b.fallback AS fallback,
						c.*, 
						a.id AS id 
					FROM banners_active AS a 
					INNER JOIN banners AS b 
						ON b.id = a.banner_id 
					INNER JOIN banners_size_profiles AS c 
						ON c.id = b.size_profile 
					WHERE b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND '{$now}' < (a.end_date+{$plusOneDay})
					OR
					b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND a.end_date_ongoing = 'true' 
						$random$limit
					");
					
				if(isset($attr['check']) && mysql_num_rows($banners) > 0) {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else {
					while($banner = mysql_fetch_array($banners)) {
						$this->bannersTotal++;
						$this->item = $banner;
						$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
					}
				}
		}
		else // STARTPAGE SMALL BANNERS
		if($attr['size_profile'] == 4) {
			// Lets figure out what page we want to load banners for
				$found = false;
				$i = 0;
				$urls[] = '/';
				
				while($i <= count($this->rurl)) {
					if(!empty($this->rurl[$i])) {
						$temp = "";
						for($x = 0; $x <= $i; $x++) {
							if(!empty($this->rurl[$x])) {
								$temp[] = $this->rurl[$x];
							}
						}
						$urls[] = "/". implode("/",$temp);
					}
				$i++;
				}
				@$urls = array_reverse($urls);
				
				// Now lets see if we can find a parent page
				$check = mysql_fetch_array(mysql_query("SELECT * FROM banners_pages WHERE url = '/'"));
				if($check['id'] > 0) {
					$categoryId = $check['id'];
				}
			
				$banners = mysql_query("SELECT 
						a.*,
						b.flash AS flash,
						b.fallback AS fallback,
						c.*, 
						a.id AS id 
					FROM banners_active AS a 
					INNER JOIN banners AS b 
						ON b.id = a.banner_id 
					INNER JOIN banners_size_profiles AS c 
						ON c.id = b.size_profile 
					WHERE b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND '{$now}' < (a.end_date+{$plusOneDay})
					OR
					b.size_profile = '{$attr['size_profile']}' 
					AND a.category_id = '{$categoryId}' 
					AND '{$now}' >= a.start_date 
					AND a.end_date_ongoing = 'true' 
						$random$limit
					");
					
				if(isset($attr['check']) && mysql_num_rows($banners) > 0) {
					return $GLOBALS['parser']->parse($tag['innerhtml']);
				}
				else {
					while($banner = mysql_fetch_array($banners)) {
						$this->bannersTotal++;
						$this->item = $banner;
						$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
					}
				}
		}
		
	return $html;
	}
	
	public function fixUrl($url) {
		if(strstr($url,'.se')) {
			$url = strstr($url,'.se');
			$url = str_replace('.se','',$url);
		}
		else if(strstr($url,'.no')) {
			$url = strstr($url,'.no');
			$url = str_replace('.no','',$url);
		}
		return $url;
	}
	
	public function parser_more_than_one($tag, $attr) {
		if($this->bannersTotal > 1) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return '';
		}
	}
	
	public function parser_banners_total($tag, $attr) {
		return $this->bannersTotal;
	}
	
	public function parser_show_once($tag, $attr) {
		if(!in_array($this->shownOnce,$attr['unique_id'])) {
			$this->shownOnce[] = $attr['unique_id'];
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else {
			return "";
		}
	}
	
	public function parser_has_flash($tag, $attr) {
		if(isset($this->item['flash']) && !empty($this->item['flash']) && $attr['state'] == "true") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		else if(empty($this->item['flash']) && $attr['state'] == "false") {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_size_profiles($tag, $attr) {
		// Load the size_profiles
		$html = "";
		@$size_profiles = mysql_query("SELECT * FROM banners_size_profiles ORDER BY name ASC");
		
		while($profile = mysql_fetch_array($size_profiles)) {
			$this->item = $profile;
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		return $html;
	}
	
	public function parser_parent($tag, $attr) {
		// Load the size_profiles
		$html = "";
		@$parent = mysql_fetch_array(mysql_query("SELECT * FROM banners WHERE id = '{$this->item['banner_id']}'"));
		
		return $parent[$attr['get']];
	}
	
	public function parser_subitem($tag, $attr) {
		if(!empty($this->subitem[$attr['get']])) {
			$get = $this->subitem[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			$get = $this->item[$attr['get']];
			// Strip tags
			if(isset($attr['remove_br'])) {
				$get = strip_tags($get);
			}
			// Fix links
			if(isset($attr['link'])) {
				if(!strstr($get,"http://")) {
					$get = "http://{$get}";
				}
			}
			// Fix and return the final string
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($get) > $attr['cut']) {
				$get = substr($get,0,$attr['cut']).$attr['add'];
			}
			// Parse new lines
			if(isset($attr['nl2br'])) {
				$get = nl2br($get);
			}
		}
		return $get;
	}
	
	
}

?>