<?php

class parser_forum {

	public $item = array();

	function __construct() {
		if($GLOBALS['parser']->parse("{page:rurl index='3'/}") == "delete") {
			$id = $GLOBALS['parser']->parse("{page:rurl index='4'/}");
			@$check = mysql_fetch_array(mysql_query("SELECT account_id,parent_id FROM forum WHERE id = '{$id}'"));
			if($check['account_id'] == account::user('id') || account::user('groups') == 1) {
				if($check['parent_id'] == 0) {
					// delete all children
					mysql_query("DELETE FROM forum WHERE parent_id = '{$id}'");
					mysql_query("DELETE FROM forum WHERE id = '{$id}'");
					header("Location: /forum/list");
					exit;
				}
				else {
					mysql_query("DELETE FROM forum WHERE id = '{$id}'");
					header("Location: /forum/list/". $GLOBALS['parser']->parse("{page:rurl index='2'/}"));
					exit;
				}
			}
		}
		if($GLOBALS['parser']->parse("{page:rurl index='1'/}") == "edit") {
			@$id = $GLOBALS['parser']->parse("{page:rurl index='2'/}");
			$this->item = mysql_fetch_array(mysql_query("SELECT * FROM forum WHERE id = '{$id}'"));
		}
	}
	
	public function parser_delete($tag, $attr){
		if($this->item['account_id'] == account::user('id') || account::user('groups') == 1) {
			return $GLOBALS['parser']->parse($tag['innerhtml']);
		}
	}
	
	public function parser_thread_topic($tag, $attr){
		$html = '';
		$id = max(0, intval($GLOBALS['parser']->parse("{page:rurl index='2'/}")));
		@$topic = mysql_query("	SELECT f.*,a.first_name,a.last_name
								FROM forum f
								INNER JOIN accounts a ON (a.id = f.account_id)
								WHERE f.id = " . $id);
		while($t = mysql_fetch_array($topic)) {
			$this->item = $t;
			$this->item['created_on'] = date("Y-m-d, H:i",$this->item['created_on']);
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		return $html;
	}
	
	public function parser_list($tag, $attr) {
		$html = '';
		$id = max(0, intval($GLOBALS['parser']->parse("{page:rurl index='2'/}")));
		$order_direction = $id == 0 ? 'DESC' : 'ASC';
		@$threads = mysql_query("	SELECT f.id,f.account_id,f.title,a.first_name,a.last_name,f.text,f.created_on,count(f2.id) as replies
									FROM forum f
									INNER JOIN accounts a ON (a.id = f.account_id)
									LEFT JOIN forum f2 ON (f.id = f2.parent_id)
									WHERE f.parent_id = ".$id."
									GROUP BY f.id
									ORDER BY f.created_on ".$order_direction.", id ".$order_direction);
		
	//	account::user("id");
	//	account::user("first_name");
	//	account::user("last_name");
	//	account::isLoggedIn();
	
	//	$id = $GLOBALS['parser']->parse("{page:rurl index='2'/}");
		
		while($thread = mysql_fetch_array($threads)) {
			$this->item = $thread;
			$this->item['created_on'] = date("Y-m-d, H:i",$this->item['created_on']);
			$html .= $GLOBALS['parser']->parse($tag['innerhtml']);
		}
		
	return $html;
	}
	
	public function parser_check_edit($tag, $attr) {
		if(isset($_POST['forum_edit'])) {
			$id = $GLOBALS['parser']->parse("{page:rurl index='2'/}");
			$title = $_POST['title'];
			$response = str_replace("\n","<br />",$_POST['description']);
			
			mysql_query("UPDATE forum SET 
							title = '{$_POST['title']}',
							text = '{$_POST['text']}' 
							WHERE id = '{$id}'");
							
			// get the parent id
			@$get = mysql_fetch_array(mysql_query("SELECT parent_id FROM forum WHERE id = '{$id}'"));
			$parent_id = (empty($get['parent_id'])) ? $id : $get['parent_id'];
							
			header("Location: /forum/list/{$parent_id}#post_{$id}");
			exit;
		}
	}
	
	public function parser_check_create($tag, $attr) {
		if(isset($_POST['forum_create'])) {
			$parent_id = max(0, intval($GLOBALS['parser']->parse("{page:rurl index='2'/}")));
			$title = $parent_id == 0 ? $_POST['title'] : '';
			$response = str_replace("\n","<br />",$_POST['text']);
			
			mysql_query("INSERT INTO forum(id, account_id, created_on, title, text, parent_id) 
						VALUES (NULL, ".account::user("id").", '". time()."', '".$title."', '".$response."', ".$parent_id.")");
			
			$last_id = $parent_id > 0 ? $parent_id : mysql_insert_id();
			header("Location: /forum/list/".$last_id);
			exit;
		}
		else if(isset($_POST['forum_reply'])) {
			$parent_id = $GLOBALS['parser']->parse("{page:rurl index='2'/}");
			$title = $_POST['title'];
			$response = str_replace("\n","<br />",$_POST['text']);
			
			mysql_query("INSERT INTO forum(id, account_id, created_on, title, text, parent_id) 
						VALUES (NULL, ".account::user("id").", '". time() ."', '".$title."', '".$response."', ".$parent_id.")");
						
			$id = mysql_insert_id();

			header("Location: /forum/list/{$parent_id}#post_{$id}");
			exit;
		}
	}
	
	public function parser_item($tag, $attr) {
		if(!empty($this->item[$attr['get']])) {
			if(isset($attr['cut']) && !empty($attr['cut']) && strlen($this->item[$attr['get']]) > $attr['cut']) {
				return substr($this->item[$attr['get']],0,$attr['cut']).$attr['add'];
			}
			else {
				return $this->item[$attr['get']];
			}
		}
		else {
			return "";
		}
	}

}

?>