<?php

class parser_image {

	function __construct() {
	
	}
	
	public function parser_render($tag, $attr) {
		$rurl = $GLOBALS['page']->get('rurl');
		$category = mysql_real_escape_string($rurl[1]);
		$name = mysql_real_escape_string($rurl[2]);
		
		$image = explode(".",$name);
		$id = $image[0];
		$extension = $image[1];
		
		// Get the image from the database
		@$dbImg = mysql_fetch_array(mysql_query("SELECT * FROM images WHERE `id` = '{$id}' AND `category` = '{$category}'"));
		
		if(!empty($dbImg['id'])) {
			// Crop dimensions.
			$width = $dbImg['width'];
			$height = $dbImg['height'];
			
			// Set the path to the image to resize
			$input_image = $dbImg['original_image'];
			// Get the size of the original image into an array
			$size = getimagesize( $input_image );
			// Prepare canvas
			$canvas = imagecreatetruecolor( $width, $height );
			// Create a new image in the memory from the file
			$img_type = $dbImg['extension'];
			
			switch ($img_type) {
				case "gif":
					$cropped = imagecreatefromgif( $input_image );
					header('content-type: image/gif');
					break;
				case "jpeg":
					$cropped = imagecreatefromjpeg( $input_image );
					header('content-type: image/jpeg');
					break;
				case "jpg":
					$cropped = imagecreatefromjpeg( $input_image );
					header('content-type: image/jpeg');
					break;
				case "png":
					$cropped = imagecreatefrompng( $input_image );
					header('content-type: image/png');
					break;
			} 
			
			// Generate the cropped image
			if(empty($dbImg['crop_x'])) {
				imagecopyresampled($canvas, $cropped, 0, 0, 0, 0, $dbImg['width'], $dbImg['height'], $dbImg['width'], $dbImg['height']);
			}
			else {
				imagecopyresampled($canvas, $cropped, 0, 0, $dbImg['crop_x'], $dbImg['crop_y'], $dbImg['width'], $dbImg['height'], $dbImg['crop_w'], $dbImg['crop_h']);
			}
			
			// WATERMARK
			if(!empty($dbImg['watermark'])) {
				// creating png image of watermark
				$watermark = imagecreatefromjpeg('/var/www/hrtorget/public/images/watermark.jpg');  
				 
				// getting dimensions of watermark image
				$watermark_width = imagesx($watermark); 
				$watermark_height = imagesy($watermark);
			
				// blending the images together
				imagealphablending($canvas, true);
				imagealphablending($watermark, true);
			
				$size = getimagesize($canvas); 
				// placing the watermark 5px from bottom and right
				$dest_x = $width - $watermark_width - 5; 
				$dest_y = $height - $watermark_height - 5;
				
				// creating the new image
				imagecopy($canvas, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
			}
			
			// Save the cropped image
			imagejpeg( $canvas, null, 100);
			// Clear the memory of the tempory images
			imagedestroy( $canvas );
			imagedestroy( $cropped );
		}
		else {
			// load the default fallback image
			
		}
		
		
		exit;
	}
}

?>